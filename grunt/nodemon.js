module.exports = {
    server: {
        script: 'src/server/server.js',
        options: {
            watch: [
                'src/server/*'
            ]
        }
    },
    serverProd: {
        script: 'dist/server/server.js',
        options: {
            watch: [
                'dist/server/*'
            ]
        }
    }
};

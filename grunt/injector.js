module.exports = {
    angular: {
        files: {
            'dist/client/index.html': [
                'dist/lib/angular/*.js',
                'dist/lib/angular-translate/angular-translate.js',
                'dist/lib/chartJS/*.js',
                'dist/lib/jquery/*.js',
                'dist/lib/bootstrap/dist/js/bootstrap.js',
                'dist/lib/moment/*.js',
                'dist/lib/flot/*.js',
                'dist/lib/**/*.js',
                'dist/client/**/*.module.js',
                'dist/client/**/*.js',
                'dist/lib/**/*.css',
                'dist/client/**/*.css'
            ]
        },
        options: {
            ignorePath: 'dist/'
        }
    },
    prod: {
        files: {
            'dist/client/index.html': [
                'dist/client/*_gct.libs.js',
                'dist/client/*_gct.client.js',
                'dist/client/*_gct.libs.css',
                'dist/client/*_gct.client.css'
            ]
        },
        options: {
            ignorePath: 'dist/client/'
        }
    }
};

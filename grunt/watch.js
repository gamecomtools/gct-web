module.exports = {
    client: {
        files: ['src/client/**'],
        tasks: ['build:dev']
    },
    clientProd: {
        files: ['src/client/**'],
        tasks: ['build:prod']
    }
}

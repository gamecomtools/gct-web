module.exports = {
    prod: {
        options: {

        },
        files: {
            'dist/client/index.html': [
                'dist/client/gct.libs.js',
                'dist/client/gct.client.js',
                'dist/client/gct.libs.css',
                'dist/client/gct.client.css'
            ]
        },
        options: {
            ignorePath: 'dist/'
        }
    }
};

module.exports = {
	less: {
        files: [
            {
                'dist/client/styles/app.css': ['src/client/less/AdminLTE.less'],
                'dist/client/styles/themes.css': ['src/client/less/skins/_all-skins.less']
            }
        ],
        options: {
          compress: true
        }
    }
}

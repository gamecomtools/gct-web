var wiredep = require('wiredep')();

function rename(dest, src) {
    if (src.split('bower_components/')[1] === undefined) {
        // This is a font move
        dest = dest + src.split('fonts/')[1];
    } else {
        dest = dest + src.split('bower_components/')[1];
    }
    if (dest.indexOf('dist') !== -1) {
        dest = dest.replace('dist/js/', '');
        dest = dest.replace('/dist/', '/');
    }
    dest = dest.replace('animate.css', 'animateCSS');
    dest = dest.replace('Chart.js', 'chartJS');
    dest = dest.replace('angular-chart.js', 'angular-chartJS');
    return dest;
}

module.exports = {
    libs:{
        files:[
            {
                src:  [
                    wiredep.js,
                    wiredep.css,
                    'bower_components/**/fonts/**/*',
                    'bower_components/**/*.map'
                ],
                dest: 'dist/lib/',
                expand: true,
                rename: rename
            }
        ]
    },
    client: {
        files: [
            {
                expand: true,
                src: [
                    '**',
                    '!less/**'
                ],
                cwd: 'src/client',
                dest: 'dist/client/'
            }
        ]
    },
    server: {
        files: [
            {
                expand: true,
                src: [
                    '**'
                ],
                cwd: 'src/server',
                dest: 'dist/server/'
            },
            {
                expand: true,
                src: [
                    '**'
                ],
                cwd: 'node_modules',
                dest: 'dist/node_modules/'
            }
        ]
    },
    prod: {
        files: [
            {
                expand: true,
                src: [
                    '**'
                ],
                cwd: 'dist/client/fonts',
                dest: 'dist/fonts/'
            },
            {
                expand: true,
                src: [
                    '**/fonts/*'
                ],
                cwd: 'dist/lib',
                dest: 'dist/fonts/',
                rename: rename
            }
        ]
    }
};

module.exports = {
    angular: {
        src: ['src/client/gct/**/*.htm', 'src/client/gct/**/*.html'],
        dest: 'dist/client/gct/gct.templates.js',
        options: {
            standalone: true,
            module: 'gct.templates',
            url: function(url) {
                return url.replace('src/client/gct/templateOverrides/', '').replace('src/client', '');
            }
        }
    }
};

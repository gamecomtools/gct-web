module.exports = {
  dist: ['dist/*'],
  angular: ['angular/*'],
  html: ['html/*'],
  prod: ['dist/lib', 'dist/client/gct', 'dist/client/styles']
};

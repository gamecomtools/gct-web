var randomNum = function(){
  return Math.floor(Math.random() * 9999999999) + 111111;
};

module.exports = {
  libraries: {
    src:[
      'dist/lib/angular/*.js',
      'dist/lib/angular-translate/angular-translate.js',
      'dist/lib/chartJS/*.js',
      'dist/lib/jquery/*.js',
      'dist/lib/bootstrap/dist/js/bootstrap.js',
      'dist/lib/moment/*.js',
      'dist/lib/flot/*.js',
      'dist/lib/**/*.js'
    ],
    dest:'dist/client/a_' + randomNum() + '_gct.libs.js'
  },
  client: {
    options: {
      banner: '(function () {',
      separator: '})(); (function () {',
      footer: '})();'
    },
    src:[
      'dist/client/**/gct.module.js',
      'dist/client/**/*.module.js',
      'dist/client/**/*.routes.js',
      'dist/client/**/*.js',
      '!dist/client/a_*_gct.libs.js'
    ],
    dest:'dist/client/b_' + randomNum() + '_gct.client.js'
  },
  librariesCSS: {
    src:[
      'dist/lib/**/*.css'
    ],
    dest:'dist/client/' + randomNum() + '_gct.libs.css'
  },
  clientCSS: {
    src:[
      'dist/client/**/*.css'
    ],
    dest:'dist/client/' + randomNum() + '_gct.client.css'
  }
}

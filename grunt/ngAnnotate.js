module.exports = {
    angular: {
        files: [
            {
                expand: true,
                src: ['dist/client/**/*.js']
            }
        ],
        options: {
            singlequote: true
        }
    }
};
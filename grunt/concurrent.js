module.exports = {
    serveDev: [
        'watch:client',
        'nodemon:server'
    ],
    serveProd: [
        'watch:clientProd',
        'nodemon:serverProd'
    ],
    options: {
        logConcurrentOutput: true
    }
}

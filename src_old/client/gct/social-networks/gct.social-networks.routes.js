'use strict';

angular
    .module('gct.socialNetworks')
    .config(GctSocialNetworksConfig);

function GctSocialNetworksConfig($stateProvider){
    $stateProvider
        .state('social-connections', {
            url: '/social-connections',
            templateUrl: '/gct/social-networks/gct.social-networks.htm',
            controller: 'GctSocialNetworks',
            controllerAs: 'GctSocialNetworksVM',
            data: {
                title: 'Social Networks',
                securePage: true
            }
        })
}

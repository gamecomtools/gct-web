'use strict';

angular
    .module('gct.socialNetworks')
    .directive('gctSocialNetwork', GctSocialNetwork);

function GctSocialNetwork(){
    return {
        templateUrl: '/gct/social-networks/gct.social-networks.directive.htm',
        scope: {
            network: '@',
            connection: '=',
            connect: '=',
            icon: '@',
            features: '@',
            comingSoon: '@',
            personal: '@'
        },
        controller: function(){
            var vm = this;
            vm.featureList = vm.features.split(',');
        },
        controllerAs: 'GctSocialNetworkPanelVM',
        bindToController: true,
        replace: true
    }
}

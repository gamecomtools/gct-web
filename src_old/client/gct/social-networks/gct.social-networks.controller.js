'use strict';

angular
    .module('gct.socialNetworks')
    .controller('GctSocialNetworks', GctSocialNetworks);

function GctSocialNetworks(GctCommunity, GctUser, $state, GctReddit, GctGroupMe, $timeout, $rootScope){
    var vm = this;
    vm.community = GctCommunity;
    vm.user = GctUser;
    vm.connecting = false;
    vm.title = $state.current.data.title;
    vm.canCancel = true;
    vm.saveText = 'Save';

    vm.redditStatus = 'waiting';
    vm.redditStatusText = 'Waiting On Moderator Invite';
    vm.redditStatusTextTwo = '';
    vm.redditStatusSubText = '(Do not close this window while waiting)';
    vm.redditStatusLoading = true;
    vm.redditStatusColor = 'yellow';
    vm.redditInfo = {};

    vm.cancelConnecting = function(){
        vm.connecting = false;
        $state.current.data.title = vm.title;
    };

    vm.save = function(){
        vm.cancelConnecting();
    };

    vm.connect = function(network) {
        vm.connecting = network;
        vm.canCancel = true;
        $state.current.data.title = vm.title + ' - Connecting ' + network;
        if (vm.networkControllers[network.toLowerCase().replace(' ', '')]) {
            vm.networkControllers[network.toLowerCase().replace(' ', '')]();
        }
    };

    vm.networkControllers = {
        xboxlive: function(){
            vm.saveText = '';
        },
        groupme: function(){
            vm.canCancel = false;
            vm.saveText = '';
            GctGroupMe.activate().then(function(){
                $rootScope.$broadcast('gctRevalidateGctData');
                $state.go('groupme');
            });
        },
        reddit: function(){
            vm.saveText = '';
            function checkModInvite() {
                GctReddit.checkModInvite().then(function(response){
                    if (response.status == 'invite') {
                        vm.redditInfo = response.plain();
                        vm.redditStatusLoading = false;
                        vm.redditStatusText = 'Invite For /r/' + vm.redditInfo.subreddit + ' Received!';
                        vm.redditStatusSubText = '(/r/' + vm.redditInfo.subreddit + ' Not Correct? Wait 10 Seconds.)';
                        vm.redditStatusTextTwo = 'Click To Continue!';
                        vm.redditStatusColor = 'green';
                        vm.canCancel = false;
                        return vm.redditStatus = 'invited';
                    }
                    $timeout(checkModInvite, 5000);
                });
            }
            checkModInvite();

            function acceptModInvite() {
                GctReddit.acceptModInvite(vm.redditInfo).then(function(response){
                    vm.redditStatus = 'accepted';
                    vm.redditStatusLoading = false;
                    vm.redditStatusText = 'Connected!';
                    vm.redditStatusTextTwo = 'You can now use Reddit tools!';
                    vm.redditStatusColor = 'green';
                    vm.redditStatusSubText = '';
                    vm.saveText = 'Continue';
                    $rootScope.$broadcast('gctRevalidateGctData');
                    vm.save = function(){
                        vm.cancelConnecting();
                    };
                });
            }

            vm.redditInvite = function(){
                if (vm.redditStatus == 'invited') {
                    vm.redditStatus = 'accepting';
                    vm.redditStatusLoading = true;
                    vm.redditStatusText = 'Accepting Moderator Request...';
                    vm.redditStatusTextTwo = '';
                    vm.redditStatusColor = 'yellow';
                    vm.redditStatusSubText = '(This can take up-to 30 seconds)'
                    acceptModInvite();
                }
            }
        }
    }
}

'use strict';

angular
    .module('gct', [
        'ngMaterial',
        'ui.router',
        'ui.bootstrap',
        'angular-loading-bar',
        'gct.dashboard',
        'gct.error',
        'gct.signIn',
        'gct.register',
        'gct.socialNetworks',
        'gct.components',
        'gct.hierarchy',
        'gct.reddit',
        'gct.events',
        'gct.members',
        'gct.groupme',
        'gct.xbox',
        'gct.templates'
    ])
    .config(GctConfig);

function GctConfig($urlRouterProvider, $locationProvider, GctThemeProvider, cfpLoadingBarProvider){
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise("/dashboard");
    GctThemeProvider.$get().setTheme('blue-grey');

    cfpLoadingBarProvider.includeSpinner = false;
}


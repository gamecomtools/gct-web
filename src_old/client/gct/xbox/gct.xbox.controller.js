'use strict';

angular
    .module('gct.xbox')
    .controller('GctXbox', GctXboxController);

function GctXboxController(GctCommunity, $timeout, GctGroupMe, $rootScope, GctUtils, $q){
    var vm = this;
    vm.title = 'What Tool Would You Like To Use?';
    vm.needsLogin = false;
    vm.isLoggedIn = false;
    vm.loginError = false;
    vm.tool = false;
    vm.xboxLive = {
        email: "",
        password: ""
    };

    vm.launchTool = function(tool) {
        vm.tool = tool;
        vm.title = 'Xbox Live Friend Sync';
        if (!vm.isLoggedIn) {
            vm.needsLogin = true;
        }
    }
};

'use strict';

angular
    .module('gct.xbox')
    .config(GctXboxConfig);

function GctXboxConfig($stateProvider){
    $stateProvider
        .state('xbox', {
            url: '/xbox',
            templateUrl: '/gct/xbox/gct.xbox.htm',
            controller: 'GctXbox',
            controllerAs: 'GctXboxVM',
            data: {
                title: 'Xbox Management',
                securePage: true
            }
        })
}

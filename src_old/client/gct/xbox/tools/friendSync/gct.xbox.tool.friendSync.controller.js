'use strict';

angular
    .module('gct.xbox')
    .controller('GctXboxFriendSync', GctXboxFriendSyncController);

function GctXboxFriendSyncController($scope, Restangular){
    var vm = this;
    vm.loading = true;
    vm.loadingText = 'Comparing Current Friends...';
    vm.friends = {
        add: [],
        rem: [],
        keep: [],
        ignore: []
    };
    vm.success = false;

    var api = Restangular.all('api');
    api.customPOST({
        username: $scope.$parent.$parent.$parent.GctXboxVM.xboxLive.email,
        password: $scope.$parent.$parent.$parent.GctXboxVM.xboxLive.password
    }, 'xbox/compareFriends').then(function(response){
        if (response.error) {
            $scope.$parent.$parent.$parent.GctXboxVM.isLoggedIn = false;
            $scope.$parent.$parent.$parent.GctXboxVM.needsLogin = true;
            $scope.$parent.$parent.$parent.GctXboxVM.loginError = response.error;
        } else {
            vm.friends = response.plain();
            vm.friends.ignore = [];
            vm.loading = false;
        }
    }, function(response) {
        $scope.$parent.$parent.$parent.GctXboxVM.isLoggedIn = false;
        $scope.$parent.$parent.$parent.GctXboxVM.needsLogin = true;
        $scope.$parent.$parent.$parent.GctXboxVM.loginError = 'Bad Request';
    });

    vm.move = function(index, from, to) {
        vm.friends[to].push(vm.friends[from][index]);
        vm.friends[from].splice(index, 1);
    };

    vm.syncFriends = function() {
        vm.loading = true;
        vm.loadingText = 'Syncing Friends...';

        vm.friends.email = $scope.$parent.$parent.$parent.GctXboxVM.xboxLive.email;
        vm.friends.password = $scope.$parent.$parent.$parent.GctXboxVM.xboxLive.password;
        api.customPOST(vm.friends, 'xbox/syncFriends').then(function(response){
            if (response.error) {
                $scope.$parent.$parent.$parent.GctXboxVM.isLoggedIn = false;
                $scope.$parent.$parent.$parent.GctXboxVM.needsLogin = true;
                $scope.$parent.$parent.$parent.GctXboxVM.loginError = response.error;
            } else {
                vm.loading = false;
                vm.success = true;
            }
        });
    }
};

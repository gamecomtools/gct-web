'use strict';

angular
    .module('gct.hierarchy')
    .config(GctHierarchyConfig);

function GctHierarchyConfig($stateProvider){
    $stateProvider
        .state('hierarchy', {
            url: '/hierarchy',
            templateUrl: '/gct/hierarchy/gct.hierarchy.htm',
            controller: 'GctHierarchy',
            controllerAs: 'GctHierarchyVM',
            data: {
                title: 'Hierarchy & Permissions',
                securePage: true
            }
        })
}

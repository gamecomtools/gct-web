'use strict';

angular
    .module('gct.hierarchy')
    .controller('GctHierarchy', GctHierarchy);

function GctHierarchy(GctUser, GctCommunity, $rootScope){
    var vm = this;

    vm.isEditing = false;
    vm.loading = true;
    vm.permissions = [
        'social-connections',
        'minecraft',
        'youtube',
        'reddit',
        'groupme',
        'xbox',
        'events',
        'hierarchy',
        'members'
    ];
    vm.groupEditor = {
        permissions: {}
    };
    vm.addingGroup = false;
    vm.movingUser = false;

    vm.hierarchy = [];

    vm.loadHierarchy = function() {
        vm.hierarchy = [];
        GctUser.getAll().then(function (users) {
            var groupPositions = {};
            for (var i in GctCommunity.data.hierarchyGroups) {
                if (!groupPositions[GctCommunity.data.hierarchyGroups[i].position]) {
                    groupPositions[GctCommunity.data.hierarchyGroups[i].position] = [];
                }
                groupPositions[GctCommunity.data.hierarchyGroups[i].position].push(i);
            }

            var keys = Object.keys(groupPositions),
                i, len = keys.length;
            keys.sort();
            for (i = 0; i < len; i++) {
                var hGroup = [];

                var groups = groupPositions[keys[i]];
                groups.forEach(function(group){
                    var g = {};
                    var id = group + '';
                    group = GctCommunity.data.hierarchyGroups[group];
                    //console.log(group);
                    g.title = group.name;
                    g.id = id;
                    if (group.permissions === '') {
                        group.permissions = 'Limited Access';
                    }
                    g.permissions = group.permissions.split(',');
                    g.members = [];
                    users.forEach(function(user){
                        if (user.role === id) {
                            g.members.push(user.username);
                        }
                    });
                    g.system = group.system;
                    hGroup.push(g);
                });

                vm.hierarchy.push(hGroup);
            }

            vm.loading = false;
        });
    };
    $rootScope.$on('gctRevalidateGctDataComplete', function() {
        vm.loadHierarchy();
    });
    if(GctCommunity.data.hierarchyGroups) {
        vm.loadHierarchy();
    }

    vm.moveUser = function(user, group){
        vm.movingUser = {
            user: user,
            group: group
        }
    };

    vm.submitMove = function(group){
        if (vm.movingUser) {
            if (vm.movingUser.init) {
                vm.movingUser.group.members.splice(vm.movingUser.group.members.indexOf(vm.movingUser.user), 1);
                group.members.push(vm.movingUser.user);
                var d = {};
                d[group.id] = vm.movingUser.user;
                GctCommunity.updated(true, d);
                vm.movingUser = false;
            } else {
                vm.movingUser.init = true;
            }
        }
    };

    vm.deleteGroup = function(group, verified){
        if (verified === undefined) {
            group.isDeleting = true;
            vm.isEditing = true;
        } else if(verified === false) {
            group.isDeleting = false;
            vm.isEditing = false;
        } else {
            var members = group.members;
            vm.hierarchy.forEach(function (groupRef, index) {
                groupRef.forEach(function(gR, i) {
                    if (gR.id === group.id) {
                        groupRef.splice(i, 1);
                    } else if (gR.id === 'na') {
                        members.forEach(function(member) {
                            gR.members.push(member);
                        })
                    }
                });
                if (groupRef.length === 0) {
                    vm.hierarchy.splice(index, 1);
                }
            });
            delete GctCommunity.data.hierarchyGroups[group.id];
            GctCommunity.updated(true);
            vm.isEditing = false;
        }
    };

    vm.editGroup = function(group){
        group.editing = true;
        vm.isEditing = true;
        var permissions = [];
        if (group.permissions.length == 1 && group.permissions[0] === '*') {
            permissions = vm.permissions;
        } else {
            permissions = group.permissions;
        }
        vm.permissions.forEach(function(permission){
            vm.groupEditor.permissions[permission] = permissions.indexOf(permission) > -1;
        });
    };

    vm.saveGroup = function(group){
        if (group.title !== '') {
            delete group.editing;
            vm.isEditing = false;
            vm.addingGroup = false;

            if (group.id === 'needsID') {
                group.id = group.title.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
                    if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
                    return index == 0 ? match.toLowerCase() : match.toUpperCase();
                }); // Camelcase the ID ;)
            }
            if (!GctCommunity.data.hierarchyGroups[group.id]) {
                GctCommunity.data.hierarchyGroups[group.id] = {
                    name: group.title,
                    permissions: '',
                    position: group.position
                };
                delete group.position;
            }

            GctCommunity.data.hierarchyGroups[group.id].name = group.title;
            var permissions = [];
            for (var i in vm.groupEditor.permissions) {
                if (vm.groupEditor.permissions[i]) {
                    permissions.push(i);
                }
            }
            if (permissions.length === vm.permissions.length) {
                permissions = ['*'];
            }
            group.permissions = permissions;
            GctCommunity.data.hierarchyGroups[group.id].permissions = permissions.join(',');
            vm.loading = true;
            GctCommunity.updated(true).then(function(){
                vm.loadHierarchy();
            });
        }
    };

    vm.addGroup = function(group){
        if (!group) {
            vm.addingGroup = true;
            vm.isEditing = true;
            var addHereGroup = {
                id: 'addHere',
                members: [],
                permissions: [],
                title: ''
            };

            vm.hierarchy.forEach(function (group, index) {
                if (group[0].id !== 'na') {
                    addHereGroup.rand = Math.random();
                    addHereGroup.position = index;
                    group.push(angular.copy(addHereGroup));
                }
            });
            addHereGroup.rand = Math.random();
            addHereGroup.position = vm.hierarchy.length - 1;
            vm.hierarchy.splice(vm.hierarchy.length - 1, 0, [angular.copy(addHereGroup)]);
        } else {
            vm.hierarchy.forEach(function (groupRef, index) {
                if (groupRef.length === 1) {
                    if (groupRef[0].id === 'addHere') {
                        if (groupRef[0].rand === group.rand) {
                            // This group consisted only of the add here
                            // and was selected
                            group.editing = true;
                            group.id = 'needsID';
                        } else {
                            // This group consisted only of the add here
                            // delete it
                            vm.hierarchy.splice(index, 1);
                        }
                    }
                } else {
                    groupRef.forEach(function(gR, i) {
                        if (gR.id === 'addHere') {
                            if (gR.rand === group.rand) {
                                // This group was selected
                                group.editing = true;
                                group.id = 'needsID';
                            } else {
                                // This group was nto selected, delete it
                                groupRef.splice(i, 1);
                            }
                        }
                    });
                }
            });
        }
    }
}

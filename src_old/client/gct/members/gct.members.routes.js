'use strict';

angular
    .module('gct.members')
    .config(GctMembersConfig);

function GctMembersConfig($stateProvider){
    $stateProvider
        .state('members', {
            url: '/members',
            templateUrl: '/gct/members/gct.members.htm',
            controller: 'GctMembersCtrl',
            controllerAs: 'GctMembersVM',
            data: {
                title: 'Member Management',
                securePage: true
            }
        })
}

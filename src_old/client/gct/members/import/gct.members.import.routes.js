'use strict';

angular
    .module('gct.members.import')
    .config(GctMembersImportConfig);

function GctMembersImportConfig($stateProvider){
    $stateProvider
        .state('members-import', {
            templateUrl: '/gct/members/import/gct.members.import.htm',
            controller: 'GctMembersImport',
            controllerAs: 'GctMembersImportVM',
            data: {
                title: 'Import Members',
                securePage: true
            }
        })
        .state('members-import.step1', {
            url: '/members/import',
            templateUrl: '/gct/members/import/steps/importSource.view.htm'
        })
        .state('members-import.step2', {
            url: '/members/import/:from',
            templateUrl: '/gct/members/import/steps/importColumns.view.htm'
        })
        .state('members-import.step3', {
            url: '/members/import/:from/importing',
            templateUrl: '/gct/members/import/steps/importing.view.htm'
        })
}

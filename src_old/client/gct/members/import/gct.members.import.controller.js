'use strict';

angular
    .module('gct.members.import')
    .controller('GctMembersImport', GctMembersImport);

function GctMembersImport(GctMembers, $rootScope, $state){
    var vm = this;

    vm.loading = false;
    vm.error = false;
    vm.worksheets = false;
    vm.selectedData = {};
    vm.worksheetData = null;
    vm.columnHeaders = [];
    vm.steps = [
        {
            text: 'Import Source',
            icon: 'cloud-download'
        },
        {
            text: 'Define Columns',
            icon: 'table',
            onEnter: function(){
                vm.importFrom = $state.params.from;
            }
        },
        {
            text: 'Importing',
            icon: 'refresh',
            onEnter: function(){
                vm.loading = true;
                vm.importFrom = $state.params.from;
                GctMembers.importData(vm.importFrom, {
                    worksheet: vm.selectedData.worksheetId,
                    columnTrackingNames: vm.columnHeaders,
                    sheetUrl: vm.sheetsURL
                }).then(function(response){
                    $rootScope.$broadcast('gctRevalidateGctData');
                    vm.loading = false;
                }, function(response){
                    vm.error = response.error;
                });
            }
        }
    ];

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        if(toState.name.indexOf('members-import') > -1) {
            vm.step = parseInt(toState.name.split('step')[1]) - 1;
            if (vm.steps[vm.step].onEnter) {
                vm.steps[vm.step].onEnter();
            }
        }
    });

    vm.step = parseInt($state.current.name.split('step')[1]) - 1;
    if (vm.steps[vm.step].onEnter) {
        vm.steps[vm.step].onEnter();
    }

    vm.sheetsURLChanged = function(){
        vm.loading = true;
        vm.error = false;
        GctMembers.analyze(vm.importFrom, {
            sheetUrl: vm.sheetsURL
        }).then(function(response){
            vm.loading = false;
            vm.worksheets = response.worksheets;
        }, function(response){
            vm.error = response.error;
        });
    };

    vm.worksheetSelected = function(index){
        vm.selectedData.worksheet = vm.worksheets[index].title;
        vm.selectedData.worksheetId = vm.worksheets[index].id;
        vm.loading = true;
        GctMembers.getImportData(vm.importFrom, {
            worksheet: vm.worksheets[index].id,
            rows: 10,
            sheetUrl: vm.sheetsURL
        }).then(function(response){
            vm.loading = false;
            vm.worksheetData = response.rows;
        }, function(response){
            vm.error = response.error;
        });
    }
}

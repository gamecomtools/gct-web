'use strict';

angular
    .module('gct.members')
    .controller('GctMembersCtrl', GctMembersCtrl);

function GctMembersCtrl(GctMembers, GctCommunity, $rootScope){
    var vm = this;

    vm.loading = true;
    vm.members = GctMembers;
    vm.community = GctCommunity;
    vm.tab = 'members';

    GctMembers.load().then(function(){
        vm.loading = false;
        console.log(vm.members.data.length);
    });
}

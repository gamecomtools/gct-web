'use strict';

angular
    .module('gct.components')
    .factory('GctUtils', GctUtils);

function GctUtils(){
    return {
        widgetsToColumns: function(items, groupItems, visibility, vm, fullWidthWidgets) {
            if (items) {
                var columnData = [[[], [], []]];

                var row = 0;
                var col = 0;
                items.forEach(function(value){
                    var isVisible = true;
                    if (typeof value === 'string' && visibility[value]) {
                        var tmp = vm;
                        var split = visibility[value].split('.');
                        split.forEach(function(value){
                            if (tmp[value] === undefined) {
                                isVisible = false;
                            } else {
                                tmp = tmp[value];
                            }
                        });
                        if (tmp === undefined){
                            isVisible = false;
                        }
                    }
                    if (isVisible) {
                        if (fullWidthWidgets && fullWidthWidgets.indexOf(value) > -1) {
                            columnData[row].splice(col, groupItems - col);
                            col = 0;
                            row ++;
                            columnData[row] = [[]];
                        }
                        columnData[row][col].push(value);
                        col++;

                        if (col == groupItems || (fullWidthWidgets && fullWidthWidgets.indexOf(value) > -1)) {
                            col = 0;
                            row ++;
                            columnData[row] = [[], [], []];
                        }
                    }
                });

                return columnData;
            }
        }
    }
}

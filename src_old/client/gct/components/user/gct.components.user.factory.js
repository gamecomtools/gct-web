'use strict';

angular
    .module('gct.components')
    .factory('GctUser', GctUser);

function GctUser($q, $rootScope, Restangular, GctCommunity){
    return {
        data: {},
        isLoggedIn: false,
        login: function(username, password){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customPOST({
                    username: username,
                    password: password
                }, 'user').then(function(response){
                    if (response.error) {
                        return reject(response.error);
                    }
                    vm.isLoggedIn = true;
                    vm.data = response.plain();
                    GctCommunity.load(vm.data.community).then(function(){
                        resolve();
                    }, function(err){
                        reject(err);
                    });
                });
            });
        },
        register: function(
            username,
            password,
            community,
            communityRole,
            position
        ){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customPUT({
                    username: username,
                    password: password,
                    community: community,
                    role: communityRole,
                    position: position
                }, 'user').then(function(response){
                    if (response.error) {
                        return reject(response.error);
                    }
                    vm.isLoggedIn = true;
                    vm.data = response.plain();
                    resolve();
                });
            });
        },
        getAll: function(){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customGET('users').then(function(response){
                    if (response.error) {
                        return reject(response.error);
                    }
                    resolve(response.plain());
                });
            });
        }
    }
}

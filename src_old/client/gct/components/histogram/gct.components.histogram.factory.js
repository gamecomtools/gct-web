'use strict';

angular
    .module('gct.components')
    .factory('GctHistogram', GctHistogram);

function GctHistogram($q, Restangular){
    return {
        get: function(community, key, days, max, fill, string, labels, threshold, series){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customGET('histogram', {
                    community: community,
                    key: key,
                    days: days,
                    max: max || false,
                    fill: fill || false,
                    string: string || false,
                    labels: labels == undefined ? true : labels,
                    threshold: threshold || 1,
                    series: series
                }).then(function(response){
                    resolve(response.plain());
                });
            });
        }
    }
}

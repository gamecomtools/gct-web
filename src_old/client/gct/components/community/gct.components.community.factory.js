'use strict';

angular
    .module('gct.components')
    .factory('GctCommunity', GctCommunity);

function GctCommunity($q, Restangular, $rootScope){
    return {
        data: {
            eventTypes: {},
            socialConnections: {}
        },
        watcher: null,
        dataEqualizer: function(){
            var vm = this;

            vm.data.eventTypes = vm.data.eventTypes || {};
            vm.data.socialConnections = vm.data.socialConnections || {};
        },
        register: function(name){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customPUT({
                    name: name
                }, 'community').then(function(response){
                    if (response.error) {
                        return reject(response.error);
                    }
                    vm.data = response.plain();
                    vm.dataEqualizer();
                    resolve();
                });
            });
        },
        load: function(id){
            var vm = this;

            if (vm.watcher !== null) {
                vm.watcher();
                vm.watcher = null;
            }

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customGET('community', {
                    id: id
                }).then(function(response){
                    if (response.error) {
                        return reject(response.error);
                    }
                    vm.data = response.plain();
                    vm.dataEqualizer();
                    resolve();
                });
            });
        },
        reload: function(){
            return this.load(this.data.id);
        },
        updated: function(syncRoles, updateUser){
            var vm = this;
            var api = Restangular.all('api');

            return $q(function(resolve, reject) {
                api.customPOST({
                    community: vm.data,
                    fullUpdate: true,
                    syncRoles: syncRoles,
                    uU: updateUser
                }, 'community/update').then(function (response) {
                    if (response.error) {
                        console.log(response.error);
                        reject();
                    } else {
                        resolve();
                    }
                });
            });
        }
    }
}

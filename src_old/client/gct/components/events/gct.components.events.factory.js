'use strict';

angular
    .module('gct.components')
    .factory('GctEvents', GctEvents);

function GctEvents($q, Restangular){
    return {
        load: function(){
            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customGET('events/google').then(function(response){
                    if (response.error) {
                        return reject(response.error);
                    }
                    resolve(response.plain());
                });
            });
        },
        loadOld: function(){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customGET('events').then(function(response){
                    if (response.error) {
                        return reject(response.error);
                    }
                    var data = response.plain();

                    data.forEach(function(event){
                        event.editable = true;
                        event.deletable = true;
                        event.incrementsBadgeTotal = false;
                        event.type = event.color;
                        event.starts_at = new Date(event.starts.iso);
                        if (event.ends !== undefined) {
                            event.ends_at = new Date(event.ends.iso);
                        } else {
                            event.ends_at = new Date(event.starts.iso);
                        }
                    });
                    resolve(data);
                });
            });
        },
        add: function(communityId, title, color, host, starts, platform){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customPUT({
                    communityId: communityId,
                    title: title,
                    color: color,
                    host: host,
                    starts: starts,
                    platform: platform
                }, 'event').then(function(response){
                    if (response.error) {
                        return reject(response.error);
                    }
                    resolve();
                });
            });
        }
    }
}

'use strict';

angular
    .module('gct.components')
    .factory('GctGroupMe', GctGroupMe);

function GctGroupMe($q, Restangular){
    return {
        activate: function(){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customGET('groupme/activate').then(function(response){
                    resolve(response);
                });
            });
        },
        checkGroups: function(){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customGET('groupme/checkGroups').then(function(response){
                    resolve(response);
                });
            });
        },
        connectGroups: function(groupData){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');
                var groups = {};
                for (var i in groupData){
                    groups[i] = groupData[i].function;
                }
                api.customPOST({
                    groups: groups
                }, 'groupme/connectGroups').then(function(response){
                    resolve(response);
                });
            });
        },
        retrieveGroups: function(groups){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customPOST({
                    groups: groups
                }, 'groupme/retrieveGroups').then(function(response){
                    resolve(response.plain());
                });
            });
        },
        updateNickname: function(groupId, nickname){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customPOST({
                    groupId: groupId,
                    nickname: nickname
                }, 'groupme/updateNickname').then(function(response){
                    resolve(response.plain());
                });
            });
        }
    }
}

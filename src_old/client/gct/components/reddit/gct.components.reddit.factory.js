'use strict';

angular
    .module('gct.components')
    .factory('GctReddit', GctReddit);

function GctReddit($q, Restangular){
    return {
        checkModInvite: function(){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customGET('reddit/checkModInvite').then(function(response){
                    resolve(response);
                });
            });
        },
        acceptModInvite: function(modInfo){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customPOST({
                        name: modInfo.name,
                        subreddit: modInfo.subreddit
                    }, 'reddit/acceptModInvite').then(function(response){
                    resolve(response);
                });
            });
        }
    }
}

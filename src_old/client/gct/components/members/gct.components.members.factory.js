'use strict';

angular
    .module('gct.components')
    .factory('GctMembers', GctMembers);

function GctMembers($q, Restangular){
    return {
        data: {},
        load: function(){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customGET('members').then(function(response){
                    if (response.error) {
                        return reject(response.error);
                    }
                    vm.data = response.plain();
                    resolve();
                });
            });
        },
        analyze: function(importFrom, data){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customGET('memberImport/' + importFrom + '/analyze', data || {}).then(function(response){
                    if (response.error) {
                        return reject(response.plain());
                    }
                    resolve(response.plain());
                });
            });
        },
        getImportData: function(importFrom, data){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customGET('memberImport/' + importFrom + '/data', data || {}).then(function(response){
                    if (response.error) {
                        return reject(response.plain());
                    }
                    resolve(response.plain());
                });
            });
        },
        importData: function(importFrom, data){
            var vm = this;

            return $q(function(resolve, reject) {
                var api = Restangular.all('api');

                api.customPOST(data, 'memberImport/' + importFrom + '/import').then(function(response){
                    if (response.error) {
                        return reject(response.plain());
                    }
                    resolve(response.plain());
                });
            });
        }
    }
}

'use strict';

angular
    .module('gct.components')
    .directive('gctNavItem', ['$timeout', function($timeout) {
        return {
            restrict: 'AC',
            link: function(scope, el, attr) {
                el.bind('click', function(e) {
                    var li = angular.element(this).parent();
                    var active = li.parent()[0].querySelectorAll('.active');
                    li.toggleClass('active');
                    angular.element(active).removeClass('active');
                });
            }
        };
    }]);

'use strict';

angular
    .module('gct.components')
    .factory('GctTheme', GctTheme);

function GctTheme($rootScope){
    return {
        setTheme: function(theme) {
            $rootScope.GctTheme = theme;
        }
    }
}

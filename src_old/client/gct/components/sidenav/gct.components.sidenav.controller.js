'use strict';

angular
    .module('gct.components')
    .controller('GctSidenavController', GctSidenavController);

function GctSidenavController($rootScope, GctUser, GctCommunity){
    var vm = this;
    vm.stateData = {};
    vm.user = GctUser;
    vm.community = GctCommunity;

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        vm.stateData = toState.data;
    });

    vm.navClasses = {
        'sectionTitle': 'nav-header m-v-sm hidden-folded'
    };

    vm.nav = [
        {
            type: 'item',
            text: 'Dashboard',
            icon: 'tachometer',
            sref: 'dashboard'
        },
        {
            type: 'item',
            text: 'Member Management',
            icon: 'users',
            sref: 'members'
        },
        {
            type: 'item',
            text: 'Hierarchy/Permissions',
            icon: 'institution',
            sref: 'hierarchy'
        },
        {
            type: 'item',
            text: 'Social Connections',
            icon: 'connectdevelop',
            sref: 'social-connections'
        },
        {
            type: 'item',
            text: 'Event Management',
            icon: 'calendar',
            sref: 'events'
        },
        {
            type: 'item',
            text: 'Reddit Management',
            icon: 'reddit',
            sref: 'reddit',
            if: function(){
                return vm.community.data.socialConnections && vm.community.data.socialConnections.reddit
            }
        },
        {
            type: 'item',
            text: 'GroupMe Management',
            icon: 'comments-o',
            sref: 'groupme',
            if: function(){
                return vm.community.data.socialConnections && vm.community.data.socialConnections.groupme
            }
        },
        {
            type: 'item',
            text: 'Xbox Management',
            icon: 'gamepad',
            sref: 'xbox'
        }
        //{
        //    type: 'dropdown',
        //    text: 'Social Management',
        //    icon: 'connectdevelop',
        //    items: [
        //        {
        //            text: 'Reddit',
        //            sref: 'test'
        //        },
        //        {
        //            text: 'Youtube',
        //            sref: 'test'
        //        },
        //        {
        //            text: 'Social Club',
        //            sref: 'test'
        //        }
        //    ]
        //}
    ];
}

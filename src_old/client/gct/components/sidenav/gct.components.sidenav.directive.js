'use strict';

angular
    .module('gct.components')
    .directive('gctSidenav', GctSidenav);

function GctSidenav(){
    return {
        templateUrl: '/gct/components/sidenav/gct.components.sidenav.htm',
        controller: 'GctSidenavController',
        controllerAs: 'GctSidenavVM',
        replace: true
    }
}

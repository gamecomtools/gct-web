'use strict';

angular
    .module('gct.components')
    .directive('gctContent', GctContent);

function GctContent(){
    return {
        templateUrl: '/gct/components/content/gct.components.content.htm',
        controller: 'GctContentController',
        controllerAs: 'GctContentVM',
        replace: true
    }
}

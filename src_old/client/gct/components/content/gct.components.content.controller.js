'use strict';

angular
    .module('gct.components')
    .controller('GctContentController', GctContentController);

function GctContentController($rootScope, $mdSidenav, GctUser, GctCommunity, $state){
    var vm = this;
    vm.community = GctCommunity;
    vm.stateData = {};
    $rootScope.showContent = true;

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        vm.stateData = toState.data;
    });

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        $rootScope.showContent = false;
        vm.secureError = false;
        if (vm.stateData.securePage && !GctUser.isLoggedIn) {
            $rootScope.showContent = false;
            GctUser.login('checkUser').then(function(){
                // The user is loggedin via session
                $rootScope.$broadcast('gctRevalidateGctData');
                validatePermissions();
            }, function(){
                $rootScope.showContent = true;
                $state.go('sign-in', {
                    toState: toState.name,
                    securePageError: true
                });
            });
        } else if (vm.stateData.securePage) {
            validatePermissions();
        } else if (!vm.stateData.securePage) {
            $rootScope.showContent = true;
        }

        function validatePermissions(){
            var userGroup = GctCommunity.data.hierarchyGroups[GctUser.data.role];
            if (userGroup.permissions === '*' ||
                userGroup.permissions.indexOf(toState.name) > -1 ||
                toState.name == 'dashboard') {
                $rootScope.showContent = true;
            } else {
                vm.secureError = true;
                $rootScope.showContent = true;
            }
        }
    });

    $rootScope.$on('gctRevalidateGctData', function() {
        GctCommunity.reload().then(function(){
            $rootScope.$broadcast('gctRevalidateGctDataComplete');
        });
    });

    vm.openAside = function(){
        $mdSidenav('aside').open();
    }
}

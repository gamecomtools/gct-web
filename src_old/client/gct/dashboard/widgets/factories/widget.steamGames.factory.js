'use strict';

angular
    .module('gct.dashboard.widgets')
    .factory('GctWidget_steamGames', GctWidget_steamGames);

function GctWidget_steamGames($q, GctHistogram, GctCommunity){
    return function(parent, communityId, days, label, threshold, series) {
        var deferred = $q.defer();

        var data = {
            labels: [],
            series: [''],
            data: [[]]
        };

        $q.all([
            GctHistogram.get(communityId, 'steamGames', days.toString(), true, true, true, label, threshold, series)
        ]).then(function (results) {
            data = results[0];
            deferred.resolve(data);

            parent.games = ['All'].concat(data.allSeries);
        });

        return deferred.promise;
    }
}

'use strict';

angular
    .module('gct.dashboard.widgets')
    .factory('GctWidget_redditsubscribers', GctWidget_redditsubscribers);

function GctWidget_redditsubscribers($q, GctHistogram, GctCommunity){
    return function(parent, communityId, days) {
        var deferred = $q.defer();

        var data = {
            labels: [],
            series: ['Subscribers', 'Viewers'],
            data: [[], []]
        };

        $q.all([
            GctHistogram.get(communityId, 'redditSubscribers', days.toString(), true, true),
            GctHistogram.get(communityId, 'redditViewers', days.toString(), true, true)
        ]).then(function (results) {
            results[0].forEach(function(value){
                data.labels.push(value.date);
                data.data[0].push(value.value);
            });
            results[1].forEach(function(value){
                data.data[1].push(value.value);
            });
            deferred.resolve(data);
        });

        return deferred.promise;
    }
}

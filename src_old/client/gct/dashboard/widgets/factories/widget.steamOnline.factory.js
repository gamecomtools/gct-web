'use strict';

angular
    .module('gct.dashboard.widgets')
    .factory('GctWidget_steamOnline', GctWidget_steamOnline);

function GctWidget_steamOnline($q, GctHistogram, GctCommunity){
    return function(parent, communityId, days, label) {
        var deferred = $q.defer();

        var data = {
            labels: [],
            series: ['Max Online Members', 'Max Online Members (Not Gaming)'],
            data: [[]]
        };

        $q.all([
            GctHistogram.get(communityId, 'steamOnline', days.toString(), true, true, false, label),
            GctHistogram.get(communityId, 'steamOnlineNonGame', days.toString(), true, true, false, label)
        ]).then(function (results) {
            data.labels = results[0].labels;
            data.data[0] = results[0].data;
            data.data[1] = results[1].data;
            deferred.resolve(data);
        });

        return deferred.promise;
    }
}

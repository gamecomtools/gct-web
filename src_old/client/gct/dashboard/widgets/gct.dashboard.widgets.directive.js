'use strict';

angular
    .module('gct.dashboard')
    .directive('gctDashboardWidget', GctDashboardWidget);

function GctDashboardWidget(){
    var first = true;

    return {
        link: function(scope, element, attrs) {
            scope.getContentUrl = function() {
                return '/gct/dashboard/widgets/templates/widget.' + scope.GctDashboardWidgetVM.type + '.htm';
            }
        },
        template: '<div ng-include="getContentUrl()"></div>',
        scope: {
            type: '@'
        },
        controller: function($rootScope, GctCommunity, $scope){
            Chart.defaults.global.animation = false;
            Chart.defaults.global.responsive = false;
            var vm = this;
            vm.loading = false;
            vm.labels = [0];
            vm.series = [''];
            vm.data = [];

            // Options
            vm.days = 7;
            vm.threshold = 10;
            vm.label = 'daily';
            if (vm.type === 'xboxGames' || vm.type === 'steamGames') {
                vm.label = 'hourly';
            }
            vm.seriesSelected = 'All';
            vm.games = ['All'];

            vm.updateData = function(){
                vm.loading = true;
                vm.labels = [];
                vm.series = [];
                vm.data = [];
                if (vm.days == 1 && vm.label !== 'hourly') {
                    vm.label = 'hourly';
                }
                if (GctCommunity.data.id !== undefined) {
                    var angularCore = angular.injector(['gct.dashboard.widgets']);
                    var factoryName = 'GctWidget_' + vm.type.replace(/-/g, '');
                    if (!angularCore.has(factoryName)) {
                        console.log('No Widget Factory For ' + vm.type);
                    }
                    var factory = angularCore.get(factoryName)(vm, GctCommunity.data.id, vm.days, vm.label, vm.threshold, vm.seriesSelected);
                    if (factory.then) {
                        // This is a promise
                        factory.then(function (results) {
                            $scope.$apply(function() {
                                for (var i in results) {
                                    vm[i] = results[i];
                                }
                                vm.loading = false;
                            });
                        });
                    } else {
                        for (var i in factory) {
                            vm[i] = factory[i];
                        }
                        vm.loading = false;
                    }
                }
            };

            $rootScope.$on('gctRevalidateGctDataComplete', function(){
                vm.updateData();
            });
            vm.updateData();
        },
        controllerAs: 'GctDashboardWidgetVM',
        bindToController: true
    }
}

'use strict';

angular
    .module('gct.dashboard')
    .controller('GctDashboard', GctDashboardController);

function GctDashboardController($stateParams, $mdDialog, GctCommunity, GctUser, $rootScope, GctMembers, GctUtils){
    var vm = this;
    vm.community = GctCommunity;
    vm.members = GctMembers;
    vm.user = GctUser;
    vm.bubbles = {};

    vm.games = [];

    if ($stateParams.showWelcome) {
        $mdDialog.show({
            controller: GctWelcomeModalController,
            templateUrl: '/gct/dashboard/templates/dashboard.welcomeModal.template.htm'
        });
    }

    vm.members.load();

    vm.widgetVisible = {
        redditsubscribers: 'community.data.socialConnections.reddit',
        xboxOnline: 'community.data.memberImportSources',
        xboxGames: 'community.data.memberImportSources',
        steamOnline: 'community.data.memberImportSources',
        steamGames: 'community.data.memberImportSources'
    };

    vm.fullWidthWidgets = [
        'xboxGames',
        'steamGames'
    ];

    var widgets = [
        'redditsubscribers',
        'xboxOnline',
        'steamOnline',
        'xboxGames',
        'steamGames'
    ];

    $rootScope.$on('gctRevalidateGctDataComplete', function() {
        vm.widgets = GctUtils.widgetsToColumns(widgets, 3, vm.widgetVisible, vm, vm.fullWidthWidgets);
    });
    vm.widgets = GctUtils.widgetsToColumns(widgets, 3, vm.widgetVisible, vm, vm.fullWidthWidgets);
};

function GctWelcomeModalController($scope, $mdDialog) {
    $scope.hide = function() {
        $mdDialog.hide();
    };
}

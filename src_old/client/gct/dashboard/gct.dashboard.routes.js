'use strict';

angular
    .module('gct.dashboard')
    .config(GctDashboardConfig);

function GctDashboardConfig($stateProvider){
    $stateProvider
        .state('dashboard', {
            url: '/dashboard',
            templateUrl: '/gct/dashboard/gct.dashboard.htm',
            controller: 'GctDashboard',
            controllerAs: 'GctDashboardVM',
            data: {
                title: 'Dashboard',
                securePage: true
            },
            params: {
                showWelcome: false
            }
        })
}

'use strict';

angular
    .module('gct.dashboard', [
        'chart.js',
        'mwl.calendar'
    ]);

angular
    .module('gct.dashboard.widgets', [
        'ng',
        'gct.components'
    ]);

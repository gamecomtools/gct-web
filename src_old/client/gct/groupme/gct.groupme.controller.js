'use strict';

angular
    .module('gct.groupme')
    .controller('GctGroupMe', GctGroupMeController);

function GctGroupMeController(GctCommunity, $timeout, GctGroupMe, $rootScope, GctUtils, $q){
    var vm = this;
    vm.community = GctCommunity;

    vm.initGroups = [];
    vm.initData = {};
    vm.initalizingGroups = false;
    vm.initLoaded = false;
    vm.groups = [[],[],[]];
    vm.loading = true;
    vm.addMore = false;
    vm.allowGroupCheck = true;
    vm.games = null;
    vm.gameTypes = null;

    vm.inviteTriggerSaveText = 'SAVE INVITE TRIGGER';
    vm.inviteTriggerError = false;
    vm.inviteTrigger = {
        type: 'disabled'
    };
    vm.triggerSaveText = 'SAVE TRIGGERS';
    vm.me = {};

    vm.newTriggers = [
        {
            trigger: '',
            response: ''
        }
    ];

    vm.tabs = [
        {
            name: 'Dynamic Banners',
            view: 'dynamicBanner'
        }
    ];

    vm.checkGroups = function(){
        GctGroupMe.checkGroups().then(function(response){
            response = response.plain();
            if (vm.games === null) {
                vm.games = response.games;
            }
            if (vm.gameTypes === null) {
                vm.gameTypes = response.gameTypes;
            }
            response = response.groups;
            if (response.length !== vm.initGroups.length && vm.allowGroupCheck) {
                for(var i = response.length -1; i >= 0 ; i--){
                    if (response[i].creator_user_id === '26724659') {
                        response.splice(i, 1);
                    }
                }
                vm.initGroups = response;
                vm.initGroups.forEach(function(group){
                    if (!vm.initData[group.group_id]) {
                        vm.initData[group.group_id] = {};
                    }
                });
            }
            if (!vm.initalizingGroups && vm.allowGroupCheck) {
                $timeout(vm.checkGroups, 5000);
            }
        });
    };

    $rootScope.$on('gctRevalidateGctDataComplete', function() {
        if (!GctCommunity.data.socialConnections.groupme.groups && !vm.initLoaded) {
            vm.checkGroups();
            vm.initLoaded = true;
        } else if(GctCommunity.data.socialConnections.groupme.groups) {
            vm.refreshGroupData(GctCommunity.data.socialConnections.groupme.groups).then(function(groups){
                vm.groups = GctUtils.widgetsToColumns(groups, 3);
                vm.initalizingGroups = false;
            });
        }
    });

    vm.refreshGroupData = function(groups){
        vm.inviteTrigger = vm.community.data.socialConnections.groupme.autoInviteTrigger || {type: 'disabled'};
        vm.loading = true;
        return $q(function(resolve, reject) {
            GctGroupMe.retrieveGroups(groups).then(function (response) {
                vm.loading = false;
                vm.me = response.me;
                resolve(response.groups);
            });
        });
    };

    vm.resetInitError = function(groupId){
        vm.initData[groupId].err = undefined;
    };

    vm.addMoreGroups = function() {
        vm.allowGroupCheck = true;
        vm.addMore = true;
        vm.checkGroups();
    };

    vm.initializeGroups = function(){
        var errors = false;
        for(var i in vm.initData) {
            if (!vm.initData[i].function) {
                vm.initData[i].err = "Group Function Required";
                errors = true;
            }
        }
        if (!errors) {
            for(var i in vm.initData) {
                if (vm.initData[i].function === 'notMine') {
                    delete vm.initData[i];
                }
            }
            vm.initalizingGroups = true;
            vm.addMore = false;
            GctGroupMe.connectGroups(vm.initData).then(function(response){
                $rootScope.$broadcast('gctRevalidateGctData');
            });
        }
    };

    vm.updateCommunity = function(type){
        vm.groups.forEach(function(col){
            col.forEach(function(group){
                if (vm.community.data.socialConnections.groupme.groupData[group.group_id]) {
                    vm.community.data.socialConnections.groupme.groupData[group.group_id].lockNameValue = group.name;
                }
            });
        });
        GctCommunity.updated();
    };

    if(GctCommunity.data.socialConnections.groupme && GctCommunity.data.socialConnections.groupme.groups) {
        vm.refreshGroupData(GctCommunity.data.socialConnections.groupme.groups).then(function(groups){
            vm.groups = GctUtils.widgetsToColumns(groups, 3);
        });
    }

    vm.newTriggerChange = function(trigger) {
        if (!trigger.alreadyAddedExtra) {
            trigger.alreadyAddedExtra = true;
            vm.newTriggers.push({
                trigger: '',
                response: ''
            });
        } else if(trigger.trigger === '' && trigger.response === '') {
            trigger.alreadyAddedExtra = false;
            vm.newTriggers.pop();
        }
    };

    vm.removeTrigger = function(index){
        vm.community.data.socialConnections.groupme.triggers.splice(index, 1);
        vm.saveTriggers();
    };

    vm.saveTriggers = function() {
        vm.triggerSaveText = 'SAVING';
        if (!vm.community.data.socialConnections.groupme.triggers) {
            vm.community.data.socialConnections.groupme.triggers = [];
        } else {
            vm.community.data.socialConnections.groupme.triggers.forEach(function(trigger, index) {
                if (trigger.trigger === '' && trigger.response === '') {
                    vm.community.data.socialConnections.groupme.triggers.splice(index, 1);
                }
            });
        }
        vm.newTriggers.forEach(function(trigger) {
            if (trigger.trigger !== '' && trigger.response !== '') {
                vm.community.data.socialConnections.groupme.triggers.push(trigger);
            }
        });
        vm.newTriggers = [
            {
                trigger: '',
                response: ''
            }
        ];
        vm.community.updated().then(function(){
            vm.triggerSaveText = 'SAVE TRIGGERS';
        });
    };

    vm.saveGroup = function(group) {
        group.saveText = 'SAVING';
        GctGroupMe.updateNickname(group.id, group.botNickname).then(function (response) {
            group.saveText = 'SAVE';
        });
    }

    vm.saveInviteTrigger = function() {
        vm.inviteTriggerSaveText = 'SAVING';
        vm.community.data.socialConnections.groupme.autoInviteTrigger = vm.inviteTrigger;
        vm.community.updated().then(function(){
            vm.inviteTriggerSaveText = 'SAVE INVITE TRIGGER';
        });
    }
};

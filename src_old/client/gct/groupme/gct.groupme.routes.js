'use strict';

angular
    .module('gct.groupme')
    .config(GctGroupMeConfig);

function GctGroupMeConfig($stateProvider){
    $stateProvider
        .state('groupme', {
            url: '/groupme',
            templateUrl: '/gct/groupme/gct.groupme.htm',
            controller: 'GctGroupMe',
            controllerAs: 'GctGroupMeVM',
            data: {
                title: 'GroupMe Management',
                securePage: true
            }
        })
}

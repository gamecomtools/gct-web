'use strict';

angular
    .module('gct.signIn')
    .config(GctSignInConfig);

function GctSignInConfig($stateProvider){
    $stateProvider
        .state('sign-in', {
            url: '/sign-in',
            templateUrl: '/gct/sign-in/gct.sign-in.htm',
            controller: 'GctSignIn',
            controllerAs: 'GctSignInVM',
            data: {
                title: 'Sign In',
                fullpage: true
            },
            params: {
                toState: null,
                securePageError: false
            }
        })
}

'use strict';

angular
    .module('gct.signIn')
    .controller('GctSignIn', GctSignInController);

function GctSignInController($state, $stateParams, GctUser){
    var vm = this;

    vm.loading = false;
    vm.error = false;
    vm.loginInfo = {};

    if ($stateParams.securePageError) {
        vm.error = "Login To Continue";
    }

    vm.login = function(){
        if (!vm.loginInfo.username || !vm.loginInfo.password) {
            return vm.error = "Invalid Username/Password";
        }
        vm.error = false;
        vm.loading = true;

        GctUser.login(vm.loginInfo.username, vm.loginInfo.password).then(function(){
            $state.go($stateParams.toState == null ? 'dashboard' : $stateParams.toState);
        }, function(err){
            vm.loading = false;
            vm.error = err;
        });
    }
}

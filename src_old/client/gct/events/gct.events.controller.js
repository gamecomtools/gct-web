'use strict';

angular
    .module('gct.events')
    .controller('GctEvents', GctEventsController);

function GctEventsController($rootScope, GctCommunity, GctEvents){
    var vm = this;

    vm.loading = true;
    vm.email = '';
    vm.events = [];
    vm.needEmail = false;
    vm.refreshEventData = function(){
        if(GctCommunity.data.googleCalendarEmail) {
            vm.loading = true;
            GctEvents.load().then(function(events){
                if (!events.error) {
                    vm.events = events;
                    vm.loading = false;
                }
            });
        } else {
            vm.loading = false;
            vm.needEmail = true;
        }
    };

    $rootScope.$on('gctRevalidateGctDataComplete', function() {
        vm.refreshEventData();
    });
    if(GctCommunity.data.id) {
        vm.refreshEventData();
    }

    vm.registerEmail = function(){
        GctCommunity.data.googleCalendarEmail = vm.email;
        vm.needEmail = false;
        GctCommunity.updated();
        vm.refreshEventData();
    }
}

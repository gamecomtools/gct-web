'use strict';

angular
    .module('gct.events')
    .config(GctEventsConfig);

function GctEventsConfig($stateProvider){
    $stateProvider
        .state('events', {
            url: '/events',
            templateUrl: '/gct/events/gct.events.htm',
            controller: 'GctEvents',
            controllerAs: 'GctEventsVM',
            data: {
                title: 'Event Management',
                securePage: true
            }
        })
}

'use strict';

angular
    .module('gct.events')
    .controller('GctNewEventTypeModalController', GctNewEventTypeModalController);

function GctNewEventTypeModalController($scope, $mdDialog){
    var vm = this;

    vm.eventColors = ['important', 'warning', 'info', 'inverse', 'success', 'special'];
    vm.eventColor = '';
    vm.type = '';

    vm.hide = function() {
        $mdDialog.hide();
    };

    vm.save = function() {
        $mdDialog.hide({
            type: vm.type,
            color: vm.eventColor
        });
    };
}

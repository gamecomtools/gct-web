'use strict';

angular
    .module('gct.events')
    .controller('GctNewEventModalController', GctNewEventModalController);

function GctNewEventModalController($mdDialog, GctCommunity){
    var vm = this;

    vm.time = new Date();
    vm.eventTypes = GctCommunity.data.eventTypes;

    vm.hide = function() {
        $mdDialog.hide();
    };

    vm.save = function() {
        $mdDialog.hide({
            name: vm.name,
            host: vm.host,
            type: vm.type,
            platform: vm.platform,
            date: vm.date,
            time: vm.time
        });
    };

    vm.startControl = {
        opened: false,
        minDate: new Date(),
        options: {
            formatYear: 'yy',
            startingDay: 1
        },
        format: 'shortDate',
        open: function($event){
            $event.preventDefault();
            $event.stopPropagation();

            vm.startControl.opened = true;
        }
    }
}

'use strict';

angular
    .module('gct.reddit')
    .controller('GctReddit', GctReddit);

function GctReddit(GctCommunity, $scope){
    var vm = this;
    vm.community = GctCommunity;

    vm.tabs = [
        {
            name: 'Dynamic Banners',
            view: 'dynamicBanner'
        },
        {
            name: 'Auto Flair',
            view: 'autoFlair'
        },
        {
            name: 'Sidebar Widgets',
            view: 'sidebarWidgets'
        }
    ];

    vm.updateCommunity = function(){
        GctCommunity.updated();
    }
}

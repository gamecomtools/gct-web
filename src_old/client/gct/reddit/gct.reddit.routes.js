'use strict';

angular
    .module('gct.reddit')
    .config(GctRedditConfig);

function GctRedditConfig($stateProvider){
    $stateProvider
        .state('reddit', {
            url: '/reddit',
            templateUrl: '/gct/reddit/gct.reddit.htm',
            controller: 'GctReddit',
            controllerAs: 'GctRedditVM',
            data: {
                title: 'Reddit Management',
                securePage: true
            }
        })
}

'use strict';

angular
    .module('gct.error')
    .config(GctErrorConfig);

function GctErrorConfig($stateProvider){
    $stateProvider
        .state('error-404', {
            url: '/404',
            templateUrl: '/gct/error/gct.error.404.htm',
            data: {
                title: '404 - Page Not Found',
                fullpage: true
            }
        })
}

'use strict';

angular
    .module('gct.register')
    .config(GctRegisterConfig);

function GctRegisterConfig($stateProvider){
    $stateProvider
        .state('register', {
            url: '/register',
            templateUrl: '/gct/register/gct.register.htm',
            data: {
                title: 'Register',
                fullpage: true
            }
        })
        .state('register-start', {
            templateUrl: '/gct/register/start/gct.register.start.htm',
            controller: 'GctRegister',
            controllerAs: 'GctRegisterVM',
            data: {
                title: 'Create A New Community',
                fullpage: true
            }
        })
        .state('register-start.step1', {
            url: '/register/start',
            templateUrl: '/gct/register/start/steps/nameAndType.view.htm'
        })
        .state('register-join', {
            templateUrl: '/gct/register/join/gct.register.join.htm',
            controller: 'GctRegister',
            controllerAs: 'GctRegisterVM',
            data: {
                title: 'Join An Existing Community',
                fullpage: true
            }
        })
        .state('register-join.step1', {
            url: '/register/join',
            templateUrl: '/gct/register/join/steps/nameAndType.view.htm'
        })
}

'use strict';

angular
    .module('gct.register')
    .controller('GctRegister', GctRegisterController);

function GctRegisterController(GctUser, $state, GctCommunity, $rootScope){
    var vm = this;
    vm.regInfo = {};
    vm.error = false;
    vm.communityId = null;
    vm.loading = false;
    vm.userRole = 'na';

    vm.validateUsername = function(){
        if (vm.regInfo.userUsername.indexOf(' ') > -1 ) {
            return 'Usernames Can Not Contain A Space';
        }
        return false;
    };

    vm.createCommunity = function(){
        if (!vm.regInfo.communityName) {
            vm.error = 'Community Name Required';
        } else if (!vm.regInfo.userUsername) {
            vm.error = 'Username Required';
        } else if (!vm.regInfo.userPassword) {
            vm.error = 'Password Required';
        } else if (!vm.regInfo.userPassword2 || vm.regInfo.userPassword != vm.regInfo.userPassword2 ) {
            vm.error = 'Passwords Do Not Match';
        } else if (vm.validateUsername()) {
            vm.error = vm.validateUsername();
        } else {
            vm.userRole = 'leader';
            // Information is valid
            vm.error = false;
            // loading
            vm.loading = true;
            // Already created the community
            // Registration of user failed last time
            if (vm.communityId !== null){
                return registerUser();
            }
            GctCommunity.register(vm.regInfo.communityName).then(function(){
                vm.communityId = GctCommunity.data.id;
                registerUser();
            }, function(err){
                vm.error = err;
                vm.loading = false;
            });
        }
    };

    vm.joinCommunity = function(){
        if (!vm.regInfo.joinID) {
            vm.error = 'Community Name Required';
        } else if (!vm.regInfo.userUsername) {
            vm.error = 'Username Required';
        } else if (!vm.regInfo.userPassword) {
            vm.error = 'Password Required';
        } else if (!vm.regInfo.userPassword2 || vm.regInfo.userPassword != vm.regInfo.userPassword2 ) {
            vm.error = 'Passwords Do Not Match';
        } else if (vm.validateUsername()) {
            vm.error = vm.validateUsername();
        } else {
            // Information is valid
            vm.error = false;
            // loading
            vm.loading = true;
            // Already created the community
            // Registration of user failed last time
            registerUser(vm.regInfo.joinID);
        }
    };

    function registerUser(joinID){
        if (joinID) {
            GctUser.register(
                vm.regInfo.userUsername,
                vm.regInfo.userPassword,
                joinID,
                'na',
                'n-a'
            ).then(function () {
                GctCommunity.load(joinID).then(function(){
                    $state.go('dashboard', {
                        showWelcome: true
                    });
                });
                $rootScope.$broadcast('gctRevalidateGctData');
            }, function (err) {
                vm.error = err;
                vm.loading = false;
            });
        } else {
            GctUser.register(
                vm.regInfo.userUsername,
                vm.regInfo.userPassword,
                vm.communityId,
                vm.userRole,
                'a-1'
            ).then(function () {
                $state.go('dashboard', {
                    showWelcome: true
                });
            }, function (err) {
                vm.error = err;
                vm.loading = false;
            });
        }
    }
}

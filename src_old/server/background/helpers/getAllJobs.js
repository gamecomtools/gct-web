module.exports = function(){
    var defer = q.defer();

    var foundJobs = [];

    var complete = 0;
    queueMod.queue.active(handler);
    queueMod.queue.inactive(handler);
    queueMod.queue.delayed(handler);

    function handler(err, ids) {
        var completedCheck = 0;
        ids.forEach(function(id){
            queueMod.kue.Job.get(id, function(err, job){
                foundJobs.push(job);
                completedCheck++;
                if (completedCheck === ids.length) {
                    complete++;
                    if (complete === 3) {
                        completed();
                    }
                }
            });
        });
        if (ids.length === 0) {
            complete++;
            if (complete === 3) {
                completed();
            }
        }
    }

    function completed(){
        defer.resolve(foundJobs);
    }

    return defer.promise;
};

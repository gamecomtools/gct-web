var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var histogram = require(global.utilsPath + '/histogram');
var common = require(global.utilsPath + '/common');
var parseFindMany = require(global.utilsPath + '/parseFindMany');

module.exports = function(data, logger) {
    var defered = q.defer();

    parseFindMany('gctMembers', {communityId: data.communityId}, 'createdAt', function (err, members) {
        if (parseErrorHandler(logger, err, members)) {
            members = members.results;
            var doneDefer = q.defer();
            var done = doneDefer.promise;
            var xuids = [];
            members.reduce(function (previous, member, index) {
                return previous.then(function (previousValue) {
                    var defer = q.defer();
                    var start = new Date().getTime();

                    if (member.xuid === undefined) {
                        if (member.xboxGametag !== '' && member.xboxGametag !== 'n/a' && member.xboxGametag !== 'N/A') {
                            global.xbox.getXUID(member.xboxGametag).then(function(xuid) {
                                var oi = member.objectId + '';
                                delete member.objectId;
                                delete member.createdAt;
                                delete member.updatedAt;
                                member.xuid = xuid;
                                if (xuid !== '') {
                                    xuids.push(member.xuid);
                                }
                                global.parse.update('gctMembers', oi, member, function (err, response) {
                                    if (!parseErrorHandler(logger, err, members)) {
                                        console.log('PARSE ERROR');
                                        console.log(err);
                                    }
                                    var end = new Date().getTime();
                                    var timeLeft = 1000 - (end - start); // Only 1 request a second
                                    if (index === members.length-1) {
                                        return doneDefer.resolve();
                                    }
                                    if (timeLeft <= 0) {
                                        return defer.resolve();
                                    }
                                    return q.delay(timeLeft).then(function(){
                                        defer.resolve();
                                    });
                                });
                            }, function() {
                                var end = new Date().getTime();
                                var timeLeft = 1000 - (end - start); // Only 1 request a second
                                if (index === members.length-1) {
                                    return doneDefer.resolve();
                                }
                                if (timeLeft <= 0) {
                                    return defer.resolve();
                                }
                                return q.delay(timeLeft).then(function(){
                                    defer.resolve();
                                });
                            });
                        } else {
                            if (index === members.length-1) {
                                return doneDefer.resolve();
                            }
                            defer.resolve();
                        }
                    } else {
                        if (member.xuid !== '') {
                            xuids.push(member.xuid);
                        }
                        if (index === members.length-1) {
                            return doneDefer.resolve();
                        }
                        defer.resolve();
                    }

                    return defer.promise;
                })
            }, q.resolve('start'));

            done.then(function () {
                global.xbox.getPresence(xuids).then(function(result) {
                    var onlineCount = 0;
                    var games = {};
                    result.forEach(function(member) {
                        if (member.state === 'Online') {
                            member.devices.forEach(function(device) {
                                if (device.type === 'MCapensis') {
                                    device.titles.forEach(function (title) {
                                        if (title.state === 'Active' && title.placement === 'Full') {
                                            if (title.name.indexOf('.') > -1) {
                                                title.name = title.name.replace(/\./g, '');
                                            }
                                            if (!games[title.name]) {
                                                games[title.name] = 0;
                                            }
                                            games[title.name]++;
                                        }
                                    });
                                }
                            });
                            onlineCount++;
                        }
                    });
                    var nonGameOnline = 0;
                    for (var i in games) {
                        if (common.nonGames.indexOf(i) > -1) {
                            // This is a non game
                            nonGameOnline += games[i];
                        }
                    }
                    histogram.add(data.communityId, 'xboxOnlineNonGame', nonGameOnline);
                    histogram.add(data.communityId, 'xboxOnline', onlineCount);
                    histogram.add(data.communityId, 'xboxGames', games);
                    var date = new Date();
                    if (date.getHours() % 4 === 0) {
                        queueMod.createJob('xboxOwnedGamesUpdate', {
                            title: data.title,
                            communityId: data.communityId
                        }).done();
                    }
                    defered.resolve();
                }, function () {
                    defered.reject();
                });
            }, function () {
                defered.reject();
            });
        } else {
            defered.reject();
        }
    });

    return defered.promise;
};

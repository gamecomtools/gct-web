var getAllJobs = require(global.helpersPath + '/getAllJobs');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(data, logger) {
    var defer = q.defer();

    logger.info('[REDDIT] Getting /message/unread', 'handlers/redditShareHandler');
    reddit('/message/unread').get().then(function(messages) {
        messages.data.children.forEach(function(message) {
            if (message.data.subject.indexOf('has shared a link with you!') > -1) {
                // This is a share
                var parts = message.data.body.split('\n');

                var link = parts[5].split('?')[0];
                var text = message.data.author + ' shared \'' + parts[3] + '\'';
                var groupID = 13054792;

                global.groupme('groups/' + groupID + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: text
                    }
                }).then(function() {
                    setTimeout(function() {
                        global.groupme('groups/' + groupID + '/messages', 'POST', {
                            message: {
                                source_guid: guid(),
                                text: link
                            }
                        });
                    }, 1000);
                });
                reddit('/api/read_message').post({
                    id: message.data.name
                });
            }
        });
    });
    defer.resolve({
        delay: 60000
    });

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }

    return defer.promise;
};

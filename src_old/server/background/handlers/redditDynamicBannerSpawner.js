var getAllJobs = require(global.helpersPath + '/getAllJobs');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(data, logger){
    var defer = q.defer();

    getAllJobs().then(function(jobs) {
        var communities = [];
        jobs.forEach(function(job) {
            if (job.type === 'redditDynamicBannerUpdate') {
                communities.push(job.data.communityId);
            }
        });
        global.parse.findMany('gctCommunities', {}, function (err, response) {
            if (parseErrorHandler(logger, err, response)) {
                response.results.forEach(function(community) {
                    // Check to see if the community has a redditDynamicBannerUpdate job
                    // and they actually need the job
                    if (communities.indexOf(community.objectId) === -1) {
                        // They do not have a job, do they need it?
                        if (community.socialConnections.reddit) {
                            logger.info('Create job `redditDynamicBannerUpdate` for ' + community.objectId, 'handlers/redditDynamicBannerSpawner');
                            // Yep, probably!
                            queueMod.createJob('redditDynamicBannerUpdate', {
                                title: community.name,
                                communityId: community.objectId
                            }).done();
                        }
                    }
                });
                defer.resolve({
                    delay: 900000
                });
            } else {
                defer.reject();
            }
        });
    });

    return defer.promise;
};

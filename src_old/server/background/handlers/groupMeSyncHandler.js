module.exports = function(data, logger) {
    var defer = q.defer();

    global.groupme('groups/13054792', 'GET').then(function (response) {
        var sync = global.persist.getItem('groupMeSync');
        if (sync === undefined) {
            sync = {
                status: 0
            }
        }
        if (!sync.fixWoops) {
            sync.status = 3;
            sync.fixWoops = true;
        }
        var stop = sync.status+1;
        while(sync.status !== stop) {
            var member = response.response.members[sync.status];
            if (member) {
                var group = null;
                global.groupme('groups', 'POST', {
                    name: 'The Reddit Rebels Community Bot',
                    share: false,
                    description: 'Always Here, Always Helpful!'
                }).then(function(grp) {
                    // Group has been created
                    group = grp;
                    return global.groupme('groups/' + group.response.id + '/members/add', 'POST', {
                        members: [
                            {
                                nickname: member.nickname,
                                user_id: member.user_id,
                                guid: guid()
                            }
                        ]
                    })
                }).then(function(resp) {
                    global.groupme('bots', 'POST', {
                        bot: {
                            name: 'groupmeSync - ' + member.nickname,
                            group_id: group.response.id,
                            callback_url: 'http://gamecomtools.com/api/groupme/callback?type=groupmeSync&communityID=tH15Q9CHZr'
                        }
                    });
                    global.groupme('groups/' + group.response.id + '/messages', 'POST', {
                        "message": {
                            "source_guid": guid(),
                            "text": "Hey there! I am a bot for The Reddit Rebels Community! We wanted to reach out and ensure that you are in all the GroupMe group chats that you would like to be a part of. To get started just say 'Hello'!"
                        }
                    });
                });
            }
            sync.status++;
        }
        global.persist.setItem('groupMeSync', sync);
        defer.resolve({
            delay: 360000
        });
    });

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }

    return defer.promise;
};

var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var histogram = require(global.utilsPath + '/histogram');
var common = require(global.utilsPath + '/common');
var updateCommunity = require(global.utilsPath + '/updateCommunity');
var parseFindMany = require(global.utilsPath + '/parseFindMany');
var request = require('request');
var j = new request.jar();

module.exports = function(data, logger) {
    var defered = q.defer();

    parseFindMany('gctMembers', {communityId: data.communityId}, 'createdAt', function (err, members) {
        if (parseErrorHandler(logger, err, members)) {
            members = members.results;
            var doneDefer = q.defer();
            var done = doneDefer.promise;
            var games = {};
            members.reduce(function (previous, member, index) {
                return previous.then(function (previousValue) {
                    var defer = q.defer();
                    var start = new Date().getTime();
                    if (member.steamId && member.steamId !== '' && member.steamId !== 'INVALID' && member.steamId !== 'INVALID_FLAG') {
                        request.get('http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=' + global.secureData.steam.apiKey + '&steamid=' + member.steamId + '&include_appinfo=1&include_played_free_games=1&format=json', function (err, response, body) {
                            body = JSON.parse(body);
                            var gamesOwned = [];
                            if (body.response.games) {
                                body.response.games.forEach(function (game) {
                                    if (game.playtime_forever > 0) {
                                        gamesOwned.push(game.name);
                                    }
                                });
                                global.parse.update('gctMembers', member.objectId, {ownedGamesSteam: gamesOwned, steamProfilePrivate: false}, function () {
                                });
                                var end = new Date().getTime();
                                var timeLeft = 1000 - (end - start); // Only 1 request a second
                                if (index === members.length - 1) {
                                    return doneDefer.resolve();
                                }
                                if (timeLeft <= 0) {
                                    return defer.resolve();
                                }
                                return q.delay(timeLeft).then(function () {
                                    defer.resolve();
                                });
                            } else {
                                global.parse.update('gctMembers', member.objectId, {steamProfilePrivate: true}, function () {
                                });
                                if (index === members.length-1) {
                                    return doneDefer.resolve();
                                }
                                defer.resolve();
                            }
                        });
                    } else {
                        if (index === members.length-1) {
                            return doneDefer.resolve();
                        }
                        defer.resolve();
                    }

                    return defer.promise;
                })
            }, q.resolve('start'));

            done.then(function () {
                defered.resolve();
            });
        } else {
            defered.reject();
        }
    });

    return defered.promise;
};

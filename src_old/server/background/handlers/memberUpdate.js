var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var memberSyncGoogleSheets = require(global.utilsPath + '/memberSyncGoogleSheets');
var getAllJobs = require(global.helpersPath + '/getAllJobs');
var parseFindMany = require(global.utilsPath + '/parseFindMany');

module.exports = function(data, logger) {
    var defer = q.defer();

    getAllJobs().then(function(jobs) {
        jobs.forEach(function (job) {
            if (job.type === 'xboxPresenceUpdateSpawner') {
                job.complete();
            }
            if (job.type === 'steamPresenceUpdateSpawner') {
                job.complete();
            }
        });
    });
    global.parse.find('gctCommunities', data.communityId, function (err, community) {
        if (parseErrorHandler(logger, err, community)) {
            parseFindMany('gctMembers', {communityId: community.objectId}, 'createdAt', function (err, members) {
                if (parseErrorHandler(logger, err, members)) {
                    members = members.results;
                    community.memberImportSources.forEach(function (importSource) {
                        if (importSource.type === 'googleSheet') {
                            memberSyncGoogleSheets(logger, importSource.sheetURL, importSource.worksheet, importSource.columnTrackingNames, data.communityId, members).then(function (result) {
                                logger.info('Create job `xboxPresenceUpdateSpawner` for ' + community.objectId, 'handlers/memberUpdate');
                                queueMod.createJob('xboxPresenceUpdateSpawner', {
                                    title: community.name,
                                    communityId: community.objectId
                                }).done();
                                logger.info('Create job `steamPresenceUpdateSpawner` for ' + community.objectId, 'handlers/memberUpdate');
                                //queueMod.createJob('steamPresenceUpdateSpawner', {
                                //    title: community.name,
                                //    communityId: community.objectId
                                //}).done();
                                defer.resolve();
                            }, function () {
                                logger.info('Create job `xboxPresenceUpdateSpawner` for ' + community.objectId, 'handlers/memberUpdate');
                                queueMod.createJob('xboxPresenceUpdateSpawner', {
                                    title: community.name,
                                    communityId: community.objectId
                                }).done();
                                logger.info('Create job `steamPresenceUpdateSpawner` for ' + community.objectId, 'handlers/memberUpdate');
                                //queueMod.createJob('steamPresenceUpdateSpawner', {
                                //    title: community.name,
                                //    communityId: community.objectId
                                //}).done();
                                defer.resolve();
                            });
                        }
                    });
                } else {
                    defer.reject();
                }
            });
        } else {
            defer.reject();
        }
    });

    return defer.promise;
};

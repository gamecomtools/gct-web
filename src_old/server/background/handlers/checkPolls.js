module.exports = function(data, logger) {
    var defer = q.defer();

    var polls = global.persist.keys();
    var now = new Date().getTime();
    polls.forEach(function(key) {
        var poll = global.persist.getItem(key);
        if (poll && now >= poll.expires) {
            var message = 'The poll has closed! The question was:\n\n' + poll.question + '\n\n Answers:\n';
            var answers = {};

            if (poll.users) {
                var total = poll.users.length;

                for (var i in poll.answers) {
                    var percent = Math.floor((poll.answers[i] / total) * 100);
                    if (answers[percent] === undefined) {
                        answers[percent] = [];
                    }
                    answers[percent].push(i);
                }
                var keys = Object.keys(answers),
                    i, len = keys.length;

                keys.sort().reverse();

                for (i = 0; i < len; i++) {
                    var k = keys[i];
                    message += '\t' + answers[k] + ' - ' + k + '%\n'
                }
                message += '\nThanks for participating!';
            } else {
                message += '\tNo-one participated';
            }

            global.groupme('groups/' + key.replace('poll-', '') + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: message
                }
            });

            global.persist.removeItemSync(key);
        }
    });

    defer.resolve({
        delay: 60000
    });

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }

    return defer.promise;
};

var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var common = require(global.utilsPath + '/common');
var analytics = require(global.utilsPath + '/analytics');

module.exports = function(data, logger) {
    var defer = q.defer();

    global.parse.find('gctCommunities', {objectId: 'tH15Q9CHZr'}, function (err, resp) {
        if (parseErrorHandler(logger, err, resp)) {
            global.groupme('groups?per_page=100', 'GET').then(function (response) {
                var groups = response.response;
                var now = Math.floor(new Date().getTime() / 1000);
                var eligibleGroups = [];

                groups.forEach(function(group) {
                    if (resp.results[0].socialConnections.groupme.groups[group.group_id]) {
                        if (group.messages.preview.nickname.indexOf('Community') === -1) {
                            if (now - group.messages.last_message_created_at < 3600) {
                                eligibleGroups.push(group.group_id);
                            }
                        }
                    }
                });

                if (eligibleGroups.length > 0) {
                    analytics.bump('connorism');
                    var group = eligibleGroups[Math.floor(Math.random() * eligibleGroups.length)];
                    var sayings = common.connorisms;
                    var rareSayings = ['ya know maybe some of the holocaust victims deserved it', 'Real talk - Panther is a bitch'];
                    var saying = sayings[Math.floor(Math.random() * sayings.length)];

                    var rare = (Math.random() * 100 <= 1);

                    if (rare) {
                        saying = rareSayings[Math.floor(Math.random() * rareSayings.length)];
                    }

                    global.groupme('groups/' + group + '/messages', 'POST', {
                        message: {
                            source_guid: guid(),
                            text: saying
                        }
                    });
                }
            });

            defer.resolve({
                delay: 3600000
            });
        } else {
            defer.reject();
        }
    });

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }

    return defer.promise;
};

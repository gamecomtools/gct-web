var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var histogram = require(global.utilsPath + '/histogram');
var common = require(global.utilsPath + '/common');
var updateCommunity = require(global.utilsPath + '/updateCommunity');
var parseFindMany = require(global.utilsPath + '/parseFindMany');

module.exports = function(data, logger) {
    var defered = q.defer();

    parseFindMany('gctMembers', {communityId: data.communityId}, 'createdAt', function (err, members) {
        if (parseErrorHandler(logger, err, members)) {
            members = members.results;
            var doneDefer = q.defer();
            var done = doneDefer.promise;
            var games = {};
            members.reduce(function (previous, member, index) {
                return previous.then(function (previousValue) {
                    var defer = q.defer();
                    var start = new Date().getTime();
                    if (member.xuid && member.xuid !== '') {
                        global.xbox.getGamesOwned(member.xuid).then(function(gamesOwned) {
                            global.parse.update('gctMembers', member.objectId, {ownedGames: gamesOwned}, function(){});

                            gamesOwned.forEach(function(game) {
                                if (games[game] === undefined) {
                                    games[game] = 0;
                                }
                                games[game]++;
                            });
                            var end = new Date().getTime();
                            var timeLeft = 1000 - (end - start); // Only 1 request a second
                            if (index === members.length-1) {
                                return doneDefer.resolve();
                            }
                            if (timeLeft <= 0) {
                                return defer.resolve();
                            }
                            return q.delay(timeLeft).then(function(){
                                defer.resolve();
                            });
                        }, function() {
                            var end = new Date().getTime();
                            var timeLeft = 1000 - (end - start); // Only 1 request a second
                            if (index === members.length-1) {
                                return doneDefer.resolve();
                            }
                            if (timeLeft <= 0) {
                                return defer.resolve();
                            }
                            return q.delay(timeLeft).then(function(){
                                defer.resolve();
                            });
                        });
                    } else {
                        if (index === members.length-1) {
                            return doneDefer.resolve();
                        }
                        defer.resolve();
                    }

                    return defer.promise;
                })
            }, q.resolve('start'));

            done.then(function () {
                defered.resolve();
            });
        } else {
            defered.reject();
        }
    });

    return defered.promise;
};

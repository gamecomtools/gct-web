var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(data, logger) {
    var defer = q.defer();

    global.parse.findMany('gctCommunities', {}, function (err, response) {
        if (parseErrorHandler(logger, err, response)) {
            response.results.forEach(function (community) {
                if (!community.preProd && community.memberImportSources !== undefined && community.memberImportSources.length > 0) {
                    logger.info('Create job `memberUpdate` for ' + community.objectId, 'handlers/memberUpdateSpawner');
                    // Yep, probably!
                    queueMod.createJob('memberUpdate', {
                        title: community.name,
                        communityId: community.objectId
                    }).done();
                }
            });
            defer.resolve({
                delay: 300000
            });
        } else {
            defer.reject();
        }
    });

    return defer.promise;
};

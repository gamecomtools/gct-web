var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var common = require(global.utilsPath + '/common');
var request = require('request');

module.exports = function(data, logger) {
    var defer = q.defer();

    global.parse.find('gctCommunities', data.communityId, function (err, response) {
        if (parseErrorHandler(logger, err, response)) {
            if (response.socialConnections.groupme) {
                var autoEventGroups = [];
                for (var i in response.socialConnections.groupme.groupData) {
                    if (response.socialConnections.groupme.groupData[i].autoEvents) {
                        autoEventGroups.push(i);
                    }
                }
                if (autoEventGroups.length > 0) {
                    var max_orig = new Date();
                    max_orig.setMinutes(max_orig.getMinutes() + 5);
                    var min_orig = new Date();

                    max = max_orig.toJSON();
                    min = min_orig.toJSON();

                    var url = 'https://www.googleapis.com/calendar/v3/calendars/' + response.googleCalendarEmail + '/events?maxResults=2500&key=' + global.googleKey + '&timeMax=' + max + '&timeMin=' + min + '&orderBy=startTime&singleEvents=true';
                    request.get(url, function (err, resp, body) {
                        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                        body = JSON.parse(body);
                        body.items.forEach(function (event) {
                            // If there is an event occurring it will come back as well
                            // So we have to check again
                            var date = new Date(event.start.dateTime);
                            if (date <= max_orig && date >= min_orig) {
                                if ((event.summary || 'TBD').split(' - ')[0] !== 'STREAM') {
                                    var groupMeType = 'main';
                                    var eventType = event.summary.split(' - ')[0].split(' (')[0].trim();
                                    if (common.groupMeEventMap[eventType]) {
                                        groupMeType = common.groupMeEventMap[eventType];
                                    }
                                    var found = false;
                                    var main = null;
                                    autoEventGroups.forEach(function(groupID) {
                                        if (response.socialConnections.groupme.groups[groupID] === 'main') {
                                            main = groupID;
                                        }
                                        if (response.socialConnections.groupme.groups[groupID] === groupMeType) {
                                            found = true;
                                            global.groupme('conversations/' + groupID + '/events/create', 'POST', {
                                                end_at: event.end.dateTime,
                                                is_all_day: false,
                                                name: (event.summary || 'TBD').split(' - ')[0] + ' Session',
                                                description: "This crew session is hosted by " + ((event.summary || 'TBD').split(' - ')[1] || 'TBD') + ' and begins within 5 minutes.',
                                                start_at: event.start.dateTime,
                                                timezone: body.timeZone
                                            });
                                        }
                                    });
                                    if (!found && main !== null) {
                                        global.groupme('conversations/' + main + '/events/create', 'POST', {
                                            end_at: event.end.dateTime,
                                            is_all_day: false,
                                            name: (event.summary || 'TBD').split(' - ')[0] + ' Session',
                                            description: "This crew session is hosted by " + ((event.summary || 'TBD').split(' - ')[1] || 'TBD') + ' and begins within 5 minutes.',
                                            start_at: event.start.dateTime,
                                            timezone: body.timeZone
                                        });
                                    }
                                }
                            }
                        });
                        defer.resolve();
                    });
                } else {
                    defer.resolve();
                }
            } else {
                defer.resolve();
            }
        } else {
            defer.reject();
        }
    });

    return defer.promise;
};

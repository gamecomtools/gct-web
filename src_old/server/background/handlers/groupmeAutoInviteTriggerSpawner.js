var getAllJobs = require(global.helpersPath + '/getAllJobs');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(data, logger) {
    var defer = q.defer();

    getAllJobs().then(function(jobs) {
        var communities = [];
        jobs.forEach(function(job) {
            if (job.type === 'groupmeAutoInviteTrigger') {
                communities.push(job.data.communityId);
            }
        });
        global.parse.findMany('gctCommunities', {}, function (err, response) {
            if (parseErrorHandler(logger, err, response)) {
                response.results.forEach(function(community) {
                    // Check to see if the community has a groupmeAutoInviteTrigger job
                    // and they actually need the job
                    if (communities.indexOf(community.objectId) === -1) {
                        // They do not have a job, do they need it?
                        if (community.socialConnections.groupme && community.socialConnections.groupme.autoInviteTrigger  && community.socialConnections.groupme.autoInviteTrigger.type !== 'disabled' && !community.preProd) {
                            logger.info('Create job `groupmeAutoInviteTrigger` for ' + community.objectId, 'handlers/groupmeAutoInviteTriggerSpawner');
                            // Yep, probably!
                            queueMod.createJob('groupmeAutoInviteTrigger', {
                                title: community.name,
                                communityId: community.objectId
                            }).done();
                        }
                    }
                });
                defer.resolve({
                    delay: 60000
                });
            } else {
                defer.reject();
            }
        });
    });

    return defer.promise;
};

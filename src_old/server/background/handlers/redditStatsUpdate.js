var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var histogram = require(global.utilsPath + '/histogram');
var updateCommunity = require(global.utilsPath + '/updateCommunity');

module.exports = function(data, logger){
    var defer = q.defer();
    logger.defer = defer;

    global.parse.find('gctCommunities', data.communityId, function (err, response) {
        logger.step();
        if (parseErrorHandler(logger, err, response)) {
            var community = response;
            var subreddit = community.socialConnections.reddit.subreddit;

            reddit('/r/' + subreddit + '/about').get().then(function (aboutData) {
                logger.step();
                community.socialConnections.reddit.subscribers = aboutData.data.subscribers;
                histogram.add(data.communityId, 'redditSubscribers', aboutData.data.subscribers);
                community.socialConnections.reddit.viewers = aboutData.data.accounts_active;
                histogram.add(data.communityId, 'redditViewers', aboutData.data.accounts_active);

                updateCommunity(logger, data.communityId, community).then(function (community) {
                    defer.resolve();
                });
            });
        } else {
            defer.reject();
        }
    });

    return defer.promise;
};

module.exports.steps = 2;

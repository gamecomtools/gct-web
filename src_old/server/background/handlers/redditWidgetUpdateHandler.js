var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var requestErrorHandler = require(global.utilsPath + '/requestErrorHandler');

module.exports = function(data, logger){
    var defer = q.defer();
    logger.defer = defer;

    logger.info('[REDDIT] Getting /r/' + data.subreddit + '/about/stylesheet', 'handlers/redditWidgetUpdateHandler');
    request('https://www.reddit.com/r/' + data.subreddit + '/about/stylesheet', function (err, response, body) {
        if (requestErrorHandler(logger, err, response, body)) {
            logger.step();
            var tmpFileName = '/tmp/' + data.widget + '-' + data.communityId + '.png';
            logger.info('Getting Widget As Image - ' + 'http://' + global.webHost + '/api/widget/' + data.widget + '?communityID=' + data.communityId, 'handlers/redditWidgetUpdateHandler');
            request('http://' + global.webHost + '/api/widget/' + data.widget + '?communityID=' + data.communityId)
                .pipe(fs.createWriteStream(tmpFileName))
                .on('finish', function(){
                    logger.step();
                    logger.info('Uploading Image To Subreddit', 'handlers/redditWidgetUpdateHandler');
                    var imageFile = fs.readFileSync(tmpFileName);
                    reddit.auth().then(function () {
                        return reddit('/r/' + data.subreddit + '/api/upload_sr_img').post({
                            file: Snoocore.file(tmpFileName, 'image/png', imageFile),
                            header: 0,
                            name: data.widget
                        });
                    }).then(function () {
                        logger.step();
                        var css = cheerio.load(body);
                        css = css(".language-css").text();
                        if (css.indexOf("a[href='#" + data.widget + "']") > -1) {
                            // Exists. Get rid of it for an update
                            var regex = new RegExp("a\\[href='#" + data.widget + "'](.*\\n*)+}", "m");
                            css = css.replace(regex, '');
                        }
                        var sizeOf = require('image-size');
                        var dimensions = sizeOf(tmpFileName);

                        var widgetCSS = 'a[href=\'#' + data.widget + '\']{\n';
                        widgetCSS += 'background-image: url(\%\%' + data.widget + '\%\%);\n';
                        widgetCSS += 'width: 100%;\n';
                        widgetCSS += 'height: ' + dimensions.height + 'px;\n';
                        widgetCSS += 'display: block;\n';
                        widgetCSS += 'cursor: default;\n';
                        widgetCSS += 'position: inherit;\n';
                        widgetCSS += 'border: none;\n';
                        widgetCSS += 'box-shadow: none;\n';
                        widgetCSS += 'border-radius: 0;\n';
                        widgetCSS += 'padding: 0;\n';
                        widgetCSS += '}';

                        css += widgetCSS.replace(/\n/g, '');

                        logger.info('Updating Stylesheet', 'socialNetworkUpdateHandlers/reddit-helpers/widgets/gctEventsWidget');
                        reddit('/r/' + data.subreddit + '/api/subreddit_stylesheet').post({
                            stylesheet_contents: encodeURIComponent(css),
                            op: 'save',
                            reason: 'Updating ' + data.widget
                        }).then(function () {
                            defer.resolve();
                        });
                    });
                });
        } else {
            defer.reject();
        }
    });

    return defer.promise;
};

module.exports.steps = 3;

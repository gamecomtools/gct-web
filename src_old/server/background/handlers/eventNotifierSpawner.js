var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(data, logger) {
    var defer = q.defer();

    global.parse.findMany('gctCommunities', {}, function (err, response) {
        if (parseErrorHandler(logger, err, response)) {
            response.results.forEach(function (community) {
                if (community.googleCalendarEmail !== undefined) {
                    logger.info('Create job `eventNotifier` for ' + community.objectId, 'handlers/eventNotifierSpawner');
                    // Yep, probably!
                    queueMod.createJob('eventNotifier', {
                        title: community.name,
                        communityId: community.objectId
                    }).done();
                }
            });
            defer.resolve({
                delay: 300000
            });
        } else {
            defer.reject();
        }
    });

    return defer.promise;
};

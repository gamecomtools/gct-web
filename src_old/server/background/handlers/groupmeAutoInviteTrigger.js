var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(data, logger) {
    var def = q.defer();

    global.parse.find('gctCommunities', data.communityId, function (err, response) {
        if (parseErrorHandler(logger, err, response)) {
            var trigger = response.socialConnections.groupme.autoInviteTrigger;
            var after = null;
            if (trigger.type === 'reddit') {
                var count = 0;
                var maxPages = 4;

                var promiseCheck = function() {
                    count++;
                    if (count < maxPages) {
                        getMessages().then(promiseCheck).catch(function(error) {
                            def.resolve();
                        });
                    } else {
                        def.resolve();
                    }
                };

                return promiseCheck();
            }

            function getMessages() {
                logger.info('[REDDIT] Getting /message/moderator', 'handlers/groupmeAutoInviteTrigger');
                var options = {
                    limit: 100
                };
                if (after !== null) {
                    options.after = after;
                }
                return reddit('/message/moderator').get(options).then(function(messages) {
                    return messagesHandler(messages);
                });
            }

            function messagesHandler(messages) {
                var doneDefer = q.defer();
                var emailRegex = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;

                messages.data.children.reduce(function (previous, message, index) {
                    return previous.then(function (previousValue) {
                        var defer = q.defer();

                        var checkMemberDB = false;
                        if (message.data.replies) {
                            var foundResponse = false;
                            message.data.replies.data.children.forEach(function(reply) {
                                if (
                                    reply.data.author === 'GameComTools' &&
                                    reply.data.body.indexOf('Thanks for your email address! We are still processing your application.') === 0
                                ) {
                                    checkMemberDB = true;
                                }
                                if (
                                    reply.data.author === 'GameComTools' &&
                                    reply.data.body.indexOf('Hey There! I have messaged you on GroupMe to get everything started') === 0
                                ) {
                                    foundResponse = true;
                                }
                            });
                            if (foundResponse) {
                                checkMemberDB = false;
                            }
                        }
                        after = message.data.name;
                        if (typeof message.data.replies !== 'object' || checkMemberDB) {
                            if (message.data.subject === trigger.redditMessageSubject) {
                                var email = message.data.body.match(emailRegex);
                                if (email !== null) {
                                    return startConversation(email[0], message.data.author, message.data.name, checkMemberDB).then(function() {
                                        if (index === messages.data.children.length-1) {
                                            return doneDefer.resolve();
                                        }
                                        defer.resolve();
                                    });
                                } else {
                                    // Send a something seems wrong, so mod mail can manually take care of it
                                    return reddit('/api/comment').post({
                                        text: 'Hey Mods! I need a little help here! I was not able to take care of this one, can a human please take care of it? Thanks!',
                                        thing_id: message.data.name
                                    }).then(function() {
                                        if (index === messages.data.children.length-1) {
                                            return doneDefer.resolve();
                                        }
                                        defer.resolve();
                                    });
                                }
                            }
                        }
                        if (index === messages.data.children.length-1) {
                            return doneDefer.resolve();
                        }
                        defer.resolve();

                        return defer.promise;
                    })
                }, q.resolve('start'));

                return doneDefer.promise;
            }

            function startConversation(email, author, thingID, disableResponse) {
                var defer = q.defer();
                var query = {
                    redditUsername: author
                };

                global.parse.find('gctMembers', query, function (err, member) {
                    if (member.results[0]) {
                        global.parse.update('gctMembers', member.results[0].objectId, {groupMeEmail: email}, function () {
                        });
                    }
                        var group = {};
                        global.groupme('groups', 'POST', {
                            name: response.name + ' Bot - ' + author,
                            share: false,
                            description: 'Welcome to the ' + response.name
                        }).then(function(grp) {
                            // Group has been created
                            group = grp;
                            return global.groupme('groups/' + group.response.id + '/members/add', 'POST', {
                                members: [
                                    {
                                        nickname: author,
                                        email: email,
                                        guid: guid()
                                    }
                                ]
                            })
                        }).then(function(resp) {
                            global.groupme('bots', 'POST', {
                                bot: {
                                    name: 'gmApplication - ' + author,
                                    group_id: group.response.id,
                                    callback_url: 'http://gamecomtools.com/api/groupme/callback?type=gmApplication&communityID=' + data.communityId
                                }
                            });
                            global.groupme('groups/' + group.response.id + '/messages', 'POST', {
                                "message": {
                                    "source_guid": guid(),
                                    "text": "Hey there! I am a bot for " + response.name + '. I am going to help you get into all the correct groups and afterwards am here if you need me. To begin, just reply with \'hello\' and we will get started!'
                                }
                            });
                            reddit('/api/comment').post({
                                text: 'Hey There! I have messaged you on GroupMe to get everything started, please see the group ' + response.name + ' Bot - ' + author + '!',
                                thing_id: thingID
                            }).then(function () {
                                defer.resolve();
                            });
                        });
                    //} else {
                    //    if (!disableResponse) {
                    //        return reddit('/api/comment').post({
                    //            text: 'Thanks for your email address! We are still processing your application. Once approved, I will begin your GroupMe onboarding!',
                    //            thing_id: thingID
                    //        }).then(function () {
                    //            defer.resolve();
                    //        });
                    //    }
                    //    defer.resolve();
                    //}
                });

                return defer.promise;
            }

            function guid() {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)
                        .toString(16)
                        .substring(1);
                }
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                    s4() + '-' + s4() + s4() + s4();
            }
        } else {
            def.reject();
        }
    });

    return def.promise;
};

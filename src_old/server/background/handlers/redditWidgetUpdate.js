var getAllJobs = require(global.helpersPath + '/getAllJobs');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(data, logger) {
    var defer = q.defer();
    logger.defer = defer;

    logger.info('[PARSE] Looking up community', 'handlers/redditWidgetUpdate');
    global.parse.find('gctCommunities', data.communityId, function (err, response) {
        logger.step();
        if (parseErrorHandler(logger, err, response)) {
            var community = response;
            var subreddit = community.socialConnections.reddit.subreddit;

            logger.info('[REDDIT] Getting /r/' + subreddit + '/about', 'handlers/redditWidgetUpdate');
            reddit('/r/' + subreddit + '/about').get().then(function(aboutData) {
                logger.step();
                var widgets = ['gctEventsWidget', 'gctXboxOnlineWidget'];
                var complete = 0;
                widgets.forEach(function(widget) {
                    if (aboutData.data.description_html.indexOf('#' + widget) > -1) {
                        // Community is using this widget
                        var exists = false;
                        getAllJobs().then(function(jobs) {
                            var communities = [];
                            jobs.forEach(function (job) {
                                if (
                                    job.type === 'redditWidgetUpdateHandler' &&
                                    job.data.communityId === data.communityId,
                                    job.data.widget === widget
                                ) {
                                    exists = true;
                                }
                            });
                            if (!exists) {
                                queueMod.createJob('redditWidgetUpdateHandler', {
                                    communityId: data.communityId,
                                    subreddit: subreddit,
                                    widget: widget,
                                    title: community.name + ' - ' + widget
                                }).done();
                            }
                            complete++;
                            if (complete === widgets.length) {
                                defer.resolve();
                            }
                        });
                    } else {
                        complete++;
                        if (complete === widgets.length) {
                            defer.resolve();
                        }
                    }
                });
            });
        } else {
            defer.reject();
        }
    });

    return defer.promise;
};

module.exports.steps = 2;

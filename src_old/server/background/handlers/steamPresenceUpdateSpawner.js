var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var histogram = require(global.utilsPath + '/histogram');
var common = require(global.utilsPath + '/common');
var parseFindMany = require(global.utilsPath + '/parseFindMany');
var request = require('request');
var j = new request.jar();
var userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.152 Safari/537.36';

module.exports = function(data, logger) {
    var defered = q.defer();
    var steamSessionId = null;

    request({
        method: 'get',
        jar: j,
        headers: {
            'User-Agent': userAgent
        },
        uri: 'http://steamcommunity.com/search/'
    }, function (err, response, body) {
        response.headers['set-cookie'].forEach(function(cookie) {
            cookie = cookie.split('; ')[0];
            if (cookie.split('=')[0] === 'sessionid') {
                steamSessionId = cookie.split('=')[1];
            }
        });
        parseFindMany('gctMembers', {communityId: data.communityId}, 'createdAt', function (err, members) {
            if (parseErrorHandler(logger, err, members)) {
                members = members.results;
                var doneDefer = q.defer();
                var done = doneDefer.promise;
                var steamids = [];
                members.reduce(function (previous, member, index) {
                    return previous.then(function (previousValue) {
                        var defer = q.defer();
                        var start = new Date().getTime();

                        if (member.steamUsername && member.steamUsername !== '' && member.steamUsername.toLowerCase() !== 'na' && member.steamUsername.toLowerCase() !== 'n/a') {
                            if (!member.steamId || member.steamId === 'INVALID') {
                                global.steam.resolveVanityURL({vanityurl: member.steamUsername}, function (err, data) {
                                    if (data.steamid) {
                                        steamids.push(data.steamid);
                                        global.parse.update('gctMembers', member.objectId, {steamId: data.steamid}, function (err, response) {
                                            if (!parseErrorHandler(logger, err, members)) {
                                                console.log('PARSE ERROR');
                                                console.log(err);
                                            }
                                            var end = new Date().getTime();
                                            var timeLeft = 1000 - (end - start); // Only 1 request a second
                                            if (index === members.length - 1) {
                                                return doneDefer.resolve();
                                            }
                                            if (timeLeft <= 0) {
                                                return defer.resolve();
                                            }
                                            return q.delay(timeLeft).then(function () {
                                                defer.resolve();
                                            });
                                        });
                                    } else if (data.message && data.message === 'No match') {
                                        console.log(member.steamUsername);
                                        request({
                                            method: 'get',
                                            jar: j,
                                            headers: {
                                                'User-Agent': userAgent
                                            },
                                            uri: 'http://steamcommunity.com/search/SearchCommunityAjax?text=' + member.steamUsername + '&filter=users&sessionid=' + steamSessionId + '&steamid_user=76561198071560161'
                                        }, function (err, response, body) {
                                            body = JSON.parse(body);
                                            var found = false;
                                            body.html.split('<div class="searchPersonaInfo">').forEach(function(data, index) {
                                                if (index > 0) {
                                                    var username = data.split('">')[1].split('</a>')[0].trim();
                                                    if (username.toLowerCase() === member.steamUsername.toLowerCase()) {
                                                        // Found 'Em!
                                                        found = true;
                                                        var parts = data.split('href="http://steamcommunity.com/')[1];
                                                        parts = parts.split('"')[0].split('/');
                                                        if (parts[0] === 'profiles') {
                                                            // This is the steamId
                                                            steamids.push(parts[1]);
                                                            global.parse.update('gctMembers', member.objectId, {steamId: parts[1]}, function (err, response) {
                                                                if (!parseErrorHandler(logger, err, members)) {
                                                                    console.log('PARSE ERROR');
                                                                    console.log(err);
                                                                }
                                                                var end = new Date().getTime();
                                                                var timeLeft = 1000 - (end - start); // Only 1 request a second
                                                                if (index === members.length - 1) {
                                                                    return doneDefer.resolve();
                                                                }
                                                                if (timeLeft <= 0) {
                                                                    return defer.resolve();
                                                                }
                                                                return q.delay(timeLeft).then(function () {
                                                                    defer.resolve();
                                                                });
                                                            });
                                                        } else {
                                                            // Need to get steamId
                                                            global.steam.resolveVanityURL({vanityurl: parts[1]}, function (err, data) {
                                                                if (data.steamid) {
                                                                    steamids.push(data.steamid);
                                                                    global.parse.update('gctMembers', member.objectId, {steamId: data.steamid}, function (err, response) {
                                                                        if (!parseErrorHandler(logger, err, members)) {
                                                                            console.log('PARSE ERROR');
                                                                            console.log(err);
                                                                        }
                                                                        var end = new Date().getTime();
                                                                        var timeLeft = 1000 - (end - start); // Only 1 request a second
                                                                        if (index === members.length - 1) {
                                                                            return doneDefer.resolve();
                                                                        }
                                                                        if (timeLeft <= 0) {
                                                                            return defer.resolve();
                                                                        }
                                                                        return q.delay(timeLeft).then(function () {
                                                                            defer.resolve();
                                                                        });
                                                                    });
                                                                } else {
                                                                    global.parse.update('gctMembers', member.objectId, {steamId: 'INVALID_FLAG'}, function (err, response) {
                                                                        if (!parseErrorHandler(logger, err, members)) {
                                                                            console.log('PARSE ERROR');
                                                                            console.log(err);
                                                                        }
                                                                        var end = new Date().getTime();
                                                                        var timeLeft = 1000 - (end - start); // Only 1 request a second
                                                                        if (index === members.length - 1) {
                                                                            return doneDefer.resolve();
                                                                        }
                                                                        if (timeLeft <= 0) {
                                                                            return defer.resolve();
                                                                        }
                                                                        return q.delay(timeLeft).then(function () {
                                                                            defer.resolve();
                                                                        });
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    }
                                                }
                                            });
                                            if (!found) {
                                                global.parse.update('gctMembers', member.objectId, {steamId: 'INVALID_FLAG'}, function (err, response) {
                                                    if (!parseErrorHandler(logger, err, members)) {
                                                        console.log('PARSE ERROR');
                                                        console.log(err);
                                                    }
                                                    var end = new Date().getTime();
                                                    var timeLeft = 1000 - (end - start); // Only 1 request a second
                                                    if (index === members.length - 1) {
                                                        return doneDefer.resolve();
                                                    }
                                                    if (timeLeft <= 0) {
                                                        return defer.resolve();
                                                    }
                                                    return q.delay(timeLeft).then(function () {
                                                        defer.resolve();
                                                    });
                                                });
                                            }
                                        });
                                    } else {
                                        var end = new Date().getTime();
                                        var timeLeft = 1000 - (end - start); // Only 1 request a second
                                        if (index === members.length - 1) {
                                            return doneDefer.resolve();
                                        }
                                        if (timeLeft <= 0) {
                                            return defer.resolve();
                                        }
                                        return q.delay(timeLeft).then(function () {
                                            defer.resolve();
                                        });
                                    }
                                });
                            } else {
                                if (member.steamId !== 'INVALID' && member.steamId !== 'INVALID_FLAG') {
                                    steamids.push(member.steamId);
                                }
                                if (index === members.length-1) {
                                    return doneDefer.resolve();
                                }
                                defer.resolve();
                            }
                        } else {
                            if (index === members.length-1) {
                                return doneDefer.resolve();
                            }
                            defer.resolve();
                        }

                        return defer.promise;
                    })
                }, q.resolve('start'));

                done.then(function () {
                    var onlineCount = 0;
                    var nonGameOnline = 0;
                    var games = {};

                    var data = {players: []};
                    var retrieved = 0;
                    var getData = function() {
                        if (retrieved == steamids.length-1) {
                            data.players.forEach(function(member) {
                                if (member.personastate === 1) {
                                    if (member.gameid && member.gameextrainfo) {
                                        if (member.gameextrainfo.indexOf('.') > -1) {
                                            member.gameextrainfo = member.gameextrainfo.replace(/\./g, '');
                                        }
                                        if (!games[member.gameextrainfo]) {
                                            games[member.gameextrainfo] = 0;
                                        }
                                        games[member.gameextrainfo]++;
                                    } else {
                                        nonGameOnline++;
                                    }
                                    onlineCount++;
                                }
                            });
                            histogram.add(data.communityId, 'steamOnlineNonGame', nonGameOnline);
                            histogram.add(data.communityId, 'steamOnline', onlineCount);
                            histogram.add(data.communityId, 'steamGames', games);
                            var date = new Date();
                            if (date.getHours() % 4 === 0) {
                                queueMod.createJob('steamOwnedGamesUpdate', {
                                    title: data.title,
                                    communityId: data.communityId
                                }).done();
                            }
                            defered.resolve();
                        } else {
                            var ids = [];
                            steamids.forEach(function(id, index) {
                                if (ids.length < 100 && index > retrieved) {
                                    ids.push(id);
                                    retrieved++;
                                }
                            });
                            global.steam.getPlayerSummaries({steamids: ids.join(',')}, function (err, d) {
                                data.players = data.players.concat(d.players);
                                getData();
                            });
                        }
                    };
                    getData();
                }, function () {
                    defered.reject();
                });
            } else {
                defered.reject();
            }
        });
    });



    return defered.promise;
};

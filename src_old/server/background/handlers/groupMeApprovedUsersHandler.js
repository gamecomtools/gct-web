var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var common = require(global.utilsPath + '/common');

module.exports = function(data, logger) {
    var defer = q.defer();

    global.parse.find('gctCommunities', {}, function (err, response) {
        if (parseErrorHandler(logger, err, response)) {
            response.results.reduce(function (previous, group, index) {
                return previous.then(function (previousValue) {
                    var def = q.defer();

                    if (group.socialConnections && group.socialConnections.groupme) {
                        if (!common.approvedGroupMeUsers[group.objectId]) {
                            common.approvedGroupMeUsers[group.objectId] = [];
                        }

                        var doneDefer = q.defer();
                        var done = doneDefer.promise;
                        var groups = Object.keys(group.socialConnections.groupme.groups);
                        groups.reduce(function (previous, groupID, index) {
                            return previous.then(function (previousValue) {
                                var d = q.defer();

                                var groupType = group.socialConnections.groupme.groups[groupID];
                                if (group.objectId === 'LHhpGZccLK') {
                                    if (common.approvedGroupMeUsers[group.objectId].indexOf('2301629') === -1) {
                                        common.approvedGroupMeUsers[group.objectId].push('2301629');
                                        common.approvedGroupMeUsers[group.objectId].push('24997941');
                                    }
                                    if (index === groups.length-1) {
                                        return doneDefer.resolve();
                                    }
                                    d.resolve();
                                } else if (groupType === 'leadership') {
                                    global.groupme('groups/' + groupID, 'GET').then(function (response) {
                                        if (response.response !== null) {
                                            response.response.members.forEach(function (member) {
                                                if (common.approvedGroupMeUsers[group.objectId].indexOf(member.user_id) === -1) {
                                                    common.approvedGroupMeUsers[group.objectId].push(member.user_id);
                                                }
                                            });
                                        }
                                        d.resolve();
                                        if (index === groups.length-1) {
                                            return doneDefer.resolve();
                                        }
                                    });
                                } else {
                                    if (index === groups.length-1) {
                                        return doneDefer.resolve();
                                    }
                                    d.resolve();
                                }

                                return d.promise;
                            });
                        }, q.resolve('start'));

                        done.then(function () {
                            def.resolve();
                        });
                    } else {
                        def.resolve();
                    }

                    return def.promise;
                })
            }, q.resolve('start'));

            defer.resolve({
                delay: 21600000 //every 6 hours
            });
        } else {
            defer.reject();
        }
    });

    return defer.promise;
};

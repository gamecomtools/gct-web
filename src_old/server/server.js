var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var appRoot = require('app-root-path');
var fs = require('fs');
process.env.env = 'DEV';
var indexHtml = appRoot.path + '/dist/client/index.html';
if (fs.existsSync(appRoot.path + '/client')) {
    process.env.env = 'PROD';
    indexHtml = appRoot.resolve('client/index.html');
}
console.log('Environment: ' + process.env.env);
require('./utils/dependencyServiceSetup')();

if (process.env.env !== 'PROD') {
    global.webHost = 'localhost:3000';
}

// Session setup
var session = require('express-session');
var FileStore = require('session-file-store')(session);
options = {
    path: '/tmp/sessions/'
};
app.set('view engine', 'jade');
app.use(session({
    store: new FileStore(options),
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}));

// Background Runners Setup
setTimeout(function() {
    global.tasker = require('./background')(app);

    app.use(function(req, res, next){
        if(req.url.indexOf('.') == -1 && req.url.indexOf('api') == -1) {
            return res.sendFile(indexHtml);
        }
        next();
    });

    app.use(bodyParser.json());
    require('./api')(app);

    if (fs.existsSync(appRoot.path + '/client')) {
        app.use(express.static(__dirname + '/../'));
    } else {
        app.use(express.static(appRoot.path + '/dist'));
    }

    // Start the server
    setTimeout(function() {
        app.listen(3000, function(){
            console.log('Server Started!');
        });
    }, 1000); //Need to wait for steam api to ready up
}, 5000); //Need to wait for steam api to ready up

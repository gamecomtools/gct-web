module.exports = function(logger, err, response) {
    if (err !== null) {
        if (logger) {
            logger.fatal('PARSE ERR: ' + err, 'parseErrorHandler');
            logger.fatal('PARSE RESPONSE: ' + response, 'parseErrorHandler');
        } else {
            console.log('PARSE ERR: ' + err);
            console.log('PARSE RESPONSE: ' + response);
        }
        return false;
    }
    return true;
};

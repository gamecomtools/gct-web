var cheerio = require('cheerio');
var requestErrorHandler = require(global.utilsPath + '/requestErrorHandler');
var request = require('request');
var requestHelper = require(global.utilsPath + '/requestHelper');
var loggerUtil = require(global.utilsPath + '/logger');
var logger = new loggerUtil('xboxLive');

module.exports = function() {};
module.exports.userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.152 Safari/537.36';
module.exports.client_id = '00000000480FEDC6';
module.exports.token = '';
module.exports.tokenExpires = '';

module.exports.getXUID = function(gamertag, token) {
    var deferred = q.defer();

    var promises = [];
    if (!token) {
        promises.push(module.exports.login());
    }
    q.all(promises).then(function() {
        request({
            method: 'get',
            headers: {
                'User-Agent': module.exports.userAgent,
                'Authorization': token || module.exports.token,
                'x-xbl-contract-version': '2',
                'Content-Type': 'application/json'
            },
            uri: 'https://profile.xboxlive.com/users/gt(' + gamertag + ')/settings?settings=Gamertag'
        }, function (err, response, body) {
            if (requestErrorHandler(undefined, err, response, body)) {
                if (body === '') {
                    return deferred.resolve('');
                }
                body = JSON.parse(body);
                if (body.profileUsers) {
                    deferred.resolve(body.profileUsers[0].id);
                } else {
                    deferred.resolve('');
                }
            } else {
                deferred.resolve('');
            }
        });
    }, function() {
        deferred.resolve('');
    });

    return deferred.promise;
};

module.exports.getFriends = function(xuid, token) {
    var deferred = q.defer();

    var promises = [];
    if (!token) {
        promises.push(module.exports.login());
    }
    q.all(promises).then(function() {
        request({
            method: 'get',
            headers: {
                'User-Agent': module.exports.userAgent,
                'Authorization': token || module.exports.token
            },
            uri: 'https://social.xboxlive.com/users/xuid(' + xuid + ')/people?maxItems=1000'
        }, function (err, response, body) {
            if (requestErrorHandler(undefined, err, response, body)) {
                deferred.resolve(JSON.parse(body));
            } else {
                deferred.reject();
            }
        });
    }, function() {
        deferred.reject();
    });

    return deferred.promise;
};

module.exports.getUsersSettings = function(users, token) {
    var deferred = q.defer();

    var promises = [];
    if (!token) {
        promises.push(module.exports.login());
    }
    q.all(promises).then(function() {
        request({
            method: 'post',
            headers: {
                'User-Agent': module.exports.userAgent,
                'Authorization': token || module.exports.token,
                'x-xbl-contract-version': 2
            },
            uri: 'https://profile.xboxlive.com/users/batch/profile/settings',
            json: {
                "settings": [
                    "Gamertag"
                ],
                "userIds": users
            }
        }, function (err, response, body) {
            if (requestErrorHandler(undefined, err, response, body)) {
                deferred.resolve(body);
            } else {
                deferred.reject();
            }
        });
    }, function() {
        deferred.reject();
    });

    return deferred.promise;
};

module.exports.addFriend = function(clientXuid, friendXuid, token) {
    var deferred = q.defer();

    var promises = [];
    if (!token) {
        promises.push(module.exports.login());
    }
    q.all(promises).then(function() {
        request({
            method: 'put',
            headers: {
                'User-Agent': module.exports.userAgent,
                'Authorization': token || module.exports.token
            },
            uri: 'https://social.xboxlive.com/users/xuid(' + clientXuid + ')/people/xuid(' + friendXuid + ')'
        }, function (err, response, body) {
            if (requestErrorHandler(undefined, err, response, body)) {
                deferred.resolve();
            } else {
                deferred.reject();
            }
        });
    }, function() {
        deferred.reject();
    });

    return deferred.promise;
};

module.exports.getGamesOwned = function(xuid, token) {
    var deferred = q.defer();

    var promises = [];
    if (!token) {
        promises.push(module.exports.login());
    }
    q.all(promises).then(function() {
        request({
            method: 'get',
            headers: {
                'User-Agent': module.exports.userAgent,
                'Authorization': token || module.exports.token,
                'x-xbl-contract-version': 2
            },
            uri: 'https://achievements.xboxlive.com/users/xuid(' + xuid + ')/history/titles'
        }, function (err, response, body) {
            if (requestErrorHandler(undefined, err, response, body)) {
                body = JSON.parse(body);
                var titles = [];
                if (body.titles) {
                    body.titles.forEach(function (title) {
                        if (title.platform === 'Durango' && title.titleType === 'DGame') {
                            titles.push(title.name);
                        }
                    });
                }
                deferred.resolve(titles);
            } else {
                deferred.reject();
            }
        });
    }, function() {
        deferred.reject();
    });

    return deferred.promise;
};

module.exports.removeFriend = function(clientXuid, friendXuid, token) {
    var deferred = q.defer();

    var promises = [];
    if (!token) {
        promises.push(module.exports.login());
    }
    q.all(promises).then(function() {
        request({
            method: 'delete',
            headers: {
                'User-Agent': module.exports.userAgent,
                'Authorization': token || module.exports.token
            },
            uri: 'https://social.xboxlive.com/users/xuid(' + clientXuid + ')/people/xuid(' + friendXuid + ')'
        }, function (err, response, body) {
            if (requestErrorHandler(undefined, err, response, body)) {
                deferred.resolve();
            } else {
                deferred.reject();
            }
        });
    }, function() {
        deferred.reject();
    });

    return deferred.promise;
};

module.exports.getPresence = function(users, token) {
    var deferred = q.defer();

    var promises = [];
    if (!token) {
        promises.push(module.exports.login());
    }
    q.all(promises).then(function() {
        request({
            method: 'post',
            headers: {
                'User-Agent': module.exports.userAgent,
                'Authorization': token || module.exports.token
            },
            uri: 'https://userpresence.xboxlive.com/users/batch',
            json: {
                "levels": "all",
                "users": users
            }
        }, function (err, response, body) {
            if (requestErrorHandler(undefined, err, response, body)) {
                deferred.resolve(body);
            } else {
                deferred.reject();
            }
        });
    }, function() {
        deferred.reject();
    });

    return deferred.promise;
};

module.exports.login = function(username, password) {
    var deferred = q.defer();
    var botLogin = true;
    if (username) {
        // This is not a bot login
        botLogin = false;
    }
    if (botLogin && module.exports.token && new Date().getTime() < module.exports.tokenExpires.getTime()) {
        // Return the valid token
        deferred.resolve(module.exports.token);
    } else {
        // Need to login
        // Get a new cookie jar
        var j = new request.jar();
        var lastUrl = '';
        requestHelper(logger, {
            method: 'get',
            jar: j,
            headers: {
                'User-Agent': module.exports.userAgent
            },
            uri: 'https://account.xbox.com/en-US/Account/Signin?returnUrl=http%3a%2f%2fwww.xbox.com%2fen-US%2f%3flc%3d1033'
        }).then(function(body) {
            if (body.split('ServerData = ')[1] === undefined) {
                // This was not a valid response
                return deferred.reject();
            }
            var serverData = body.split('ServerData = ')[1].split(';</script>')[0];
            serverData = (new Function("return " + serverData))();
            //lastUrl = serverData.urlPost;
            return requestHelper(logger, {
                method: 'post',
                jar: j,
                headers: {
                    'User-Agent': module.exports.userAgent
                },
                uri: serverData.urlPost,
                form: {
                    PPFT: serverData.sFTTag.split('value="')[1].split('"')[0],
                    login: username || module.exports.username,
                    passwd: password || module.exports.password,
                    SI: 'Sign in',
                    type: 11,
                    PPSX: 'P',
                    idsbho: 1,
                    sso: 0,
                    NewUser: 1,
                    LoginOptions: 3,
                    i1: 0,
                    i2: 1,
                    i3: 70089,
                    i4: 0,
                    i7: 0,
                    i12: 1,
                    i13: 0,
                    i14: 611,
                    i15: 1370,
                    i17: 0,
                    i18: '__Login_Strings|1,__Login_Core|1,'
                }
            })
        }).then(function(body) {
            if (body.split('action="')[1] === undefined) {
                // The login was incorrect
                return deferred.reject();
            }
            var url = body.split('action="')[1].split('"')[0];
            var form = {};
            var values = body.split('value="');
            var keys = body.split('name="');
            keys.splice(0, 1);
            var formBody = '';
            values.forEach(function (value, index) {
                if (index !== 0) {
                    value = value.split('"')[0];
                    var key = keys[index].split('"')[0];
                    form[key] = value;
                    formBody += '&' + key + '=' + encodeURIComponent(value);
                }
            });
            formBody = formBody.substring(1);
            lastUrl = url;

            return requestHelper(logger, {
                method: 'post',
                jar: j,
                headers: {
                    'Accept-Language': 'en-US,en;q=0.8,da;q=0.6',
                    'User-Agent': module.exports.userAgent,
                    'Origin': 'https://login.live.com',
                    'Host': 'account.xbox.com',
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                uri: url,
                body: formBody,
                followRedirect: false
            });
        }).then(function(body) {
            var cookies = j.getCookies(lastUrl);
            var rawToken = '';
            var tokenHash = '';
            cookies.forEach(function (cookie) {
                if (cookie.key === 'XBXXtk' + 'http://xboxlive.com') {
                    rawToken = decodeURIComponent(cookie.value);
                    module.exports.tokenExpires = cookie.expires;
                } else if (cookie.key === 'XAnon') {
                    tokenHash = cookie.value;
                }
            });

            var tokenObj = JSON.parse(rawToken);
            if (username) {
                requestHelper(logger, {
                    method: 'get',
                    jar: j,
                    headers: {
                        'User-Agent': module.exports.userAgent
                    },
                    uri: 'https://account.xbox.com/en-US/ShellData'
                }).then(function(body) {
                    body = body.split('(')[1].split(')')[0];
                    body = JSON.parse(body);
                    deferred.resolve({
                        token: 'XBL3.0 x='.concat(tokenObj.UserClaims.uhs, ';', tokenObj.Token),
                        gamertag: body.gamertag
                    });
                });
            } else {
                module.exports.token = 'XBL3.0 x='.concat(tokenObj.UserClaims.uhs, ';', tokenObj.Token);
                deferred.resolve(module.exports.token);
            }
        }).fail(function() {
            console.log('PROMISE FAIL', arguments);
            deferred.reject();
        });
    }
    return deferred.promise;
};

var isDevelopment = true;

module.exports = function (jobId) {
    return {
        defer: undefined,
        steps: 0,
        logs: [],
        toExport: false,
        log: function(type, message, module){
            this.logs.push({
                type: type,
                message: message
            });
            if (type === 'error' || type == 'fatal') {
                this.toExport = true;
            }
            if (isDevelopment) {
                console.log('[' + type.toUpperCase() + '][' + module + '][' + jobId + '] ' + message);
            }
        },
        debug: function(message, module){
            this.log('debug', message, module);
        },
        info: function(message, module){
            this.log('info', message, module);
        },
        error: function(message, module){
            this.log('error', message, module);
        },
        fatal: function(message, module){
            this.log('fatal', message, module);
        },
        access: function(message, module){
            this.log('access', message, module);
        },
        start: function(message, module){
            this.log('start', message, module);
        },
        end: function(message, module){
            this.log('end', message, module);
        },
        failed: function(message, module){
            this.log('failed', message, module);
        },
        step: function(){
            this.log('step', '', '');
            this.steps++;
            var d = {};
            d.steps = this.steps;
            d.data = this.logs[this.logs.length-1];
            this.defer.notify(d);
        },
        complete: function(){
            if (this.toExport) {
                // TODO: Export to log service (Parse????)
            }
        }
    }
};

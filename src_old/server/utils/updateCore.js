var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var Q = require('q');
var merge = require('deepmerge');

module.exports = function(logger, key, updatedData) {
    var deferred = Q.defer();

    logger.info('[PARSE] Looking up key', 'util/updateCore');
    global.parse.find('gctCore', {key: key}, function (err, response) {
        if (parseErrorHandler(logger, err, response)) {
            response = response.results[0];
            if (response === undefined) {
                logger.error('[PARSE] Key Not Found, Inserting', 'util/updateCore');
                global.parse.insert('gctCore', {
                    key: key,
                    value: updatedData
                }, function (err, response) {
                    logger.info('[PARSE] Key Updated', 'util/updateCore');
                    deferred.resolve(updatedData);
                });
            }
            logger.info('[PARSE] Key Found', 'util/updateCore');
            var value = response.value;
            var valueId = response.objectId;

            value = merge(value, updatedData);
            logger.info('[PARSE] Updating Key', 'util/updateCore');

            global.parse.update('gctCore', valueId, {value: value}, function (err, response) {
                logger.info('[PARSE] Key Updated', 'util/updateCore');
                deferred.resolve(value);
            });
        }else {
            deferred.reject('Parse.com Error');
        }
    });

    return deferred.promise;
};

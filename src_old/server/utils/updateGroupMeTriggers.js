var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var Q = require('q');
var merge = require('deepmerge');

module.exports = function() {
    global.parse.findMany('gctCommunities', {}, function (err, response) {
        if (parseErrorHandler(undefined, err, response)) {
            global.groupmeTriggers = {};
            response.results.forEach(function(community) {
                if (community.socialConnections && community.socialConnections.groupme && community.socialConnections.groupme.triggers) {
                    for(var groupID in community.socialConnections.groupme.groups) {
                        if (!global.groupmeTriggers[groupID]) {
                            global.groupmeTriggers[groupID] = [];
                        }
                        community.socialConnections.groupme.triggers.forEach(function(trigger) {
                            global.groupmeTriggers[groupID].push(trigger);
                        });
                    }
                }
            });
        }
    });
};

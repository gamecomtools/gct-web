var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(logger, sheetURL, worksheetName, columnTrackingNames, communityId, existingMembers) {
    var defered = q.defer();
    var sheetKey = sheetURL.split('/d/')[1].split('/')[0];

    var GoogleSpreadsheet = require("google-spreadsheet");
    var my_sheet = new GoogleSpreadsheet(sheetKey);
    my_sheet.setAuth(global.google.username, global.google.password, function(err) {
        if(err) {
            return defered.reject({
                error: 'Internal Error. Please try again later.',
                googleErr: err.toString()
            });
        }
        var analyzedInfo = {};
        my_sheet.getInfo( function( err, sheet_info ){
            if(err) {
                return defered.reject({
                    error: 'Internal Error. Please try again later.',
                    googleErr: err.toString()
                });
            }
            var found = false;
            sheet_info.worksheets.forEach(function(worksheet){
                if (worksheet.id === worksheetName) {
                    found = true;
                    var response = {};
                    worksheet.getCells( function( err, cells ){
                        if(err) {
                            return defered.reject({
                                error: 'Internal Error. Please try again later.',
                                googleErr: err.toString()
                            });
                        }
                        var rows = [];
                        var r = 1;
                        var rowData = [];
                        var maxCols = 0;
                        var lastCol = 0;
                        cells.forEach(function(cell){
                            if (cell.row !== r) {
                                r = cell.row;
                                rows.push(rowData);
                                if (rowData.length > maxCols) {
                                    maxCols = rowData.length;
                                }
                                rowData = [];
                                lastCol = 0;
                            }
                            while(lastCol + 1 != cell.col) {
                                rowData.push('');
                                lastCol++;
                            }
                            rowData.push(cell.value);
                            lastCol++;
                        });
                        var indexMappings = {};
                        columnTrackingNames.forEach(function(value, index) {
                            if (value !== null) {
                                indexMappings[value] = index;
                            }
                        });
                        var doneDefer = q.defer();
                        var done = doneDefer.promise;

                        var chain = rows.reduce(function (previous, item, index) {
                            return previous.then(function (previousValue) {
                                var defer = q.defer();
                                if (index === 0) {
                                    defer.resolve();
                                    if (index === rows.length-1) {
                                        return doneDefer.resolve();
                                    }
                                    return defer.promise;
                                }
                                var start = new Date().getTime();
                                var memberData = {
                                    communityId: communityId
                                };
                                for (var i in indexMappings) {
                                    memberData[i] = item[indexMappings[i]];
                                }

                                var found = false;
                                if (existingMembers) {
                                    existingMembers.every(function(value) {
                                        if (
                                            memberData.xboxGametag === value.xboxGametag &&
                                            memberData.steamUsername === value.steamUsername
                                        ) {
                                            found = true;
                                            return false;
                                        }
                                        return true;
                                    });
                                }

                                if (!found) {
                                    global.parse.insert('gctMembers', memberData, function (err, response) {
                                        if (parseErrorHandler(logger, err, response)) {
                                            var end = new Date().getTime();
                                            var timeLeft = 32 - (end - start);
                                            if (index === rows.length-1) {
                                                return doneDefer.resolve();
                                            }
                                            if (timeLeft <= 0) {
                                                return defer.resolve();
                                            }
                                            return Q.delay(timeLeft).then(function(){
                                                defer.resolve();
                                            });
                                        }else {
                                            return defered.reject({
                                                error: "Unknown Error (Parse)"
                                            });
                                            return defer.reject();
                                        }
                                    });
                                } else {
                                    if (index === rows.length-1) {
                                        return doneDefer.resolve();
                                    }
                                    defer.resolve();
                                }
                                return defer.promise;
                            })
                        }, q.resolve('start'));

                        done.then(function () {
                            defered.resolve({
                                status: 'complete'
                            });
                        });
                    });
                }
            });
            if (!found) {
                return defered.reject({
                    error: 'Internal Error. Please try again later.',
                    googleErr: 'Worksheet Not Found'
                });
            }
        });
    });

    return defered.promise;
};

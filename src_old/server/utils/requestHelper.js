var requestErrorHandler = require(global.utilsPath + '/requestErrorHandler');

module.exports = function(logger, options) {
    var deferred = q.defer();
    var request = require('request');
    options.strictSSL = false;
    request(options, function (err, response, body) {
        if (err === null && (response.statusCode == 200 || response.statusCode == 302)) {
            deferred.resolve(body);
        } else {
            console.log('======================');
            console.log("REQUEST ERROR: " + err);
            console.log("REQUEST CODE: " + response.statusCode);
            console.log('======================');
            deferred.reject();
        }
    });
    return deferred.promise;
};

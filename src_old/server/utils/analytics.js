var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports.bump = function(key){
    var now = new Date();
    key += '-' + now.getMonth() + '_' + now.getFullYear();

    global.parse.find('gctAnalytics', {key: key}, function (err, response) {
        if (parseErrorHandler(null, err, response)) {
            response = response.results[0];
            if (response) {
                global.parse.update('gctAnalytics', response.objectId, {value: response.value+1}, function(){});
            } else {
                global.parse.insert('gctAnalytics', {key: key, value: 1}, function(){});
            }
        }
    });
};

module.exports.add = function(communityID, key, value){
    global.parse.findMany('gctHistogram', {communityId: communityID, key: key}, '-createdAt', 1, function (err, response) {
        if (response.results.length == 0 || response.results[0].value !== value) {
            // Add the value to the histogram
            var historyInfo = {
                communityId: communityID,
                key: key,
                value: value
            };
            if (typeof value !== 'number') {
                global.parse.insert('gctHistogramStrings', historyInfo, function (err, response) {
                });
            } else {
                global.parse.insert('gctHistogram', historyInfo, function (err, response) {
                });
            }
        }
    });
};

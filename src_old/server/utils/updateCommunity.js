var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var Q = require('q');
var merge = require('deepmerge');

module.exports = function(logger, communityID, updatedData, fullUpdate) {
    var deferred = Q.defer();

    logger.info('[PARSE] Looking up community', 'util/updateCommunity');
    global.parse.find('gctCommunities', communityID, function (err, response) {
        if (parseErrorHandler(logger, err, response)) {
            if (response === undefined) {
                logger.error('[PARSE] Community Not Found', 'util/updateCommunity');
                return deferred.reject('Community Not Found');
            }
            logger.info('[PARSE] Community Found', 'util/updateCommunity');
            var community = response;
            delete community.objectId;
            delete community.createdAt;
            delete community.updatedAt;

            delete updatedData.objectId;
            delete updatedData.createdAt;
            delete updatedData.updatedAt;

            if (!fullUpdate) {
                community = merge(community, updatedData);
            } else {
                community = updatedData;
            }
            logger.info('[PARSE] Updating Community', 'util/updateCommunity');
            global.parse.update('gctCommunities', communityID, community, function (err, response) {
                console.log(err);
                console.log(response);
                logger.info('[PARSE] Community Updated', 'util/updateCommunity');
                deferred.resolve(community);
            });
        }else {
            deferred.reject('Parse.com Error');
        }
    });

    return deferred.promise;
};

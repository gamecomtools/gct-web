module.exports = function(logger, req, res){
    if (req.session.user === undefined) {
        logger.error('Auth Required', 'requireAuth');
        res.status(401).send({
            error: "Auth Error"
        });
        return false;
    }
    return true;
};

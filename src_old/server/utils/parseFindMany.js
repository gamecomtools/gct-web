module.exports = function(collection, query, sort, callback) {
    var totalCount = 0;
    var retrieved = 0;
    var returnResults = [];

    var nextSet = function() {
        global.parse.find(collection + '?limit=1000&order=' + sort + '&skip=' + retrieved, query, function(err, response) {
            if (err === null) {
                retrieved += response.results.length;
                returnResults = returnResults.concat(response.results);
                if (retrieved === totalCount) {
                    callback(null, {results: returnResults});
                } else {
                    nextSet();
                }
            } else {
                callback(err, {results: returnResults});
            }
        });
    };

    global.parse.countObjects(collection + '?count=1&limit=0', query, function(err, response) {
        totalCount = response.count;
        nextSet();
    });
};

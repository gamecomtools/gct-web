module.exports = function(logger, req, res, required){
    var sent = false;
    required.forEach(function(value){
        if (req.query[value] === undefined && req.body[value] === undefined) {
            logger.error('Missing Param \'' + value + '\'', 'requiredParams');
            if (!sent) {
                sent = true;
                res.send({
                    error: "Invalid Request"
                });
            }
        }
    });
    return !!!sent;
};

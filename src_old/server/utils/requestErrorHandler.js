module.exports = function(logger, err, response, body) {
    if (err !== null && response.statusCode == 200) {
        logger.fatal('REQUEST ERR: ' + err, 'requestErrorHandler');
        logger.fatal('REQUEST RESPONSE: ' + body, 'requestErrorHandler');
        return false;
    }
    return true
};

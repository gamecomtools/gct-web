var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var xboxLive = require(global.utilsPath + '/xboxLive');

module.exports = function(app) {
    app.get('/api/xboxTest', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/xboxTest', 'get/xboxTest');

        xboxLive.getGamesOwned('2533274845022982').then(function(result) {
            res.send(result);
        });
    });
};

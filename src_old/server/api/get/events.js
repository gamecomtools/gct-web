var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(app) {
    app.get('/api/events', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/events', 'get/events');

        if (requireAuth(logger, req, res)) {
            logger.info('[PARSE] Finding Events By Community', 'get/events');
            var query = {
                communityId: req.session.community
            };
            global.parse.findMany('gctEvents', query, function (err, response) {
                if (parseErrorHandler(logger, err, response)) {
                    res.send(response.results);
                }
            });
        }else{
            logger.complete();
        }
    });
};

var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(app) {
    app.get('/api/memberImport/google/analyze', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/memberImport/google/analyze', 'get/memberImport/google/analyze');

        if (requireAuth(logger, req, res) && requiredParams(logger, req, res, ['sheetUrl'])) {
            var sheetKey = req.query.sheetUrl.split('/d/')[1].split('/')[0];

            var GoogleSpreadsheet = require("google-spreadsheet");
            var my_sheet = new GoogleSpreadsheet(sheetKey);
            my_sheet.setAuth(global.google.username, global.google.password, function(err) {
                if(err) {
                    return res.send({
                        error: 'Internal Error. Please try again later.',
                        googleErr: err.toString()
                    });
                }
                var analyzedInfo = {};
                my_sheet.getInfo( function( err, sheet_info ){
                    if(err) {
                        return res.send({
                            error: 'Internal Error. Please try again later.',
                            googleErr: err.toString()
                        });
                    }
                    res.send(sheet_info);
                });
            });
        }else{
            logger.complete();
        }
    });
};

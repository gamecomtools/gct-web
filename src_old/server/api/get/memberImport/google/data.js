var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(app) {
    app.get('/api/memberImport/google/data', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/memberImport/google/data', 'get/memberImport/google/data');

        if (requireAuth(logger, req, res) && requiredParams(logger, req, res, ['sheetUrl', 'worksheet'])) {
            var sheetKey = req.query.sheetUrl.split('/d/')[1].split('/')[0];

            var GoogleSpreadsheet = require("google-spreadsheet");
            var my_sheet = new GoogleSpreadsheet(sheetKey);
            my_sheet.setAuth(global.google.username, global.google.password, function(err) {
                if(err) {
                    return res.send({
                        error: 'Internal Error. Please try again later.',
                        googleErr: err.toString()
                    });
                }
                var analyzedInfo = {};
                my_sheet.getInfo( function( err, sheet_info ){
                    if(err) {
                        return res.send({
                            error: 'Internal Error. Please try again later.',
                            googleErr: err.toString()
                        });
                    }
                    var found = false;
                    sheet_info.worksheets.forEach(function(worksheet){
                        if (worksheet.id === req.query.worksheet) {
                            found = true;
                            var response = {};
                            worksheet.getCells( function( err, cells ){
                                if(err) {
                                    return res.send({
                                        error: 'Internal Error. Please try again later.',
                                        googleErr: err.toString()
                                    });
                                }
                                response.rows = [];
                                req.query.rows = req.query.rows || 9999;
                                req.query.rows = parseInt(req.query.rows);
                                var r = 1;
                                var rowData = [];
                                var maxCols = 0;
                                var lastCol = 0;
                                cells.forEach(function(cell){
                                    if (response.rows.length < req.query.rows) {
                                        if (cell.row !== r) {
                                            r = cell.row;
                                            response.rows.push(rowData);
                                            if (rowData.length > maxCols) {
                                                maxCols = rowData.length;
                                            }
                                            rowData = [];
                                            lastCol = 0;
                                        }
                                        while(lastCol + 1 != cell.col) {
                                            rowData.push('');
                                            lastCol++;
                                        }
                                        rowData.push(cell.value);
                                        lastCol++;
                                    }
                                });
                                response.rows.forEach(function(row){
                                    while (row.length !== maxCols) {
                                        row.push('');
                                    }
                                });
                                res.send(response);
                            });
                        }
                    });
                    if (!found) {
                        return res.send({
                            error: 'Internal Error. Please try again later.',
                            googleErr: 'Worksheet Not Found'
                        });
                    }
                });
            });
        }else{
            logger.complete();
        }
    });
};

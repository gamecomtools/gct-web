var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(app) {
    app.get('/api/community', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/community', 'get/community');

        if (requireAuth(logger, req, res) && requiredParams(logger, req, res, ['id'])) {
            logger.info('[PARSE] Finding Community By Name', 'get/community');
            global.parse.find('gctCommunities', req.query.id, function (err, response) {
                if (parseErrorHandler(logger, err, response)) {
                    if (response != undefined) {
                        logger.info('[PARSE] Community Found', 'get/community');
                        response.id = response.objectId;
                        delete response.objectId;
                        logger.complete();
                        return res.send(response);
                    }
                    logger.error('Community Not Found', 'get/community');
                    logger.complete();
                    return res.send({
                        error: "Community Not Found"
                    });
                }else{
                    logger.complete();
                    return res.send({
                        error: "Unknown Error (Parse)"
                    });
                }
            });
        }else{
            logger.complete();
        }
    });
};

var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var common = require(global.utilsPath + '/common');
var parseFindMany = require(global.utilsPath + '/parseFindMany');

module.exports = function(app) {
    app.get('/api/histogram', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/histogram', 'get/histogram');

        if (requireAuth(logger, req, res) && requiredParams(logger, req, res, ['community', 'days', 'key', 'max', 'fill'])) {
            logger.info('[PARSE] Finding History By Key', 'get/histogram');
            var d = new Date();
            d.setDate(d.getDate()-req.query.days);
            var query = {
                key: req.query.key,
                communityId: req.query.community,
                createdAt: {
                    $gte: {
                        __type: 'Date',
                        iso: d.toJSON()
                    }
                }
            };
            var collection = 'gctHistogram';
            if (req.query.string === 'true') {
                collection = 'gctHistogramStrings';
            }
            var blackList = common.nonGames.slice(0);
            var sameNames = common.sameNames;
            var allSeries = [];
            parseFindMany(collection, query, 'createdAt', function (err, response) {
                if (parseErrorHandler(logger, err, response)) {
                    if (req.query.max === 'false' || req.query.string === 'true' || (req.query.string === 'false' && req.query.labels !== 'true')) {
                        var data = {
                            labels: [],
                            datasets: [],
                            data: [],
                            series: []
                        };
                        var lastPoint = null;
                        function getTimeString(d) {
                            var h = d.getHours();
                            var m = d.getMinutes();
                            var a = 'AM';
                            if (h >= 12) {
                                a = 'PM';
                                if (h !== 12) {
                                    h = h - 12;
                                }
                            }
                            if (h === 0) {
                                h = 12;
                            }
                            if (m < 10) {
                                m = "0" + m;
                            }

                            return h + ':' + m + ' ' + a;
                        }
                        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                        var average = 0;
                        var averageStore = {};
                        var addingLabel = false;
                        var games = [];
                        var labelCount = -1;
                        response.results.forEach(function(value, index) {
                            for (var i in value.value) {
                                var newKey = i;
                                if (i.indexOf("®") > -1) {
                                    newKey = newKey.replace("®", "");
                                }
                                if (i.indexOf("™") > -1) {
                                    newKey = newKey.replace("™", "");
                                }
                                if (i.indexOf("\n") > -1) {
                                    newKey = newKey.replace("\n", "");
                                }
                                if (sameNames[newKey]) {
                                    newKey = sameNames[newKey];
                                }
                                if (allSeries.indexOf(newKey) === -1 && blackList.indexOf(newKey) == -1) {
                                    allSeries.push(newKey);
                                }
                                if (req.query.series !== undefined && req.query.series !== 'All') {
                                    if (req.query.series !== newKey && blackList.indexOf(newKey) === -1) {
                                        blackList.push(newKey);
                                    }
                                }
                            }
                        });
                        response.results.forEach(function(value, index) {
                            for (var i in value.value) {
                                var newKey = i;
                                if (i.indexOf("®") > -1) {
                                    newKey = newKey.replace("®", "");
                                }
                                if (i.indexOf("™") > -1) {
                                    newKey = newKey.replace("™", "");
                                }
                                if (i.indexOf("\n") > -1) {
                                    newKey = newKey.replace("\n", "");
                                }
                                if (sameNames[newKey]) {
                                    newKey = sameNames[newKey];
                                }
                                if (newKey !== i) {
                                    if (value.value[newKey] === undefined) {
                                        value.value[newKey] = value.value[i];
                                    } else {
                                        value.value[newKey] = value.value[newKey] + value.value[i];
                                    }
                                    delete value.value[i];
                                }
                            }
                            if (req.query.labels === 'true') {
                                data.labels.push(value.createdAt);
                            } else {
                                if (req.query.labels === 'hourly') {
                                    var d = new Date(value.createdAt);
                                    if (lastPoint == null || d.getHours() !== lastPoint.getHours()) {
                                        lastPoint = d;
                                        if (lastPoint != null) {
                                            var label = lastPoint.getMonth() + 1 + '/' + lastPoint.getDate() + ' ' + getTimeString(lastPoint);
                                            data.labels.push(label);
                                            addingLabel = true;
                                            labelCount++;
                                        }
                                    } else {
                                        if (req.query.string === 'true' && req.query.max === 'false') {
                                            data.labels.push('');
                                        }
                                        addingLabel = false;
                                    }
                                } else if (req.query.labels === 'daily') {
                                    var d = new Date(value.createdAt);
                                    if (lastPoint == null || d.getDate() !== lastPoint.getDate()) {
                                        lastPoint = d;
                                        if (lastPoint != null) {
                                            var label = lastPoint.getMonth() + 1 + '/' + lastPoint.getDate();
                                            data.labels.push(label);
                                            addingLabel = true;
                                            labelCount++;
                                        }
                                    } else {
                                        if (req.query.string === 'true' && req.query.max === 'false') {
                                            data.labels.push('');
                                        }
                                        addingLabel = false;
                                    }
                                } else {
                                    if (req.query.string === 'true' && req.query.max === 'false') {
                                        data.labels.push('');
                                    }
                                    addingLabel = false;
                                }
                            }
                            if (req.query.string === 'true') {
                                var hasAddedBlacklist = false;
                                for (var i in value.value) {
                                    if ((blackList.indexOf(i) == -1 && req.query.blackListOnly === undefined) || (req.query.blackListOnly === 'true' && blackList.indexOf(i) > -1)) {
                                        if (games.indexOf(i) === -1) {
                                            games.push(i);
                                        }
                                        if (data.datasets[games.indexOf(i)] == undefined) {
                                            data.datasets[games.indexOf(i)] = {
                                                label: i,
                                                data: []
                                            }
                                        }
                                        if (req.query.max === 'false') {
                                            data.datasets[games.indexOf(i)].data[index] = value.value[i];
                                        } else {
                                            if (averageStore[i] === undefined) {
                                                averageStore[i] = 0;
                                            }
                                            if (averageStore[i] < value.value[i]) {
                                                averageStore[i] = value.value[i];
                                            }
                                            if (addingLabel) {
                                                data.datasets[games.indexOf(i)].data[labelCount] = averageStore[i];
                                                averageStore[i] = 0;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (req.query.labels === 'true') {
                                    data.data.push(value.value);
                                } else {
                                    if (req.query.max === 'false') {
                                        var div = true;
                                        if (average === 0) {
                                            div = false;
                                        }
                                        average += value.value;
                                        if (div) {
                                            average /= 2;
                                        }
                                    } else {
                                        if (average < value.value) {
                                            average = value.value;
                                        }
                                    }
                                    if (addingLabel) {
                                        data.data.push(average);
                                        average = 0;
                                    }
                                }
                            }
                        });
                        if (req.query.string === 'true') {
                            data.datasets.forEach(function (ds) {
                                data.series.push(ds.label);
                                data.data.push(ds.data);
                            });
                        }
                        delete data.datasets;
                        data.data.forEach(function(d, index) {
                            var i = 0;
                            while (i < data.labels.length) {
                                if (d[i] === undefined) {
                                    data.data[index][i] = 0;
                                }
                                i++;
                            }
                        });
                        if (typeof data.data[0] === 'object') {
                            for (var i = data.data.length; i--;) {
                                var d = data.data[i];
                                var above = false;
                                d.forEach(function (dd) {
                                    if (!req.query.threshold || dd >= parseInt(req.query.threshold)) {
                                        above = true;
                                    }
                                });
                                if (!above && (req.query.series === undefined || req.query.series === 'All')) {
                                    data.data.splice(i, 1);
                                    data.series.splice(i, 1);
                                } else if (!above && req.query.series !== undefined && req.query.series !== 'All') {
                                    data.data[i] = [];
                                    var j = 0;
                                    while (j < data.labels.length) {
                                        data.data[i].push(0);
                                        j++;
                                    }
                                }
                            }
                        }
                        data.allSeries = allSeries;
                        data.allSeries.sort(function(a, b){
                            if(a < b) return -1;
                            if(a > b) return 1;
                            return 0;
                        });
                        res.send(data);
                    } else {
                        logger.info('Determining Daily Max', 'get/histogram');
                        var results = [];
                        var currentDate = '';
                        response.results.forEach(function(value){
                            var date = new Date(value.createdAt);
                            date.setHours(0);
                            date.setMinutes(0);
                            date.setSeconds(0);
                            date.setMilliseconds(0);
                            if (currentDate == '' || date.toJSON() !== currentDate.toJSON()) {
                                results.push({
                                    date: date.toJSON(),
                                    value: value.value
                                });
                                currentDate = new Date(date);
                            } else {
                                if (req.query.average) {
                                    results[results.length - 1].value = (results[results.length - 1].value + value.value) / 2;
                                } else {
                                    if (results[results.length - 1].value < value.value) {
                                        results[results.length - 1].value = value.value;
                                    }
                                }
                            }
                        });
                        if (req.query.fill === 'true') {
                            logger.info('Filling Missing Days', 'get/histogram');
                            var d = new Date();
                            // if data is missing for today
                            var lastDateDay = new Date(results[results.length - 1].date);
                            if(lastDateDay.getDate() != d.getDate()) {
                                var date = d.getDate();
                                var parseDate = d.toJSON();
                                var data = {
                                    date: parseDate,
                                    value: results[results.length - 1].value
                                };
                                results.push(data);
                            }
                            // Take care of filling days in past that data is missing for
                            for (var i = results.length - 1; i !== 0; i--) {
                                var nextResult = results[i - 1];
                                var currrentDateDay = new Date(results[i].date);
                                currrentDateDay.setHours(0);
                                currrentDateDay.setMinutes(0);
                                currrentDateDay.setSeconds(0);
                                currrentDateDay.setMilliseconds(0);
                                var nextDateDay = new Date(nextResult.date);
                                nextDateDay.setHours(0);
                                nextDateDay.setMinutes(0);
                                nextDateDay.setSeconds(0);
                                nextDateDay.setMilliseconds(0);
                                var oneDayOffset = (24*60*60*1000);
                                if (nextDateDay < currrentDateDay) {
                                    while (currrentDateDay.getTime() !== nextDateDay.getTime()) {
                                        currrentDateDay.setTime(currrentDateDay.getTime() - oneDayOffset);
                                        if (currrentDateDay.getTime() !== nextDateDay.getTime()) {
                                            results.splice(i, 0, {
                                                date: currrentDateDay.toJSON(),
                                                value: nextResult.value
                                            });
                                        }
                                    }
                                }
                            }
                            // Now fill out the days missing to get to the days
                            var missing = parseInt(req.query.days) - results.length;
                            if (missing > 0) {
                                for(var i = 0; i !== missing; i++){
                                    var d = new Date(results[0].date);
                                    var oneDayOffset = (24*60*60*1000);
                                    d.setTime(d.getTime() - oneDayOffset);
                                    var parseDate = d.toJSON();
                                    results.unshift({
                                        date: parseDate,
                                        value: 0
                                    });
                                }
                            }

                            results.forEach(function(value) {
                                value.date = value.date.split('T')[0].replace('-', '/').split('/')[1].replace('-', '/');
                            });

                            if (results.length > req.query.days) {
                                var tooMany = results.length - req.query.days;
                                results.splice(0, tooMany);
                            }
                        }
                        res.send(results);
                    }
                    logger.complete();
                }
            });
        }else{
            logger.complete();
        }
    });
};

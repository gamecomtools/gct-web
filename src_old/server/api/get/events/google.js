var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var request = require('request');

module.exports = function(app) {
    app.get('/api/events/google', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/events/google', 'get/events/google');

        //if (requireAuth(logger, req, res)) {
            logger.info('[PARSE] Finding Community By ID', 'get/events/google');
            var community = req.query.community || req.session.community;
            global.parse.find('gctCommunities', community, function (err, response) {
                if (parseErrorHandler(logger, err, response)) {
                    if (response.googleCalendarEmail) {

                        var min = new Date().toJSON();
                        var maxevents = req.query.max || 2500;
                        var url = 'https://www.googleapis.com/calendar/v3/calendars/' + response.googleCalendarEmail + '/events?maxResults=' + maxevents + '&key=' + global.googleKey + '&orderBy=startTime&singleEvents=true&timeMin=' + min;
                        request.get(url, function(err, response, body) {
                            var cal = JSON.parse(body);
                            cal.items.forEach(function(event){
                                event.timeZone = cal.timeZone;
                                delete event.kind;
                                delete event.etag;
                                delete event.creator;
                                delete event.organizer;
                                event.start = event.start.dateTime;
                                event.end = event.end.dateTime;
                                delete event.iCalUID;
                                delete event.sequence;

                                var titleParts = event.summary.match(/\[(.*)\] (.*) - (.*)/);
                                if (titleParts !== null) {
                                    var game = titleParts[2].split(' (');
                                    event.data = {
                                        platform: titleParts[1],
                                        game: game[0],
                                        info: (game[1] || '').replace(')', ''),
                                        host: titleParts[3]
                                    };
                                }
                            });
                            res.send(cal.items);
                        });

                    } else {
                        return res.send({
                            error: "Invalid Google Calendar Address"
                        });
                    }
                }else{
                    logger.complete();
                    return res.send({
                        error: "Unknown Error (Parse)"
                    });
                }
            });
        //}else{
        //    logger.complete();
        //}
    });
};

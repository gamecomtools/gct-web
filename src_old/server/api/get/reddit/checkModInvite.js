var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(app) {
    app.get('/api/reddit/checkModInvite', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/reddit/checkModInvite', 'get/reddit/checkModInvite');

        if (requireAuth(logger, req, res)) {
            logger.info('[REDDIT] Authenticate', 'get/reddit/checkModInvite');
            reddit.auth().then(function () {
                logger.info('[REDDIT] Getting Unread Messages', 'get/reddit/checkModInvite');
                // Make an OAuth call to show that it is working
                return reddit('/message/unread').get();
            }).then(function (data) {
                if (data.data.children.length === 0) {
                    logger.info('[REDDIT] No Unread Messages', 'get/reddit/checkModInvite');
                    return res.send({status: 'noInvites'});
                }
                if (data.data.children[0].data.distinguished !== 'moderator') {
                    logger.info('[REDDIT] No Unread Moderator Messages', 'get/reddit/checkModInvite');
                    return res.send({status: 'noInvites'});
                }
                logger.info('[REDDIT] Moderator Message Found', 'get/reddit/checkModInvite');
                res.send({
                    status: 'invite',
                    subreddit: data.data.children[0].data.subreddit,
                    name: data.data.children[0].data.name
                });
            }, function () {
                logger.info('[REDDIT] Error Getting Messages', 'get/reddit/checkModInvite');
                return res.send({status: 'noInvites'});
            });
        }
    });
};

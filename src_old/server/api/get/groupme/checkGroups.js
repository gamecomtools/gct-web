var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var updateCommunity = require(global.utilsPath + '/updateCommunity');
var common = require(global.utilsPath + '/common');

module.exports = function(app) {
    app.get('/api/groupme/checkGroups', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/groupme/checkGroups', 'get/groupme/checkGroups');

        if (requireAuth(logger, req, res)) {
            logger.info('[PARSE] Getting Core', 'get/groupme/checkGroups');
            global.parse.find('gctCore', {key: 'knownGroupMeGroups'}, function (err, response) {
                if (parseErrorHandler(logger, err, response)) {
                    var knownGroups = response.results[0].value.groups;

                    logger.info('[GROUPME] Getting Group Index', 'get/groupme/checkGroups');
                    global.groupme('groups?per_page=100', 'GET').then(function (response) {
                        var groups = response.response;
                        var resp = [];

                        groups.forEach(function(group, index){
                            if(knownGroups.indexOf(group.group_id) === -1) {
                                resp.push(group);
                            }
                        });
                        res.send({
                            groups: resp,
                            games: common.groupMeGameMap,
                            gameTypes: common.groupMeGameTypes
                        });
                    });
                }
            });
        }
    });
};

var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var updateCommunity = require(global.utilsPath + '/updateCommunity');

module.exports = function(app) {
    app.get('/api/groupme/activate', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/groupme/activate', 'get/groupme/activate');

        if (requireAuth(logger, req, res)) {
            logger.info('Updating Community', 'get/groupme/activate');
            updateCommunity(logger, req.session.community, {
                socialNetworks: ['groupme'],
                socialConnections: {
                    groupme: {}
                }
            }).then(function(community){
                res.send(community);
            });
        }
    });
};

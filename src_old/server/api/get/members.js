var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var parseFindMany = require(global.utilsPath + '/parseFindMany');

module.exports = function(app) {
    app.get('/api/members', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/members', 'get/members');

        if (requireAuth(logger, req, res)) {
            logger.info('[PARSE] Finding Members By Community', 'get/members');
            var query = {
                communityId: req.session.community
            };
            parseFindMany('gctMembers', query, 'createdAt', function (err, response) {
                if (parseErrorHandler(logger, err, response)) {
                    res.send(response.results);
                }
            });
        }else{
            logger.complete();
        }
    });
};

var loggerUtil = require(global.utilsPath + '/logger');

module.exports = function(app) {
    app.get('/api/widget/:widget', function(req, res, next){
        var webshot = require('webshot');
        var fs      = require('fs');

        // TODO: Make the widget width dynamic based on stylesheet
        var options = {
            windowSize: {
                width: 268,
                height: 1
            },
            shotSize: {
                width: 'window',
                height: 'all'
            }
        };

        var query = "";
        for(var i in req.query){
            query += i + '=' + req.query[i] + '&';
        }

        console.log('[WEBSHOT] Getting: http://' + global.webHost + '/api/widget/source/' + req.params.widget + '?' + query);

        var tmpFileName = '/tmp/' + req.params.widget + '-' + req.query.communityID + '.png';
        webshot('http://' + global.webHost + '/api/widget/source/' + req.params.widget + '?' + query, tmpFileName, options, function(err) {
            if (err !== null) {
                console.log('[WEBSHOT] ERR: ' + err);
            } else {
                res.sendFile(tmpFileName);
            }
        });
    });
};

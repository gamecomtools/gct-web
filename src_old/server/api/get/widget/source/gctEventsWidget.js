var request = require('request');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var loggerUtil = require(global.utilsPath + '/logger');

module.exports = function(app) {
    app.get('/api/widget/source/gctEventsWidget', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/widget/source/gctEventsWidget', 'get/widget/source/gctEventsWidget');
        if (req.query.communityID === undefined) {
            return res.send('Invalid Community ID');
        }
        var events = {
            Monday: [],
            Tuesday: [],
            Wednesday: [],
            Thursday: [],
            Friday: [],
            Saturday: [],
            Sunday: []
        };

        var days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday', 'Sunday'];
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var d = new Date();
        var day = d.getDay(),
            diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
        var monday_orig = new Date(d.setDate(diff));
        var sunday_orig = new Date(monday_orig.toJSON());
        sunday_orig.setDate(monday_orig.getDate()+7);
        sunday_orig.setHours(23);

        var monday = monday_orig.toJSON().split('T')[0] + 'T00:00:00.000Z';
        var sunday = sunday_orig.toJSON().split('T')[0] + 'T23:59:59.000Z';

        var daysDate = {};
        var d = new Date(monday_orig);
        var i = 0;
        while(d.getDate() !== sunday_orig.getDate()) {
            daysDate[days[i]] = months[d.getMonth()]+ ' ' + getDateString(d.getDate());
            i++;
            d.setDate(d.getDate() + 1);
        }

        days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        global.parse.find('gctCommunities', req.query.communityID, function (err, response) {
            if (parseErrorHandler(logger, err, response)) {
                if (response.googleCalendarEmail) {
                    var url = 'https://www.googleapis.com/calendar/v3/calendars/' + response.googleCalendarEmail + '/events?maxResults=2500&key=' + global.googleKey + '&timeMax=' + sunday + '&timeMin=' + monday + '&orderBy=startTime&singleEvents=true';
                    request.get(url, function (err, response, body) {
                        body = JSON.parse(body);
                        body.items.forEach(function(event){
                            var start = new Date(event.start.dateTime);
                            var end = new Date(event.end.dateTime);
                            if(start >= monday_orig && start <= sunday_orig) {
                                var sTime = getTimeString(start);
                                var eTime = getTimeString(end);

                                events[days[start.getDay()]].push({
                                    date: months[start.getMonth()]+ ' ' + getDateString(start.getDate()),
                                    time: sTime + '-' + eTime,
                                    title: (event.summary || 'TBD').split(' - ')[0],
                                    host: (event.summary || 'TBD').split(' - ')[1] || 'TBD'
                                });
                            }
                        });
                        render();
                    });
                } else {
                    render();
                }

                function render(){
                    res.render(__dirname + '/gctEventsWidget', {
                        days: daysDate,
                        events: events,
                        stylesheet: "https://www.reddit.com/r/" + response.socialConnections.reddit.subreddit + "/stylesheet/"
                    });
                }
            } else {
                res.setStatus(500);
            }
        });
    });



    function getTimeString(d) {
        var h = d.getHours();
        var m = d.getMinutes();
        var a = 'AM';
        if (h >= 12) {
            a = 'PM';
            if (h !== 12) {
                h = h - 12;
            }
        }
        if (h === 0) {
            h = 12;
        }
        if (m < 10) {
            m = "0" + m;
        }

        return h + ':' + m + ' ' + a;
    }

    function getDateString(d) {
        if(d>3 && d<21) return d+'th'; // thanks kennebec
        switch (d % 10) {
            case 1:  return d+'st';
            case 2:  return d+'nd';
            case 3:  return d+'rd';
            default: return d+'th';
        }
    }
};

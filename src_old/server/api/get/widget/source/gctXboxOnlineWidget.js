var request = require('request');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var loggerUtil = require(global.utilsPath + '/logger');

module.exports = function(app) {
    app.get('/api/widget/source/gctXboxOnlineWidget', function(req, res, next) {
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/widget/source/gctXboxOnlineWidget', 'get/widget/source/gctXboxOnlineWidget');
        if (req.query.communityID === undefined) {
            return res.send('Invalid Community ID');
        }
        var query = {
            key: 'xboxOnline',
            communityId: req.query.community
        };
        global.parse.find('gctCommunities', req.query.communityID, function (err, community) {
            if (parseErrorHandler(logger, err, community)) {
                global.parse.findMany('gctHistogram', query, '-createdAt', 1, function (err, resp) {
                    query.key = 'xboxOnlineNonGame';
                    global.parse.findMany('gctHistogram', query, '-createdAt', 1, function (err, response) {
                        res.render(__dirname + '/gctXboxOnlineWidget', {
                            onlineUsers: (resp.results[0].value - response.results[0].value),
                            onlineUsersNonGame: response.results[0].value,
                            stylesheet: "https://www.reddit.com/r/" + community.socialConnections.reddit.subreddit + "/stylesheet/"
                        });
                    });
                });
            }
        });
    });
};

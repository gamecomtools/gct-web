var requiredParams = require(global.utilsPath + '/requiredParams');
var loggerUtil = require(global.utilsPath + '/logger');
var common = require(global.utilsPath + '/common');

module.exports = function(app) {
    app.get('/api/twitchStream/streamReady', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/twitchStream/unpublish', 'post/twitchStream/streamReady');

        if (requiredParams(logger, req, res, ['name'])) {
            if (common.currentlyStreaming[req.query.name].game === 'N/A') {
                res.send({status: false});
            } else {
                res.send({status: true});
            }
        }
    });
};

var requiredParams = require(global.utilsPath + '/requiredParams');
var loggerUtil = require(global.utilsPath + '/logger');
var common = require(global.utilsPath + '/common');

module.exports = function(app) {
    app.get('/api/twitchStream/validate', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/twitchStream/validate', 'post/twitchStream/validate');

        if (requiredParams(logger, req, res, ['name'])) {
            if (common.streamers[req.query.name]) {
                common.streamCount++;
                startGroup(common.streamers[req.query.name], req.query.name).then(function(groupID) {
                    global.groupme('groups/' + groupID + '/messages', 'GET', {
                        limit: 1
                    }).then(function(messages) {
                        if (messages.response.messages[0].system) {
                            // This is the first time the user has been here
                            return global.groupme('groups/' + groupID + '/messages', 'POST', {
                                "message": {
                                    "source_guid": guid(),
                                    "text": "Hey there! Glad you would like to stream with us! Since you are new here let me give you a list of commands you can use to control your portion of the stream:\n\n" +
                                        "Define the game you are playing or switching to:\n\t!game GAME NAME\n-------------\n" +
                                        "Turn on/off groupme <-> twitch chat integration:\n\t!chat on/off\n-------------\n" +
                                        "Define a sponsor for the current stream session: (Not Yet Implemented)\n\t!sponsor SPONSORSHIP INFO"
                                }
                            });
                        }
                        return q.resolve();
                    }).then(function() {
                        if (common.streamCount <= 4) {
                            common.currentlyStreaming[req.query.name] = {
                                game: 'N/A'
                            };
                            return q.delay(1500).then(function() {
                                return global.groupme('groups/' + groupID + '/messages', 'POST', {
                                    "message": {
                                        "source_guid": guid(),
                                        "text": "Let's get your stream started! Please use '!game GAME NAME' so I can set the game. NOTE: This must be done before your connection will be established."
                                    }
                                });
                            });
                        }
                        common.streamCount--;
                        // Currently streaming the max number of people
                        global.groupme('groups/' + group.response.id + '/messages', 'POST', {
                            "message": {
                                "source_guid": guid(),
                                "text": "There are currently 4 people streaming. This is our max at this time! Please try again later."
                            }
                        });
                        res.status(500).send({error: "Max connections"});
                    }).then(function(resp) {
                        if (common.streamCount > 1) {
                            global.groupme('groups/' + group.response.id + '/messages', 'POST', {
                                "message": {
                                    "source_guid": guid(),
                                    "text": "Also, there are " + common.streamCount-1 + " other " + ((common.streamCount-1 === 1) ? 'person' : 'people') + " streaming currently, so you will be joining them!"
                                }
                            });
                        }
                        res.status(200).send({});
                    });
                });
            } else {
                res.status(401).send({error: 'Not Authorized'});
            }
        }

        function startGroup(email, author, res) {
            var defer = q.defer();

            global.groupme('groups', 'GET').then(function(resp) {
                var found = false;
                resp.response.forEach(function(data) {
                    if (data.name == 'Twitch Stream Bot - ' + author) {
                        defer.resolve(data.id);
                        found = true;
                    }
                });
                if (!found) {
                    var group = {};
                    // Create the group
                    global.groupme('groups', 'POST', {
                        name: 'Twitch Stream Bot - ' + author,
                        share: false,
                        description: 'Helping you with all of your streaming needs!'
                    }).then(function(grp) {
                        // Group has been created
                        group = grp;
                        return global.groupme('groups/' + group.response.id + '/members/add', 'POST', {
                            members: [
                                {
                                    nickname: author,
                                    user_id: email,
                                    guid: guid()
                                }
                            ]
                        })
                    }).then(function(resp) {
                        global.groupme('bots', 'POST', {
                            bot: {
                                name: 'Twitch Stream Bot Bot - ' + author,
                                group_id: group.response.id,
                                callback_url: 'http://gamecomtools.com/api/groupme/callback?type=twitchStreamer'
                            }
                        });
                        setTimeout(function() {
                            defer.resolve(group.response.id);
                        }, 1500);
                    });
                }
            });

            return defer.promise;
        }

        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }
    });
};

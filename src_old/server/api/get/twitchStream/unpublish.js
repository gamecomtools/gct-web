var requiredParams = require(global.utilsPath + '/requiredParams');
var loggerUtil = require(global.utilsPath + '/logger');
var common = require(global.utilsPath + '/common');

module.exports = function(app) {
    app.get('/api/twitchStream/unpublish', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/twitchStream/unpublish', 'post/twitchStream/unpublish');

        if (requiredParams(logger, req, res, ['name'])) {
            res.send({});

            delete common.currentlyStreaming[req.query.name];
            common.streamCount--;
        }
    });
};

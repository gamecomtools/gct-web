module.exports = function(app) {
    app.get('/api/twitchOverlay', function(req, res, next) {
        res.render(__dirname + '/twitchOverlay', {
            game: 'Alien: Isolation',
            streamer: 'Spot_Spotterson'
        });
    });
}

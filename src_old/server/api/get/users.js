var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(app) {
    app.get('/api/users', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[GET] /api/users', 'get/users');

        if (requireAuth(logger, req, res)) {
            logger.info('[PARSE] Finding Users By Community', 'get/users');
            var query = {
                community: req.session.community
            };
            global.parse.findMany('gctUsers', query, function (err, response) {
                if (parseErrorHandler(logger, err, response)) {
                    res.send(response.results);
                }
            });
        }else{
            logger.complete();
        }
    });
};

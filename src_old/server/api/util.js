var bcrypt = require('bcrypt');
var merge = require('deepmerge');
var Q = require('q');
var parseErrorHandler = require('../utils/parseErrorHandler');

exports.cryptPassword = function(password, callback) {
    bcrypt.genSalt(10, function(err, salt) {
        if (err)
            return callback(err);

        bcrypt.hash(password, salt, function(err, hash) {
            return callback(err, hash);
        });

    });
};

exports.comparePassword = function(password, userPassword, callback) {
    bcrypt.compare(password, userPassword, function(err, isPasswordMatch) {
        if (err)
            return callback(err);
        return callback(null, isPasswordMatch);
    });
};

exports.updateCommunity = function(logger, communityID, updatedData) {
    var deferred = Q.defer();

    logger.info('[PARSE] Looking up community', 'util/updateCommunity');
    global.parse.find('gctCommunities', communityID, function (err, response) {
        if (parseErrorHandler(logger, err, response)) {
            if (response === undefined) {
                logger.error('[PARSE] Community Not Found', 'util/updateCommunity');
                return deferred.reject('Community Not Found');
            }
            logger.info('[PARSE] Community Found', 'util/updateCommunity');
            var community = response;
            delete community.objectId;
            delete community.createdAt;
            delete community.updatedAt;
            community = merge(community, updatedData);
            logger.info('[PARSE] Updating Community', 'util/updateCommunity');
            global.parse.update('gctCommunities', communityID, community, function (err, response) {
                logger.info('[PARSE] Community Updated', 'util/updateCommunity');
                deferred.resolve(community);
            });
        }else {
            deferred.reject('Parse.com Error');
        }
    });

    return deferred.promise;
};

var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(app) {
    app.put('/api/event', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[PUT] /api/event', 'put/event');

        if (requireAuth(logger, req, res) && requiredParams(logger, req, res, ['communityId', 'title', 'color', 'host', 'starts', 'platform'])) {
            var event = {
                communityId: req.body.communityId,
                title: req.body.title,
                color: req.body.color,
                host: req.body.host,
                platform: req.body.platform,
                starts: {
                    __type: 'Date',
                    iso: req.body.starts
                }
            };
            logger.info('[PARSE] Inserting Event', 'put/event');
            global.parse.insert('gctEvents', event, function (err, response) {
                if (parseErrorHandler(logger, err, response)) {
                    logger.info('[PARSE] Event Inserted', 'put/event');
                    res.send({});
                    logger.complete();
                }else {
                    logger.complete();
                    return res.send({
                        error: "Unknown Error (Parse)"
                    });
                }
            });
        }else{
            logger.complete();
        }
    });
};

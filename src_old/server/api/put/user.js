var passwordUtil = require(global.utilsPath + '/password');
var requiredParams = require(global.utilsPath + '/requiredParams');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(app) {
    app.put('/api/user', function (req, res, next) {
        var logger = new loggerUtil(req.session.id);
        logger.access('[PUT] /api/user', 'put/user');

        if (requiredParams(logger, req, res, ['password', 'username'])) {
            logger.access('Encrypting Password', 'put/user');
            passwordUtil.cryptPassword(req.body.password, function (err, hash) {
                if (err) {
                    logger.error('PassHash Err: ' + err, 'put/user');
                    logger.complete();
                    return res.send({
                        error: "Unknown Error Occurred (passHash)"
                    });
                }
                logger.info('[PARSE] Finding User By Username', 'put/user');
                global.parse.find('gctUsers', {username: req.body.username}, function (err, response) {
                    if (parseErrorHandler(logger, err, response)) {
                        if (response.results.length > 0) {
                            logger.error('Username Already Taken', 'put/user');
                            logger.complete();
                            //username already exists
                            return res.send({
                                error: "Username Already Taken"
                            });
                        }
                        var user = {
                            username: req.body.username,
                            password: hash,
                            community: req.body.community,
                            role: req.body.role,
                            position: req.body.position
                        };
                        logger.info('[PARSE] Inserting User', 'put/user');
                        global.parse.insert('gctUsers', user, function (err, response) {
                            if (parseErrorHandler(logger, err, response)) {
                                logger.info('[PARSE] User Inserted', 'put/user');
                                user.id = response.objectId;
                                delete user.password;
                                logger.complete();
                                req.session.user = req.body.username;
                                req.session.community = req.body.community;
                                res.send(user);
                            }else {
                                logger.complete();
                                res.send({
                                    error: "Unknown Error (Parse)"
                                });
                            }
                        });
                    }else {
                        logger.complete();
                        res.send({
                            error: "Unknown Error (Parse)"
                        });
                    }
                });
            });
        }else{
            logger.complete();
        }
    });
};

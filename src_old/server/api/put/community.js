var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(app) {
    app.put('/api/community', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[PUT] /api/community', 'put/community');

        if (requiredParams(logger, req, res, ['name'])) {
            logger.info('[PARSE] Finding Community By Name', 'put/community');
            global.parse.find('gctCommunities', {name: encodeURIComponent(req.body.name)}, function (err, response) {
                if (parseErrorHandler(logger, err, response)) {
                    if (response.results.length > 0) {
                        logger.error('Community Already Exists', 'put/community');
                        //name already exists
                        return res.send({
                            error: "Community Already Registered"
                        });
                    }
                    var community = {
                        name: req.body.name,
                        socialConnections: {},
                        socialNetworks: [],
                        hierarchyGroups: {
                            leader: {
                                name: 'Leaders',
                                permissions: '*',
                                position: 0
                            },
                            na: {
                                name: 'Unallocated',
                                permissions: '',
                                position: 9999,
                                system: true
                            }
                        }
                    };
                    logger.info('[PARSE] Inserting Community', 'put/community');
                    global.parse.insert('gctCommunities', community, function (err, response) {
                        if (parseErrorHandler(logger, err, response)) {
                            logger.info('[PARSE] Community Inserted', 'put/community');
                            community.id = response.objectId;
                            res.send(community);
                            logger.complete();
                        }else {
                            logger.complete();
                            return res.send({
                                error: "Unknown Error (Parse)"
                            });
                        }
                    });
                }else {
                    logger.complete();
                    return res.send({
                        error: "Unknown Error (Parse)"
                    });
                }
            });
        }else{
            logger.complete();
        }
    });
};

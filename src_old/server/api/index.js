var util = require('./util');
global.groupmeTriggers = {};

module.exports = function(app){
    require('./put/user')(app);
    require('./post/user')(app);
    require('./put/community')(app);
    require('./get/community')(app);
    require('./get/events')(app);
    require('./get/members')(app);
    require('./put/event')(app);
    require('./get/histogram')(app);
    require('./get/reddit/checkModInvite')(app);
    require('./get/groupme/activate')(app);
    require('./get/groupme/checkGroups')(app);
    require('./post/groupme/connectGroups')(app);
    require('./post/groupme/retrieveGroups')(app);
    //require('./post/groupme/callback')(app);
    require('./post/groupme/updateNickname')(app);
    require('./post/xbox/compareFriends')(app);
    require('./post/xbox/syncFriends')(app);
    require('./post/community/update')(app);
    require('./get/widget/widget')(app);
    require('./get/memberImport/google/analyze')(app);
    require('./get/memberImport/google/data')(app);
    require('./post/reddit/acceptModInvite')(app);
    require('./post/memberImport/google/import')(app);
    require('./get/users')(app);
    require('./get/events/google')(app);
    require('./get/xboxTest')(app);
    require('./get/twitchOverlay/twitchOverlay')(app);
    require('./get/twitchStream/validate')(app);
    require('./get/twitchStream/unpublish')(app);
    require('./get/twitchStream/streamReady')(app);
    require('./post/minecraft/callback')(app);

    require('./post/groupme/callback2.0/')(app);

    // Widgets
    require('./get/widget/source/gctEventsWidget')(app);
    require('./get/widget/source/gctXboxOnlineWidget')(app);

    // Update the GroupMe Triggers in memory
    require(global.utilsPath + '/updateGroupMeTriggers')();
};

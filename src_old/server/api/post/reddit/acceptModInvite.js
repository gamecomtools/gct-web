var passwordUtil = require(global.utilsPath + '/password');
var requiredParams = require(global.utilsPath + '/requiredParams');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var updateCommunity = require(global.utilsPath + '/updateCommunity');

module.exports = function(app) {
    app.post('/api/reddit/acceptModInvite', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[POST] /api/reddit/acceptModInvite', 'post/reddit/acceptModInvite');

        if (requiredParams(logger, req, res, ['name', 'subreddit'])) {
            logger.info('[REDDIT] Authenticate', 'post/reddit/acceptModInvite');
            reddit.auth().then(function() {
                logger.info('[REDDIT] Accepting Moderator Invite', 'post/reddit/acceptModInvite');
                return reddit('/r/' + req.body.subreddit + '/api/accept_moderator_invite').post();
            })
            .then(function(data) {
                logger.info('[REDDIT] Marking Message As Read', 'post/reddit/acceptModInvite');
                return markAsRead(req.body.name).then(function(){
                    logger.complete();
                    return res.send({
                        success: true
                    });
                });
            }, function(data) {
                logger.info('[REDDIT] Error Accepting Invite', 'post/reddit/acceptModInvite');
                var error = JSON.parse(data.body);
                if (error.json.errors[0][0] == 'NO_INVITE_FOUND') {
                    logger.complete();
                    return markAsRead(req.body.name).then(function(){
                        return res.send({
                            success: true
                        });
                    });
                }
                res.send({
                    success: false,
                    error: error.json.errors[0][0]
                });
                logger.complete();
            });

            var markAsRead = function(messageName){
                return reddit.auth().then(function() {
                    // Make an OAuth call to show that it is working
                    return reddit('/api/read_message').post({
                        id: messageName
                    }).then(function(){
                        return updateCommunity(logger, req.session.community, {
                            socialNetworks: ['reddit'],
                            socialConnections: {
                                reddit: {
                                    subreddit: req.body.subreddit
                                }
                            }
                        });
                    });
                });
            }
        }else{
            logger.complete();
        }
    });
};

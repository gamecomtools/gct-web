var passwordUtil = require(global.utilsPath + '/password');
var requiredParams = require(global.utilsPath + '/requiredParams');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var xboxLive = require(global.utilsPath + '/xboxLive');

module.exports = function(app) {
    var token = '';

    app.post('/api/xbox/syncFriends', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[POST] /api/xbox/syncFriends', 'post/xbox/syncFriends');

        if (requiredParams(logger, req, res, ['email', 'password', 'add', 'rem', 'keep', 'xuid'])) {
            xboxLive.login(req.body.email, req.body.password).then(function(result) {
                token = result.token;
                doAction(req.body.xuid, req.body.add, 'addFriend').then(function () {
                    doAction(req.body.xuid, req.body.rem, 'removeFriend').then(function () {
                        res.send({status: 'success'});
                    });
                });
            }, function() {
                res.send({error: "Invalid Email/Password"});
            })
        }else{
            logger.complete();
        }
    });

    var doAction = function(xuid, array, action) {
        var doneDefer = q.defer();

        array.reduce(function (previous, item, index) {
            return previous.then(function (previousValue) {
                var defer = q.defer();
                var start = new Date().getTime();

                var endFunction = function() {
                    var end = new Date().getTime();
                    var timeLeft = 1000 - (end - start); // Only 1 request a second
                    if (index === array.length-1) {
                        return doneDefer.resolve();
                    }
                    if (timeLeft <= 0) {
                        return defer.resolve();
                    }
                    return q.delay(timeLeft).then(function(){
                        defer.resolve();
                    });
                };

                xboxLive[action](xuid, item.xuid, token).then(endFunction, endFunction);

                return defer.promise;
            });
        }, q.resolve('start'));

        return doneDefer.promise;
    }
};

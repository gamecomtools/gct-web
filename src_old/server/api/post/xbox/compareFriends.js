var passwordUtil = require(global.utilsPath + '/password');
var requiredParams = require(global.utilsPath + '/requiredParams');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var xboxLive = require(global.utilsPath + '/xboxLive');
var parseFindMany = require(global.utilsPath + '/parseFindMany');

module.exports = function(app) {
    app.post('/api/xbox/compareFriends', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[POST] /api/xbox/compareFriends', 'post/xbox/compareFriends');

        if (requiredParams(logger, req, res, ['username', 'password'])) {
            xboxLive.login(req.body.username, req.body.password).then(function(result){
                var gamertag = result.gamertag;
                var token = result.token;
                xboxLive.getXUID(gamertag, token).then(function(xuid) {
                    if (xuid === '') {
                        return res.send({error: "An Unknown Error Occurred. Please Try Again."});
                    }
                    xboxLive.getFriends(xuid, token).then(function(friends) {
                        var userXuids = [];
                        friends.people.forEach(function(friend) {
                            userXuids.push(friend.xuid);
                        });
                        xboxLive.getUsersSettings(userXuids, token).then(function(users) {
                            var gamerTags = [];
                            var gamerTagsProper = [];
                            var xuids = [];
                            users.profileUsers.forEach(function(user) {
                                if (gamerTags.indexOf(user.settings[0].value.toLowerCase()) === -1) {
                                    gamerTags.push(user.settings[0].value.toLowerCase());
                                    gamerTagsProper.push(user.settings[0].value);
                                    xuids.push(user.id);
                                }
                            });
                            var query = {
                                communityId: req.session.community
                            };
                            parseFindMany('gctMembers', query, 'createdAt', function (err, members) {
                                if (parseErrorHandler(logger, err, members)) {
                                    members = members.results;
                                    var remove = [];
                                    var add = [];
                                    var keep = [];
                                    var found = [];
                                    members.forEach(function (member) {
                                        found.push(member.xboxGametag.toLowerCase());
                                        if (index = gamerTags.indexOf(member.xboxGametag.toLowerCase()) > -1) {
                                            // On the friend list
                                            keep.push({
                                                gamertag: member.xboxGametag,
                                                xuid: member.xuid || ''
                                            });
                                        } else {
                                            // Not on the list
                                            if (member.xuid && member.xuid !== '' && member.xboxGametag.toLowerCase() !== gamertag.toLowerCase()) {
                                                add.push({
                                                    gamertag: member.xboxGametag,
                                                    xuid: member.xuid
                                                });
                                            }
                                        }
                                    });
                                    gamerTagsProper.forEach(function (gamerTag, index) {
                                        if (found.indexOf(gamerTags[index]) === -1) {
                                            remove.push({
                                                gamertag: gamerTag,
                                                xuid: xuids[index]
                                            });
                                        }
                                    });
                                    console.log('here');
                                    res.send({
                                        add: add,
                                        rem: remove,
                                        keep: keep,
                                        xuid: xuid
                                    });
                                } else {
                                    return res.send({error: "A Parse.com Error Occurred. Please Try Again."});
                                }
                            });
                        }, function() {
                            res.send({error: "An Unknown Error Occurred. Please Try Again."});
                        });
                    }, function() {
                        res.send({error: "An Unknown Error Occurred. Please Try Again."});
                    });
                }, function() {
                    res.send({error: "An Unknown Error Occurred. Please Try Again."});
                });
            }, function() {
                res.send({error: "Invalid Email/Password"});
            })
        }else{
            logger.complete();
        }
    });
};

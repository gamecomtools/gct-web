var analytics = require(global.utilsPath + '/analytics');
var fs = require('fs');

module.exports = function(app) {
    app.post('/api/groupme/callback/', function(req, res, next){
        if (!req.query.communityId) {
            req.query.communityId = 'NOTSET';
        }
        var commandCooldown = global.persist.getItem('commandCooldown-' + req.query.communityId) || {};
        var cooldownNotifyCooldown = global.persist.getItem('cooldownNotifyCooldown-' + req.query.communityId) || {};

        global.clearCommandCooldown = function(command, groupId) {
            if (commandCooldown[groupId]) {
                commandCooldown[groupId][command] = undefined;
            }
        };

        analytics.bump(req.query.communityId + '_message');

        setTimeout(function () {
            var config = {};
            if (fs.existsSync(__dirname + '/communityConfig/' + req.query.communityId + '.js')) {
                config = require(__dirname + '/communityConfig/' + req.query.communityId + '.js');
            }

            if (req.body.text.indexOf('changed the group\'s name to') > -1) {
                // Handle name change if it is locked
                require('./handlers/lockGroupName')(guid, req);
            } else if (req.body.name.indexOf('Community') === -1 && req.body.name !== 'GameComTools' && req.body.name !== 'GroupMe') {
                if (!req.query.type) {
                    if (req.body.text.indexOf('!') === 0) {
                        // This is a command
                        if (req.query.communityId === 'NOTSET') {
                            require('./handlers/dfm')(guid, req, config);
                        } else {
                            require('./handlers/command')(commandCooldown, cooldownNotifyCooldown, guid, req, config);
                        }
                    } else if(req.body.text.indexOf('/r/') > -1 && req.body.text.indexOf('http') === -1) {
                        // This contained a subreddit link
                        require('./handlers/subredditLink')(guid, req, config);
                    } else {
                        // Run triggers
                        if (global.groupmeTriggers[req.body.group_id]) {
                            require('./handlers/triggers')(guid, req, config);
                        }
                        // Run communitySpecific
                        if (fs.existsSync(__dirname + '/communitySpecific/' + req.query.communityId + '.js')) {
                            require(__dirname + '/communitySpecific/' + req.query.communityId + '.js')(guid, req, config);
                        }
                        // Run groupSpecific
                        if (fs.existsSync(__dirname + '/groupSpecific/' + req.body.group_id+ '.js')) {
                            require(__dirname + '/groupSpecific/' + req.body.group_id + '.js')(guid, req, config);
                        }
                    }
                } else {
                    // This is a type callback
                    var typeStepData = global.persist.getItem('typeStepData') || {};
                    var step = 0;
                    if (typeStepData[req.body.group_id]) {
                        step = typeStepData[req.body.group_id].step;
                    } else {
                        typeStepData[req.body.group_id] = {
                            step: step
                        }
                    }
                    var runner = require('../types/' + req.query.type + '/step' + step + '.js');
                    runner(guid, req.body, req.query.communityID, typeStepData);
                }
            }
            global.persist.setItem('commandCooldown-' + req.query.communityId, commandCooldown);
            global.persist.setItem('cooldownNotifyCooldown-' + req.query.communityId, cooldownNotifyCooldown);
        },  global.groupmeWaitTime);

        res.send({});

        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }
    });
};

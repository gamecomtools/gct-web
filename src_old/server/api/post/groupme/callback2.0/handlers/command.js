var path = require('path');
var commandDir = path.resolve(__dirname + '/../../commands/') + '/';
var analytics = require(global.utilsPath + '/analytics');

module.exports = function(commandCooldown, cooldownNotifyCooldown, guid, req, config) {
    var fs = require('fs');
    var command = req.body.text.split(' ')[0].replace('!', '').toLowerCase();
    if (fs.existsSync(commandDir + command + '.js') && (!config.allowedCommands || config.allowedCommands.indexOf(command) > -1)) {
        var reqCommand = require(commandDir + command + '.js');
        var cooldownTime = 10;
        module.children.forEach(function(mod) {
            var cmd = mod.id.split('/');
            cmd = cmd[cmd.length - 1].replace('.js', '');
            if (cmd === command) {
                cooldownTime = mod.cooldownTime || cooldownTime;
            }
        });
        var now = new Date();

        if (commandCooldown[req.body.group_id] === undefined) {
            commandCooldown[req.body.group_id] = {};
        }
        if (cooldownNotifyCooldown[req.body.group_id] === undefined) {
            cooldownNotifyCooldown[req.body.group_id] = {};
        }

        if (commandCooldown[req.body.group_id][command] === undefined || commandCooldown[req.body.group_id][command] <= now.getTime()) {
            analytics.bump(req.query.communityId + '_command');
            analytics.bump(req.query.communityId + '_' + command  + '_command');
            now.setMinutes(now.getMinutes() + cooldownTime);
            commandCooldown[req.body.group_id][command] = now.getTime();
            reqCommand(req.body, req.query, false, config);
        } else {
            if (cooldownNotifyCooldown[req.body.group_id][command] === undefined || cooldownNotifyCooldown[req.body.group_id][command] <= now.getTime()) {
                var diffMs = (commandCooldown[req.body.group_id][command] - now.getTime()); // milliseconds between now & Christmas
                var diffMins = Math.round(diffMs/1000/60); // minutes
                var diffSeconds = Math.round(diffMs/1000); // seconds

                var message = 'That command was just used! Try again in ';
                if (diffSeconds < 60) {
                    message += diffSeconds + ' seconds!';
                } else {
                    message += diffMins + ' minutes!';
                }
                global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: message
                    }
                });
                now.setMinutes(now.getMinutes() + 1);
                cooldownNotifyCooldown[req.body.group_id][command] = now.getTime();
            }
        }
    } else {
        global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
            message: {
                source_guid: guid(),
                text: 'Unknown Command (Use !help to see command list)'
            }
        });
    }
};

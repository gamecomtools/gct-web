module.exports = function(guid, req) {
    global.parse.findMany('gctCommunities', {}, function (err, response) {
        response.results.forEach(function (community) {
            if (community.socialConnections && community.socialConnections.groupme && community.socialConnections.groupme.groupData) {
                for (var i in community.socialConnections.groupme.groupData) {
                    if (i === req.body.group_id && community.socialConnections.groupme.groupData[i].lockName) {
                        global.groupme('groups/' + req.body.group_id + '/update', 'POST', {
                            name: community.socialConnections.groupme.groupData[i].lockNameValue
                        });
                        global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
                            message: {
                                source_guid: guid(),
                                text: "This is a reminder, please do not change the group name. Thank you!"
                            }
                        });
                    }
                }
            }
        });
    });
}

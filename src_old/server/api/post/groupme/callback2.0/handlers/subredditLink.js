module.exports = function(guid, req) {
    var subreddit = req.body.text.split('/r/')[1].split(' ')[0];
    global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
        message: {
            source_guid: guid(),
            text: 'http://www.reddit.com/r/' + subreddit
        }
    });
};

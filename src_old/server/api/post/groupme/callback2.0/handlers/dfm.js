var dfmTrack = [];

module.exports = function(guid, req) {
    if (dfmTrack.indexOf(req.body.group_id) === -1) {
        dfmTrack.push(req.body.group_id);
        global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
            message: {
                source_guid: guid(),
                text: 'I\'m sorry... I am currently down for maintenance or I have ran into an unexpected problem and unable to fulfill your request. A team of hard working humans are currently working on resolving the issue as soon as possible. Please try again at a later time. I will no longer respond to commands until the issue has been resolved.'
            }
        });
    }
};

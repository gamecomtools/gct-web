module.exports = function(guid, req) {
    var found = false;
    global.groupmeTriggers[req.body.group_id].forEach(function (trigger) {
        if (req.body.text.toLowerCase().indexOf(trigger.trigger.toLowerCase()) > -1 && !found) {
            found = true;
            global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: trigger.response
                }
            });
        }
    })
};

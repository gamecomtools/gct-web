var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var updateCommunity = require(global.utilsPath + '/updateCommunity');
var updateCore = require(global.utilsPath + '/updateCore');
var Q = require('q');

module.exports = function(app) {
    app.post('/api/groupme/updateNickname', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[POST] /api/groupme/updateNickname', 'post/groupme/updateNickname');

        if (requireAuth(logger, req, res) && requiredParams(logger, req, res, ['groupId', 'nickname'])) {
            global.groupme('groups/' + req.body.groupId + '/memberships/update', 'POST', {
                membership: {
                    nickname: req.body.nickname
                }
            }).then(function (response) {
                res.send({success:true});
            });
        }
    });
};

var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var updateCommunity = require(global.utilsPath + '/updateCommunity');
var updateCore = require(global.utilsPath + '/updateCore');
var Q = require('q');

module.exports = function(app) {
    app.post('/api/groupme/retrieveGroups', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[POST] /api/groupme/retrieveGroups', 'post/groupme/retrieveGroups');

        if (requireAuth(logger, req, res) && requiredParams(logger, req, res, ['groups'])) {
            global.groupme('users/me', 'GET').then(function (me) {
                global.groupme('groups?per_page=100', 'GET').then(function (response) {
                    var groups = response.response;
                    var resp = {
                        me: me.response,
                        groups: []
                    };

                    groups.forEach(function (group, index) {
                        if (req.body.groups[group.group_id]) {
                            group.functionalType = req.body.groups[group.group_id];
                            group.botNickname = '';
                            group.members.forEach(function(member, i) {
                                if (member.user_id === resp.me.user_id) {
                                    group.botNickname = member.nickname;
                                }
                            });
                            group.saveText = 'Save';
                            resp.groups.push(group);
                        }
                    });
                    res.send(resp);
                });
            });
        }
    });
};

var common = require(global.utilsPath + '/common');
var request = require('request');

module.helpText = "Returns the current session, if one is occurring, and the next upcoming session so you can be prepared to join!";

module.directConnect = true;

module.exports = function(data, query) {
    var xbSessions = null;
    var pcSessions = null;
    var cohost = undefined;
    var next = 0;
    request.get('http://' + global.webHost + '/api/events/google?max=3&community=tH15Q9CHZr', function(err, response, body) {
        body = JSON.parse(body);
        request.get('http://reddit.com/r/TheRedditRebels/search.json?q=flair:XB Session&sort=new&restrict_sr=on', function(err, response, xbs) {
            request.get('http://reddit.com/r/TheRedditRebels/search.json?q=flair:PC Session&sort=new&restrict_sr=on', function(err, response, pcs) {
                xbSessions = JSON.parse(xbs);
                pcSessions = JSON.parse(pcs);

                var d = new Date(body[0].start);
                var now = new Date().getTime();
                if (body[0].originalStartTime) {
                    d = new Date(body[0].originalStartTime.dateTime);
                }

                var platform = body[0].summary.split('] ')[0].replace('[', '').trim();
                var game = body[0].summary.split('] ')[1].split(' - ');
                var host = game[1];
                game = game[0];
                var activities = getActivities(platform, d, host, game);
                next = 1;

                var message = '';
                if (now > d.getTime()) {
                    d = new Date(body[1].start);
                    if (body[1].originalStartTime) {
                        d = new Date(body[1].originalStartTime.dateTime);
                    }
                    if (now > d.getTime()) {
                        message += 'There are currently 2 sessions occurring. Here are the details:\n\nPlatform: ' + platform + '\nGame: ' + game + '\nHost: ' + host + '\n';
                        if (cohost) {
                            message += 'Co-Host: ' + cohost + '\n';
                        }
                        if (activities) {
                            message += 'Activities Planned:\n' + activities + '\n';
                        }
                        message += '\n';
                        platform = body[1].summary.split('] ')[0].replace('[', '');
                        game = body[1].summary.split('] ')[1].split(' - ');
                        host = game[1];
                        game = game[0];
                        d = new Date(body[1].start);
                        if (body[1].originalStartTime) {
                            d = new Date(body[1].originalStartTime.dateTime);
                        }
                        activities = getActivities(platform, d, host, game);
                        message += 'Platform: ' + platform + '\nGame: ' + game + '\nHost: ' + host + '\n';
                        if (cohost) {
                            message += 'Co-Host: ' + cohost + '\n';
                        }
                        if (activities) {
                            message += 'Activities Planned:\n' + activities + '\n';
                        }
                        message += '\n';

                        platform = body[2].summary.split('] ')[0].replace('[', '');
                        game = body[2].summary.split('] ')[1].split(' - ');
                        host = game[1];
                        game = game[0];
                        d = new Date(body[2].start);
                        if (body[2].originalStartTime) {
                            d = new Date(body[2].originalStartTime.dateTime);
                        }
                        activities = getActivities(platform, d, host, game);
                        next = 3;
                    } else {
                        message += 'There is currently a session occurring. Here are the details:\n\nPlatform: ' + platform + '\nGame: ' + game + '\nHost: ' + host + '\n';
                        if (cohost) {
                            message += 'Co-Host: ' + cohost + '\n';
                        }
                        if (activities) {
                            message += 'Activities Planned:\n' + activities + '\n';
                        }
                        message += '\n';
                        platform = body[1].summary.split('] ')[0].replace('[', '');
                        game = body[1].summary.split('] ')[1].split(' - ');
                        host = game[1];
                        game = game[0];
                        d = new Date(body[1].start);
                        if (body[1].originalStartTime) {
                            d = new Date(body[1].originalStartTime.dateTime);
                        }
                        activities = getActivities(platform, d, host, game);
                        next = 2;
                    }
                }
                var n = new Date();

                var nextdate = new Date(body[next].start);
                if (body[next].originalStartTime) {
                    nextdate = new Date(body[next].originalStartTime.dateTime);
                }

                if (nextdate.getDate() === d.getDate()) {
                    if (d.getDate() !== n.getDate()) {
                        message += 'There are 2 sessions up next tomorrow starting at ' + getTimeString(d) + ' EST. ';
                    } else {
                        message += 'There are 2 sessions up next starting at ' + getTimeString(d) + ' EST (Currently ' + getTimeString(new Date()) + ' EST). ';
                    }
                    message += 'Here are the session details:\n\n';
                    message += 'Platform: ' + platform + '\nGame: ' + game + '\nHost: ' + host + '\n';
                    if (cohost) {
                        message += 'Co-Host: ' + cohost + '\n';
                    }
                    if (activities) {
                        message += 'Activities Planned:\n' + activities + '\n';
                    }
                    message += '\n';
                    platform = body[next].summary.split('] ')[0].replace('[', '');
                    game = body[next].summary.split('] ')[1].split(' - ');
                    host = game[1];
                    game = game[0];
                    d = new Date(body[next].start);
                    if (body[next].originalStartTime) {
                        d = new Date(body[next].originalStartTime.dateTime);
                    }
                    activities = getActivities(platform, d, host, game);
                } else {
                    if (d.getDate() !== n.getDate()) {
                        message += 'The next session will be tomorrow starting at ' + getTimeString(d) + ' EST. ';
                    } else {
                        message += 'The next session is starting at ' + getTimeString(d) + ' EST (Currently ' + getTimeString(new Date()) + ' EST). ';
                    }
                    message += 'Here are the session details:\n\n';
                }

                message += 'Platform: ' + platform + '\nGame: ' + game + '\nHost: ' + host + '\n';
                if (cohost) {
                    message += 'Co-Host: ' + cohost + '\n';
                }
                if (activities) {
                    message += 'Activities Planned:\n' + activities;
                }

                if (message.length > 1000) {
                    var part = message.split('The next session')[0];
                    global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                        message: {
                            source_guid: guid(),
                            text: part
                        }
                    });
                    message = 'The next session' + message.split('The next session')[1];
                }

                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: message
                    }
                });
            });
        });
    });

    function getActivities(platform, date, host, game) {
        var stream = xbSessions;
        if (platform === 'PC') {
            stream = pcSessions;
        }
        cohost = undefined;
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var dateString = months[date.getMonth()] + ' ' + date.getDate();
        var activities = '';
        stream.data.children.forEach(function(post) {
            if (post.data.title.indexOf(dateString) > -1) {
                var parts = post.data.selftext.split('###Planned activities for the session w/ ');
                var validateHost = undefined;
                if (parts.length === 1) {
                    validateHost = post.data.selftext.split('Hosted by ')[1].split('\n')[0];
                    if (validateHost.indexOf(' and ') > -1) {
                        // co-host
                        cohost = validateHost.split(' and ')[1].trim();
                    }
                    if (validateHost.indexOf(' & ') > -1 && !cohost) {
                        // co-host
                        cohost = validateHost.split(' & ')[1].trim();
                    }
                    if (validateHost.indexOf(' (Social Club name)') > -1) {
                        validateHost = validateHost.replace(' (Social Club name)', '');
                    }
                    validateHost = validateHost.split(' and ')[0].trim();
                }
                parts = post.data.selftext.split('###Planned activities for the session:');
                parts.forEach(function(part, index) {
                    if (index > 0) {
                        var sessionHost = validateHost || part.split(':')[0].trim();
                        if (sessionHost.toLowerCase() === host.toLowerCase()) {
                            var act = part.split('\n');
                            var stop = false;
                            act.forEach(function(actPart, index) {
                                if (index > 3 && !stop) {
                                    var a = actPart.split('|');
                                    if (a.length > 1) {
                                        if (activities !== '') {
                                            activities += '\n';
                                        }
                                        activities += '   ' + a[1] + ' - ' + a[2]
                                    } else {
                                        stop = true;
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
        if (activities === '') activities = false;
        return activities;
    }

    function getTimeString(d) {
        var h = d.getHours();
        var m = d.getMinutes();
        var a = 'AM';
        if (h >= 12) {
            a = 'PM';
            if (h !== 12) {
                h = h - 12;
            }
        }
        if (h === 0) {
            h = 12;
        }
        if (m < 10) {
            m = "0" + m;
        }

        return h + ':' + m + ' ' + a;
    }

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
};

var common = require(global.utilsPath + '/common');
var request = require('request');

module.directConnect = true;

module.exports = function(data, query, isDirectConnect, config) {
    var message = "The community bot can respond to the following commands, each command can only be used once every 10 minutes:";
    if (isDirectConnect) {
        message = 'Hi there! I can respond to the following commands in order to assist you:';
    }
    message += '\n-------------------------------\n';

    var fs = require('fs');
    fs.readdir(__dirname, function(err, files) {
        files.forEach(function(file) {
            if (file !== 'help.js') {
                require('./' + file);
            }
        });
        setTimeout(function() {
            module.children.forEach(function (mod) {
                if (!mod.limitCommand) {
                    if (!isDirectConnect || (isDirectConnect && mod.directConnect)) {
                        var command = mod.id.split('/');
                        command = command[command.length - 1].replace('.js', '');
                        if (!config.allowedCommands || config.allowedCommands.indexOf(command) > -1) {
                            message += '!' + command;
                            if (mod.helpText) {
                                message += ' - ' + mod.helpText;
                            }
                            message += '\n\n';
                        }
                    }
                }
            });
            global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: message
                }
            });
        }, 1000);
    });

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
};

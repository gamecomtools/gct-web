var common = require(global.utilsPath + '/common');
var request = require('request');

module.helpText = "Returns a random gif based on the keyword after the command. ex. !giphy funny cat";

module.cooldownTime = 30;

module.exports = function(data, query) {
    var userLimit = global.persist.getItem('userLimit-' + data.group_id) || {};

    var now = new Date();
    if (userLimit[data.user_id] === undefined) {
        userLimit[data.user_id] = {
            count: 0,
            expires: null,
            notified: false
        }
    }
    if (userLimit[data.user_id].expires !== null && userLimit[data.user_id].expires <= now.getTime()) {
        userLimit[data.user_id] = {
            count: 0,
            expires: null,
            notified: false
        }
    }
    if (userLimit[data.user_id].count === 2) {
        if (!userLimit[data.user_id].notified) {
            userLimit[data.user_id].notified = true;

            var diffMs = (userLimit[data.user_id].expires - now.getTime()); // milliseconds between now & Christmas
            var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
            global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: 'You have been doing that too much. Try again in ' + diffMins + ' minutes.'
                }
            });
            return global.clearCommandCooldown('giphy', data.group_id);
        }
    } else {
        if (userLimit[data.user_id].expires === null) {
            userLimit[data.user_id].expires = now.setMinutes(now.getMinutes() + 60);
            userLimit[data.user_id].expires = now.getTime();
        }

        userLimit[data.user_id].count++;
        var searchTerm = data.text.replace('!giphy ', '').replace(/ /, '+');
        if (common.bannedGroupMeKeywords.indexOf(searchTerm.toLowerCase()) > -1) {
            global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: 'That keyword has been banned.'
                }
            });
            return global.clearCommandCooldown('giphy', data.group_id);
        }
        request.get('http://api.giphy.com/v1/gifs/random?tag=' + searchTerm + '&api_key=dc6zaTOxFJmzC&limit=1&rating=pg-13', function (err, response, body) {
            body = JSON.parse(body);

            if (body.meta) {
                return global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: 'giphy is currently down, unable to get a gif :('
                    }
                });
            }

            if (body.data.id) {
                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: body.data.image_url
                    }
                });
            } else if(searchTerm === 'awood') {
                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: 'http://i.groupme.com/777x777.gif.522e41f74c6e46519fbc39779548fcc1.large'
                    }
                });
            } else {
                userLimit[data.user_id].count--;
                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: 'No giphy found :('
                    }
                });
                global.clearCommandCooldown('giphy', data.group_id);
            }
        });
    }
    global.persist.setItemSync('userLimit-' + data.group_id, userLimit);

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
};

var common = require(global.utilsPath + '/common');
var request = require('request');
var parseFindMany = require(global.utilsPath + '/parseFindMany');

module.helpText = 'Returns all online members that are currently playing the game you asked for so that you can join them or invite them! ex. !online gta';

module.cooldownTime = 1;

module.directConnect = true;

module.exports = function(data, query) {
    var xuids = [];
    var gamertags = [];
    var gameInput = data.text.replace('!online', '').trim().toLowerCase();
    gameInput = gameInput.replace('™', '').replace('®', '');

    if (gameInput === '') {
        global.clearCommandCooldown('online', data.group_id);
        return global.groupme('groups/' + data.group_id + '/messages', 'POST', {
            message: {
                source_guid: guid(),
                text: '!online requires a game now. Try \'!online GAME_TITLE\' instead'
            }
        });
    } else {
        global.groupme('groups/' + data.group_id + '/messages', 'POST', {
            message: {
                source_guid: guid(),
                text: 'One Moment...'
            }
        });
        parseFindMany('gctMembers', {communityId: 'tH15Q9CHZr'}, 'createdAt', function (err, members) {
            members.results.forEach(function (member) {
                if (member.xuid && member.xuid !== '') {
                    xuids.push(member.xuid);
                    gamertags.push(member.xboxGametag);
                }
            });
            global.xbox.getPresence(xuids).then(function (result) {
                var onlineCount = 0;
                var nonGameCount = 0;
                var games = {};
                var gameCount = {};
                result.forEach(function (member) {
                    if (member.state === 'Online') {
                        member.devices.forEach(function (device) {
                            if (device.type === 'MCapensis') {
                                device.titles.forEach(function (title) {
                                    if (title.state === 'Active' && title.placement === 'Full') {
                                        if (common.sameNames[title.name]) {
                                            title.name = common.sameNames[title.name];
                                        }
                                        if (title.name.indexOf('.') > -1) {
                                            title.name = title.name.replace(/\./g, '').replace(/\n/, '').replace(/\r/, '');
                                        }
                                        if (common.nonGames.indexOf(title.name) == -1 && title.name.indexOf('Hulu Plus') === -1) {
                                            if (!games[title.name]) {
                                                games[title.name] = [];
                                                gameCount[title.name] = 0;
                                            }
                                            if (games[title.name].indexOf(gamertags[xuids.indexOf(member.xuid)]) === -1) {
                                                gameCount[title.name]++;
                                                games[title.name].push(gamertags[xuids.indexOf(member.xuid)]);
                                            }
                                        } else {
                                            nonGameCount++;
                                        }
                                    }
                                });
                            }
                        });
                        onlineCount++;
                    }
                });
                var message = '';
                if (gameInput == 'all') {
                    message += 'There are ' + onlineCount + ' members online playing ' + Object.keys(games).length + ' games and ' + nonGameCount + ' non-games! (xbox only currently)\n';
                    message += '-------------------------------\n';
                    var sortable = [];
                    for (var game in gameCount) {
                        sortable.push([game, gameCount[game]]);
                    }
                    sortable.sort(function (a, b) {
                        return a[1] - b[1]
                    });
                    sortable.reverse();

                    sortable.forEach(function (game) {
                        message += game[0] + ' (' + game[1] + '):\n\t' + games[game[0]].join(', ') + '\n\n';
                    });
                } else {
                    var foundAcronym = false;
                    for(var i in common.gameAcronyms) {
                        if (common.gameAcronyms[i].indexOf(gameInput) > -1) {
                            gameInput = i.toLowerCase();
                            foundAcronym = true;
                        }
                    }

                    var foundGame = false;

                    for (var i in games) {
                        var name = i.toLowerCase();
                        if (gameInput.indexOf('the ') === -1 && name.indexOf('the ') === 0) {
                            name = name.replace('the ', '');
                        }
                        if (name.indexOf(gameInput) === 0) {
                            foundGame = true;
                            message += 'There are ' + gameCount[i] + ' members online playing ' + i + '! (xbox only currently)\n';
                            message += '-------------------------------\n';
                            message += games[i].join(', ');
                        }
                    }
                    if (!foundGame) {
                        if (!foundAcronym) {
                            message += 'There is either no-one playing that game currently, or I do not understand what game you are asking for.';
                        } else {
                            message += 'There is either no-one playing that game currently.';
                        }
                        global.clearCommandCooldown('online', data.group_id);
                    }
                }

                var removedGames = [];
                var ready = false;
                var newMess = '';

                while (!ready) {
                    newMess = message + '';
                    if (removedGames.length > 0) {
                        newMess += '\nAlso: ' + removedGames.join(', ');
                    }
                    if (newMess.length > 1000) {
                        var mess = message.split('\n');
                        removedGames.push(mess[mess.length - 4].replace(':', ''));
                        mess.splice(-1, 1);
                        mess.splice(-1, 1);
                        mess.splice(-1, 1);
                        mess.splice(-1, 1);
                        message = mess.join('\n');
                    } else {
                        ready = true;
                    }
                }

                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: newMess
                    }
                });
            });
        });
    }

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
};

var common = require(global.utilsPath + '/common');

module.helpText = "Used for clash of clans communities to declare war and enable members to callout enemies.";

module.cooldownTime = 1;

module.exports = function(data, query) {
    var allowed = (common.approvedGroupMeUsers[query.communityId] && common.approvedGroupMeUsers[query.communityId].indexOf(data.user_id) > -1);
    var opponent = data.text.replace('!war', '').trim();
    var now = new Date();
    var exp = new Date();
    var currentWar = global.persist.getItem('war-' + data.group_id);

    if (currentWar !== undefined && currentWar.expires <= now.getTime) {
        currentWar = undefined;
    }

    if (currentWar === undefined) {
        if (allowed) {
            if (opponent === '') {
                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: 'An opponent is required. ex \'!war Opponent Name\''
                    }
                });
                return global.clearCommandCooldown('war', data.group_id);
            }

            now.setHours(now.getHours() + 48);
            exp.setHours(exp.getHours() + 24);
            var war = {
                opponent: opponent,
                enemyCount: 0,
                callouts: [],
                expires: now.getTime(),
                calloutExpires: exp.getTime()
            };
            global.persist.setItemSync('war-' + data.group_id, war);
            global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: 'Declare the number of enemies! Use \'!war NUMBER_HERE\''
                }
            });
            global.clearCommandCooldown('war', data.group_id);
        } else {
            global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: 'You are not allowed to start a war. Please contact your community leaders.'
                }
            });
            return global.clearCommandCooldown('war', data.group_id);
        }
    } else {
        if (currentWar.enemyCount === 0 || currentWar.enemyCount === null) {
            if (allowed) {
                if (opponent === '') {
                    global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                        message: {
                            source_guid: guid(),
                            text: 'An enemy count is required. ex \'!war 25\''
                        }
                    });
                    return global.clearCommandCooldown('war', data.group_id);
                }

                currentWar.enemyCount = parseInt(opponent);
                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: 'A war has begun against ' + currentWar.opponent + '!\n\nThere are ' + currentWar.enemyCount + ' enemies, use the command \'!callout ENEMY_NUMBER\' to call an enemy, use \'!war\' to see current calls!\n\nYou have 24 hours to call an enemy!'
                    }
                });
                global.persist.setItemSync('war-' + data.group_id, currentWar);
                global.clearCommandCooldown('war', data.group_id);
            } else {
                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: 'You are not allowed to configure a war. Please contact your community leaders.'
                    }
                });
                return global.clearCommandCooldown('war', data.group_id);
            }
        } else {
            var message = 'A war is open against ' + currentWar.opponent + '!\n\nThere are ' + currentWar.enemyCount + ' enemies, use the command \'!callout ENEMY_NUMBER\' to call an enemy, use \'!war\' to see current calls!\n\n';
            var i = 0;
            while(i !== currentWar.enemyCount) {
                message += 'Enemy ' + (i+1) + ': ';
                if (currentWar.callouts[i] === undefined || currentWar.callouts[i] === null) {
                    message += '';
                } else {
                    message += currentWar.callouts[i];
                }
                message += '\n';
                i++;
            }
            message += '\n';
            now = new Date();
            var msec = (currentWar.calloutExpires - now.getTime());

            var hours = Math.floor(msec / 1000 / 60 / 60);
            msec -= hours * 1000 * 60 * 60;
            var minutes = Math.floor(msec / 1000 / 60);
            msec -= minutes * 1000 * 60;
            var seconds = Math.floor(msec / 1000);
            msec -= seconds * 1000;

            if (hours > 0) {
                message += hours + ' hour' + (hours > 1 ? 's' : '') + ' left to call an enemy. ';
            } else if (minutes > 0) {
                message += minutes + ' minute' + (minutes > 1 ? 's' : '') + ' left to call an enemy. ';
            } else if (seconds > 0) {
                message += seconds + ' second' + (seconds > 1 ? 's' : '') + ' left to call an enemy. ';
            } else {
                message += 'You can no longer call an enemy. '
            }

            now = new Date();
            msec = (currentWar.expires - now.getTime());

            hours = Math.floor(msec / 1000 / 60 / 60);
            msec -= hours * 1000 * 60 * 60;
            minutes = Math.floor(msec / 1000 / 60);
            msec -= minutes * 1000 * 60;
            seconds = Math.floor(msec / 1000);
            msec -= seconds * 1000;

            if (hours > 0) {
                message += hours + ' hour' + (hours > 1 ? 's' : '') + ' left in war.';
            } else if (minutes > 0) {
                message += minutes + ' minute' + (minutes > 1 ? 's' : '') + ' left in war.';
            } else {
                message += seconds + ' second' + (seconds > 1 ? 's' : '') + ' left in war.';
            }

            global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: message
                }
            });
        }
    }

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
};

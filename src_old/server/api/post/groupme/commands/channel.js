var common = require(global.utilsPath + '/common');

module.limitCommand = true;

module.exports = function(data, query) {
    if (common.approvedGroupMeUsers[query.communityId] && common.approvedGroupMeUsers[query.communityId].indexOf(data.user_id) > -1) {
        global.groupme('groups/' + data.group_id, 'GET').then(function (response) {
            var message = '';
            var attachments = [{
                loci: [],
                type: 'mentions',
                user_ids: []
            }];
            response.response.members.forEach(function (member) {
                if (member.nickname.indexOf(' Bot') > -1) {
                    return;
                }
                if ((message.length + (' @' + member.nickname).length) > 1000) {
                    sendMessage();
                    message = '';
                    attachments = [{
                        loci: [],
                        type: 'mentions',
                        user_ids: []
                    }];
                }
                var start = 0;
                if (message !== '') {
                    message += ' ';
                    start = message.length + 1;
                }
                message += '@' + member.nickname;

                attachments[0].loci.push([start, ('@' + member.nickname).length]);
                attachments[0].user_ids.push(member.id);
            });
            if (message !== '') {
                sendMessage();
            }

            function sendMessage() {
                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: message,
                        attachments: attachments
                    }
                });
            }
        });
    } else {
        global.groupme('groups/' + data.group_id + '/messages', 'POST', {
            message: {
                source_guid: guid(),
                text: 'You are not authorized to use that command.'
            }
        });
    }

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
};

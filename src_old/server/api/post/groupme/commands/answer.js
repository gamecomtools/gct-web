var common = require(global.utilsPath + '/common');

module.helpText = "Answer the currently active poll. ex. !answer yes";

module.cooldownTime = 1;

module.exports = function(data, query) {
    var parts = data.text.split('\n');
    var question = parts[0].replace('!poll ', '');
    var now = new Date();
    var currentPoll = global.persist.getItem('poll-' + data.group_id);

    if (currentPoll === undefined) {
        global.groupme('groups/' + data.group_id + '/messages', 'POST', {
            message: {
                source_guid: guid(),
                text: data.name + ', there is no active poll. Feel free to start one via \'!poll QUESTION\''
            }
        });
    } else {
        if (currentPoll.users.indexOf(data.user_id) === -1) {
            var answer = data.text.replace('!answer ', '').toLowerCase().trim();
            if (answer === 'yes' || answer === 'no') {
                currentPoll.users.push(data.user_id);
                if (currentPoll.answers[answer] === undefined) {
                    currentPoll.answers[answer] = 0;
                }
                currentPoll.answers[answer]++;
                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: data.name + ', vote saved!'
                    }
                });
            } else {
                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: data.name + ', invalid answer!'
                    }
                });
            }
        } else {
            if (currentPoll.notifiedUsers.indexOf(data.user_id) === -1) {
                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: data.name + ', you are only allowed to answer once!'
                    }
                });
                currentPoll.notifiedUsers.push(data.user_id);
            }
        }
    }
    global.clearCommandCooldown('answer', data.group_id);
    global.persist.setItemSync('poll-' + data.group_id, currentPoll);

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
};

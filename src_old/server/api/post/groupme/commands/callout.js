var common = require(global.utilsPath + '/common');

module.helpText = "Used for clash of clans community members to call enemies whe war has begun.";

module.exports = function(data, query) {
    var callout = data.text.replace('!callout', '').trim();
    var now = new Date();
    var currentWar = global.persist.getItem('war-' + data.group_id);

    if (currentWar !== undefined && currentWar.expires <= now.getTime) {
        currentWar = undefined;
    }

    if (currentWar === undefined) {
        global.groupme('groups/' + data.group_id + '/messages', 'POST', {
            message: {
                source_guid: guid(),
                text: 'A war has not yet begun!'
            }
        });
        global.clearCommandCooldown('callout', data.group_id);
    } else {
        if (currentWar.enemyCount === 0) {
            global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: 'A war is still being configured!'
                }
            });
            global.clearCommandCooldown('callout', data.group_id);
        } else {
            if (currentWar.calloutExpires <= now.getTime()) {
                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: 'Callouts are closed! Use \'!war\' to see the list of calls'
                    }
                });
                return global.clearCommandCooldown('callout', data.group_id);
            }

            if (callout === '') {
                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: 'An enemy number is required. ex \'!callout 3\''
                    }
                });
                return global.clearCommandCooldown('callout', data.group_id);
            }
            callout = parseInt(callout);

            if (currentWar.callouts.indexOf(data.name) === -1) {
                if (callout > currentWar.enemyCount) {
                    global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                        message: {
                            source_guid: guid(),
                            text: data.name + ', that enemy number is too high. Only ' + currentWar.enemyCount + ' enemies in this war.'
                        }
                    });
                } else if(currentWar.callouts[callout - 1] !== undefined && currentWar.callouts[callout - 1] !== null) {
                    global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                        message: {
                            source_guid: guid(),
                            text: data.name + ', that enemy has already been called.'
                        }
                    });
                } else {
                    currentWar.callouts[callout - 1] = data.name;
                    global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                        message: {
                            source_guid: guid(),
                            text: data.name + ' has called enemy ' + callout + '!'
                        }
                    });
                    global.persist.setItemSync('war-' + data.group_id, currentWar);
                }
            } else {
                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: data.name + ', you have already called enemy ' + (parseInt(currentWar.callouts.indexOf(data.name))+1) + '.'
                    }
                });
            }
        }
        global.clearCommandCooldown('callout', data.group_id);
    }

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
};

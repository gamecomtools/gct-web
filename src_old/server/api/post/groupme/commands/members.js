var common = require(global.utilsPath + '/common');
var parseFindMany = require(global.utilsPath + '/parseFindMany');

module.helpText = "Returns a list of members that own the specified game. ex. !members gta";

module.cooldownTime = 1;

module.exports = function(data, query) {
    var gameInput = data.text.replace('!members', '').trim().toLowerCase();
    gameInput = gameInput.replace('™', '').replace('®', '');

    if (gameInput === '') {
        global.clearCommandCooldown('members', data.group_id);
        return global.groupme('groups/' + data.group_id + '/messages', 'POST', {
            message: {
                source_guid: guid(),
                text: '!members requires a game. ex. \'!members GAME_TITLE\''
            }
        });
    } else {
        parseFindMany('gctMembers', {communityId: 'tH15Q9CHZr'}, 'createdAt', function (err, members) {
            var foundMembers = [];

            for(var i in common.gameAcronyms) {
                if (common.gameAcronyms[i].indexOf(gameInput) > -1) {
                    gameInput = i.toLowerCase();
                }
            }

            members.results.forEach(function (member) {
                if (member.ownedGames) {
                    member.ownedGames.forEach(function(game) {
                        if (common.sameNames[game]) {
                            game = common.sameNames[game];
                        }
                        if (game.indexOf('.') > -1) {
                            game = game.replace(/\./g, '').replace(/\n/, '').replace(/\r/, '');
                        }
                        if (gameInput.indexOf('the ') === -1 && game.indexOf('the ') === 0) {
                            game = game.replace('the ', '');
                        }
                        if (game.toLowerCase().indexOf(gameInput) === 0) {
                            foundMembers.push(member.xboxGametag);
                        }
                    });
                }
            });

            var message = 'The following members own the game:\n\n' + foundMembers.join(', ');
            if (foundMembers.length === 0) {
                message = 'Ether no-one owns that game currently, or I do not understand what game you are asking for.';
            }
            message += '\n\nKeep in mind, we only do an update every 4 hours on owned games.';

            if (message.length > 1000) {
                message = 'Sorry, ' + foundMembers.length + ' people own that game which is too many for me to display.';
            }

            global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: message
                }
            });
        });
    }

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
};

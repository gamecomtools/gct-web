var common = require(global.utilsPath + '/common');

module.helpText = "Open a poll for the group to be able to answer. Polls last for 5 hours and are yes or no only. ex. !poll Does everyone enjoy the bot?";

module.cooldownTime = 5;

module.exports = function(data, query) {
    var parts = data.text.split('\n');
    var question = parts[0].replace('!poll ', '');
    var now = new Date();
    var currentPoll = global.persist.getItem('poll-' + data.group_id);

    if (currentPoll === undefined) {
        now.setHours(now.getHours() + 4);
        var poll = {
            question: question,
            expires: now.getTime(),
            answers: {},
            users: [],
            notifiedUsers: []
        };
        global.persist.setItemSync('poll-' + data.group_id, poll);
        global.groupme('groups/' + data.group_id + '/messages', 'POST', {
            message: {
                source_guid: guid(),
                text: 'A poll has been opened! To answer use \'!answer yes\' or \'!answer no\'. The question is:\n\n' + question + '\n\nThis poll is open for 4 hours.'
            }
        });
        global.clearCommandCooldown('poll', data.group_id);
    } else {
        var message = 'A poll has already been opened. To answer use \'!answer yes\' or \'!answer no\'. The question is:\n\n' + currentPoll.question + '\n\nThis poll is open for another ';

        now = new Date();
        var msec = (currentPoll.expires - now.getTime());

        var hours = Math.floor(msec / 1000 / 60 / 60);
        msec -= hours * 1000 * 60 * 60;
        var minutes = Math.floor(msec / 1000 / 60);
        msec -= minutes * 1000 * 60;
        var seconds = Math.floor(msec / 1000);
        msec -= seconds * 1000;

        if (hours > 0) {
            message += hours + ' hour' + (hours > 1 ? 's' : '') + '.';
        } else if (minutes > 0) {
            message += minutes + ' minute' + (minutes > 1 ? 's' : '') + '.';
        } else if (seconds > 0) {
            message += seconds + ' second' + (seconds > 1 ? 's' : '') + '.';
        }

        global.groupme('groups/' + data.group_id + '/messages', 'POST', {
            message: {
                source_guid: guid(),
                text: message
            }
        });
    }

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
};

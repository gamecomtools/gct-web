var common = require(global.utilsPath + '/common');

module.exports = function(guid, body) {
    var streamer = body.name;
    var handlers = {
        game: function() {
            var game = body.text.split('!game ')[1];

            common.currentlyStreaming[streamer].game = game;

            global.groupme('groups/' + body.group_id + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: "Game set to '" + game + "'!"
                }
            });
        }
    };



    if (body.text.indexOf('!') === 0) {
        var command = body.text.split(' ')[0].replace('!', '');
        if (handlers[command]) {
            return handlers[command]();
        }
    }

    global.groupme('groups/' + body.group_id + '/messages', 'POST', {
        message: {
            source_guid: guid(),
            text: "I don't know that command '" + command + "'... Please try again or try !help"
        }
    });



    //global.groupme('groups/' + body.group_id + '/messages', 'POST', {
    //    message: {
    //        source_guid: guid(),
    //        text: "Alright, let's get everything in order! I am just going to ask a series of yes/no questions to get you into the correct groups."
    //    }
    //}).then(function() {
    //    setTimeout(function () {
    //        global.groupme('groups/' + body.group_id + '/messages', 'POST', {
    //            message: {
    //                source_guid: guid(),
    //                text: "Do you play Battlefield?"
    //            }
    //        });
    //        // update the bot
    //        global.groupme('bots', 'GET').then(function (bots) {
    //            var botName = '';
    //            bots.response.forEach(function (bot) {
    //                if (bot.group_id == body.group_id) {
    //                    botName = bot.name;
    //                    global.groupme('bots/destroy', 'POST', {
    //                        bot_id: bot.bot_id
    //                    }).then(function(bots) {
    //                        global.groupme('bots', 'POST', {
    //                            bot: {
    //                                name: botName,
    //                                group_id: body.group_id,
    //                                callback_url: 'http://gamecomtools.com/api/groupme/callback?type=gmApplication&step=1'
    //                            }
    //                        });
    //                    });
    //                }
    //            })
    //        });
    //    }, 1300);
    //})
};

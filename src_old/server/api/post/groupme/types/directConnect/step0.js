module.exports = function(guid, body, communityID, typeStepData) {
    var fs = require('fs');
    var command = body.text.split(' ')[0].replace('!', '');
    if (fs.existsSync(__dirname + '/../../commands/' + command + '.js')) {
        var reqCommand = require('../../commands/' + command);
        var validDirectConnectCommand = false;
        module.children.forEach(function(mod) {
            var cmd = mod.id.split('/');
            cmd = cmd[cmd.length - 1].replace('.js', '');
            if (cmd === command && mod.directConnect) {
                reqCommand(body, {}, true, {});
                validDirectConnectCommand = true;
            }
        });

        if (!validDirectConnectCommand) {
            global.groupme('groups/' + body.group_id + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: 'That command cannot be used in direct connect.'
                }
            });
        }
    } else {
        global.groupme('groups/' + body.group_id + '/messages', 'POST', {
            message: {
                source_guid: guid(),
                text: 'Unknown Command'
            }
        });
    }
};

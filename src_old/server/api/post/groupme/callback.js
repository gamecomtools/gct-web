var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var updateCommunity = require(global.utilsPath + '/updateCommunity');
var updateCore = require(global.utilsPath + '/updateCore');
var analytics = require(global.utilsPath + '/analytics');
var common = require(global.utilsPath + '/common');
var request = require('request');
var Q = require('q');

module.exports = function(app) {
    var commandCooldown = {};
    var cooldownNotifyCooldown = {};

    global.clearCommandCooldown = function(command, groupId) {
        delete commandCooldown[groupId][command];
        delete cooldownNotifyCooldown[groupId][command];
    };

    app.post('/api/groupme/callback', function(req, res, next){
        analytics.bump('message');
        var logger = new loggerUtil(req.session.id);
        logger.access('[POST] /api/groupme/callback', 'post/groupme/callback');
        if (req.body.name.indexOf('Community') === -1 && req.body.name !== 'GameComTools' && req.body.name !== 'GroupMe') {
            if (!req.query.type) {
                if (req.body.text.indexOf('!') === 0) {
                    // This is a command
                    var fs = require('fs');
                    var command = req.body.text.split(' ')[0].replace('!', '');
                    if (fs.existsSync(__dirname + '/commands/' + command + '.js')) {
                        var reqCommand = require('./commands/' + command);
                        var cooldownTime = 10;
                        module.children.forEach(function(mod) {
                            var cmd = mod.id.split('/');
                            cmd = cmd[cmd.length - 1].replace('.js', '');
                            if (cmd === command) {
                                cooldownTime = mod.cooldownTime || cooldownTime;
                            }
                        });
                        var now = new Date();

                        if (commandCooldown[req.body.group_id] === undefined) {
                            commandCooldown[req.body.group_id] = {};
                        }
                        if (cooldownNotifyCooldown[req.body.group_id] === undefined) {
                            cooldownNotifyCooldown[req.body.group_id] = {};
                        }
                        if (commandCooldown[req.body.group_id][command] === undefined || commandCooldown[req.body.group_id][command] <= now.getTime()) {
                            analytics.bump('command');
                            setTimeout(function () {
                                reqCommand(req.body);
                            }, global.groupmeWaitTime);
                            now.setMinutes(now.getMinutes() + cooldownTime);
                            commandCooldown[req.body.group_id][command] = now.getTime();
                        } else {
                            if (cooldownNotifyCooldown[req.body.group_id][command] === undefined || cooldownNotifyCooldown[req.body.group_id][command] <= now.getTime()) {
                                var diffMs = (commandCooldown[req.body.group_id][command] - now.getTime()); // milliseconds between now & Christmas
                                var diffMins = Math.round(diffMs/1000/60); // minutes
                                var diffSeconds = Math.round(diffMs/1000); // seconds

                                setTimeout(function () {
                                    var message = 'That command was just used! Try again in ';
                                    if (diffSeconds < 60) {
                                        message += diffSeconds + ' seconds!';
                                    } else {
                                        message += diffMins + ' minutes!';
                                    }
                                    global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
                                        message: {
                                            source_guid: guid(),
                                            text: message
                                        }
                                    });
                                }, global.groupmeWaitTime);
                                now.setMinutes(now.getMinutes() + 1);
                                cooldownNotifyCooldown[req.body.group_id][command] = now.getTime();
                            }
                        }
                    } else {
                        setTimeout(function () {
                            global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
                                message: {
                                    source_guid: guid(),
                                    text: 'Unknown Command'
                                }
                            });
                        }, global.groupmeWaitTime);
                    }
                } else if(req.body.text.indexOf('/r/') > -1 && req.body.text.indexOf('http') === -1) {
                    // This contained a subreddit link
                    var subreddit = req.body.text.split('/r/')[1].split(' ')[0];
                    setTimeout(function () {
                        global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
                            message: {
                                source_guid: guid(),
                                text: 'http://www.reddit.com/r/' + subreddit
                            }
                        });
                    }, global.groupmeWaitTime);
                } else if(req.body.text.toLowerCase().indexOf('connorbot') > -1) {
                    var saying = common.connorisms[Math.floor(Math.random() * common.connorisms.length)];
                    var attachments = [{
                        loci: [],
                        type: 'mentions',
                        user_ids: []
                    }];
                    attachments[0].loci.push([0, (req.body.name).length]);
                    attachments[0].user_ids.push(req.body.user_id);
                    setTimeout(function () {
                        global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
                            message: {
                                source_guid: guid(),
                                text: req.body.name + ', '+ saying,
                                attachments: attachments
                            }
                        });
                    }, global.groupmeWaitTime);
                } else {
                    if (req.body.text.indexOf('changed the group\'s name to') > -1) {
                        logger.info('[PARSE] Finding Community By Name', 'get/community');
                        global.parse.findMany('gctCommunities', {}, function (err, response) {
                            response.results.forEach(function (community) {
                                if (community.socialConnections && community.socialConnections.groupme && community.socialConnections.groupme.groupData) {
                                    for (var i in community.socialConnections.groupme.groupData) {
                                        if (i === req.body.group_id && community.socialConnections.groupme.groupData[i].lockName) {
                                            logger.info('[GROUPME] Changing Name Back', 'post/groupme/callback');
                                            global.groupme('groups/' + req.body.group_id + '/update', 'POST', {
                                                name: community.socialConnections.groupme.groupData[i].lockNameValue
                                            });
                                        }
                                    }
                                }
                            });
                        });
                    }
                    if (global.groupmeTriggers[req.body.group_id]) {
                        var found = false;
                        global.groupmeTriggers[req.body.group_id].forEach(function (trigger) {
                            if (req.body.text.toLowerCase().indexOf(trigger.trigger.toLowerCase()) > -1 && !found) {
                                found = true;
                                setTimeout(function () {
                                    global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
                                        message: {
                                            source_guid: guid(),
                                            text: trigger.response
                                        }
                                    });
                                }, global.groupmeWaitTime);
                            }
                        })
                    }
                    if (req.body.text.toLowerCase().indexOf('awood is a homo') > -1) {
                        setTimeout(function () {
                            global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
                                message: {
                                    source_guid: guid(),
                                    text: "http://www.quickmeme.com/img/8a/8a8d32df11bd447ff64e8652dc10f093c9341fb87419ea4cdcd89ebdb8b2c947.jpg"
                                }
                            });
                        }, global.groupmeWaitTime);
                    }
                }
            } else {
                // This is a type callback
                var typeStepData = global.persist.getItem('typeStepData') || {};
                var step = 0;
                if (typeStepData[req.body.group_id]) {
                    step = typeStepData[req.body.group_id].step;
                } else {
                    typeStepData[req.body.group_id] = {
                        step: step
                    }
                }
                var runner = require('./types/' + req.query.type + '/step' + step + '.js');
                setTimeout(function () {
                    runner(guid, req.body, req.query.communityID, typeStepData);
                }, global.groupmeWaitTime);
            }
        }

        res.send({});

        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }
    });
};

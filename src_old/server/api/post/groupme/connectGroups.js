var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var updateCommunity = require(global.utilsPath + '/updateCommunity');
var updateCore = require(global.utilsPath + '/updateCore');
var Q = require('q');

module.exports = function(app) {
    app.post('/api/groupme/connectGroups', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[POST] /api/groupme/connectGroups', 'post/groupme/connectGroups');

        if (requireAuth(logger, req, res) && requiredParams(logger, req, res, ['groups'])) {
            logger.info('[PARSE] Finding Community By Name', 'get/community');
            global.parse.find('gctCommunities', req.session.community, function (err, response) {
                if (parseErrorHandler(logger, err, response)) {
                    var nickname = response.name + (response.name.indexOf('Community') === -1 || response.name.indexOf('community') === -1 ? ' Community' : '') + ' Bot';
                    // Update The Nickname
                    var promises = [];
                    var groups = [];
                    promises.push(global.groupme('groups', 'GET'));
                    for (var i in req.body.groups) {
                        groups.push(i);
                        var promise = global.groupme('bots', 'POST', {
                            bot: {
                                name: nickname,
                                group_id: i,
                                callback_url: 'http://gamecomtools.com/api/groupme/callback?communityId=' + req.session.community
                            }
                        });
                        promises.push(promise);
                        promise = global.groupme('groups/' + i + '/memberships/update', 'POST', {
                            membership: {
                                nickname: nickname
                            }
                        });
                        promises.push(promise);
                    }
                    Q.all(promises).then(function(responses) {
                        var groupsResponse = responses[0].response;
                        var promises = [];
                        groupsResponse.forEach(function (value) {
                            if (groups.indexOf(value.group_id) > -1) {
                                var promise = global.groupme('groups/' + value.group_id + '/messages', 'POST', {
                                    message: {
                                        source_guid: 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                                            var r = Math.random() * 16 | 0, v = c == 'x' ? r : r & 0x3 | 0x8;
                                            return v.toString(16);
                                        }),
                                        text: 'Welcome to GameComTools, powered by The Reddit Rebels! I am now active and ready for commands! You can use !help to see all available commands.'
                                    }
                                });
                                promises.push(promise);
                            }
                        });
                        return Q.all(promises);
                    }).then(function(){
                        return updateCore(logger,'knownGroupMeGroups', {
                            groups: groups
                        })
                    }).then(function(){
                        return updateCommunity(logger,req.session.community, {
                            socialConnections: {
                                groupme: {
                                    groups: req.body.groups
                                }
                            }
                        })
                    }).then(function(){
                        require(global.utilsPath + '/updateGroupMeTriggers')();
                        res.send({
                            result: 'success'
                        });
                    })
                }
            });
        }
    });
};

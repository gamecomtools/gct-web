var requiredParams = require(global.utilsPath + '/requiredParams');
var loggerUtil = require(global.utilsPath + '/logger');
var requireAuth = require(global.utilsPath + '/requireAuth');
var updateCommunity = require(global.utilsPath + '/updateCommunity');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(app) {
    app.post('/api/community/update', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[POST] /api/community/update', 'post/community/update');

        if (requireAuth(logger, req, res) && requiredParams(logger, req, res, ['community'])) {
            if (req.body.community.id !== req.session.community){
                return res.status(401).send({error: 'Not Authorized'});
            }
            logger.info('Updating Community', 'post/community/update');
            updateCommunity(logger, req.session.community, req.body.community, req.body.fullUpdate).then(function(community){
                require(global.utilsPath + '/updateGroupMeTriggers')();
                var q = require('q');
                var promises = [];
                if (req.body.syncRoles) {
                    var defer = q.defer();

                    logger.info('[PARSE] Finding Users By Community', 'post/community/update');
                    var query = {
                        community: req.session.community
                    };
                    global.parse.findMany('gctUsers', query, function (err, response) {
                        if (parseErrorHandler(logger, err, response)) {
                            response.results.forEach(function(user){
                                if (req.body.uU) {
                                    for( var i in req.body.uU) {
                                        if (req.body.uU[i] === user.username) {
                                            global.parse.update('gctUsers', user.objectId, {
                                                role: i
                                            }, function(){});
                                        }
                                    }
                                } else if (!req.body.community.hierarchyGroups[user.role]) {
                                    global.parse.update('gctUsers', user.objectId, {
                                        role: 'na'
                                    }, function(){});
                                }
                            });
                            defer.resolve();
                        }
                    });

                    promises.push(defer.promise);
                }
                q.all(promises).then(function(){
                    res.send(community);
                });
            });
        }else{
            logger.complete();
        }
    });
};

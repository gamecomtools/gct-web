var passwordUtil = require(global.utilsPath + '/password');
var requiredParams = require(global.utilsPath + '/requiredParams');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(app) {
    app.post('/api/user', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[POST] /api/user', 'post/user');

        var bypassAuth = false;
        if (req.session.user && req.body.password === undefined) {
            logger.info('Bypass Auth Due To Session', 'post/user');
            bypassAuth = true;
            req.body.username = req.session.user;
            req.body.password = 'invalid';
        }

        if (requiredParams(logger, req, res, ['password', 'username'])) {
            logger.info('[PARSE] Finding User By Username', 'post/user');
            global.parse.find('gctUsers', {username: req.body.username}, function (err, response) {
                if (parseErrorHandler(logger, err, response)) {
                    if (response.results.length > 0) {
                        var user = response.results[0];
                        logger.info('Comparing Passwords', 'post/user');
                        return passwordUtil.comparePassword(req.body.password, user.password, function (err, isMatch) {
                            if (err) {
                                logger.error('PassComp Err: ' + err, 'post/user');
                                return res.send({
                                    error: "Unknown Error Occurred (passComp)"
                                });
                            }
                            if (isMatch || bypassAuth) {
                                logger.info('Password Matched', 'post/user');
                                req.session.user = req.body.username;
                                req.session.community = user.community;
                                delete user.password;
                                user.id = user.objectId;
                                delete user.objectId;
                                logger.complete();
                                return res.send(user);
                            }
                            logger.error('Invalid Password', 'post/user');
                            logger.complete();
                            return res.send({
                                error: "Invalid Username/Password"
                            });
                        });
                    }
                    logger.error('Invalid Username', 'post/user');
                    logger.complete();
                    return res.send({
                        error: "Invalid Username/Password"
                    });
                }else {
                    logger.complete();
                    return res.send({
                        error: "Unknown Error (Parse)"
                    });
                }
            });
        }else{
            logger.complete();
        }
    });
};

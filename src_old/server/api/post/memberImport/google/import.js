var requiredParams = require(global.utilsPath + '/requiredParams');
var requireAuth = require(global.utilsPath + '/requireAuth');
var loggerUtil = require(global.utilsPath + '/logger');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var updateCommunity = require(global.utilsPath + '/updateCommunity');
var memberSync = require(global.utilsPath + '/memberSyncGoogleSheets');
var Q = require('q');

module.exports = function(app) {
    app.post('/api/memberImport/google/import', function(req, res, next){
        var logger = new loggerUtil(req.session.id);
        logger.access('[POST] /api/memberImport/google/import', 'post/memberImport/google/import');

        if (requireAuth(logger, req, res) && requiredParams(logger, req, res, ['sheetUrl', 'worksheet', 'columnTrackingNames'])) {
            memberSync(logger, req.body.sheetUrl, req.body.worksheet, req.body.columnTrackingNames, req.session.community).then(function(result) {
                logger.info('Updating Community', 'post/memberImport/google/import');
                updateCommunity(logger, req.session.community, {
                    memberImportSources: [{
                        type: 'googleSheet',
                        sheetURL: req.body.sheetUrl,
                        worksheet: req.body.worksheet,
                        columnTrackingNames: req.body.columnTrackingNames
                    }]
                }).then(function(community){
                    res.send(result);
                });
            }, function(result) {
                logger.complete();
                res.send(result);
            });
        }else{
            logger.complete();
        }
    });
};

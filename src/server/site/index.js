var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var appRoot = require('app-root-path');
var fs = require('fs');
process.env.env = 'DEV';
var indexHtml = appRoot.path + '/dist/client/index.html';
if (fs.existsSync(appRoot.path + '/client')) {
    process.env.env = 'PROD';
    indexHtml = appRoot.resolve('client/index.html');
}
console.log('Environment: ' + process.env.env);
require('../utils/dependencyServiceSetup')();

if (process.env.env !== 'PROD') {
    global.webHost = 'localhost:3000';
}

// Session setup
var session = require('express-session');
var FileStore = require('session-file-store')(session);
options = {
    path: '/tmp/sessions/'
};
app.set('view engine', 'jade');
app.use(session({
    store: new FileStore(options),
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}));

app.use(bodyParser());

app.use(function(req, res, next){
    if(req.url.indexOf('.') == -1 && req.url.indexOf('api') == -1  && req.url.indexOf('kue') == -1) {
        return res.sendFile(indexHtml);
    }
    next();
});

if (fs.existsSync(appRoot.path + '/client')) {
    app.use(express.static(appRoot.path + '/client'));
    app.use(express.static(appRoot.path + '/'));
} else {
    app.use(express.static(appRoot.path + '/dist'));
}

module.exports = app;

var ts3 = require("node-ts");

module.exports = function(req, res, next) {
    req.body.teams = JSON.parse(req.body.teams);
    var cl = new ts3.TeamSpeakClient('ts85.gameservers.com', 9100);
    cl.send("login", {
            client_login_name: "automation",
            client_login_password: "ywkqv2Ix"
        })
        .then(function(_) {
            return cl.send("use", {port: 9146})
        })
        .then(function(_) {
            return cl.send("channellist", {}, []);
        })
        .then(function(channellist) {
            var parentCid = 0;
            channellist.response.forEach(function(channel) {
                if (channel.channel_name === 'Team Channels') {
                    parentCid = channel.cid;
                }
            });
            var promises = [];
            for (var i in req.body.teams) {
                var team = i.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
                promises.push(cl.send('channelcreate', {
                    channel_name: team + ' Team',
                    channel_password: '5631',
                    channel_flag_semi_permanent: 1,
                    cpid: parentCid
                }, []));
            }
            return promises;
        })
        .spread(function() {
            return [
                cl.send("clientlist", {}, []),
                cl.send("channellist", {}, [])
            ];
        })
        .spread(function(clientlist, channellist) {
            var promises = [];
            for (var i in req.body.teams) {
                var team = i.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
                var cid = 0;
                channellist.response.forEach(function(channel) {
                    if (channel.channel_name === team + ' Team') {
                        cid = channel.cid;
                    }
                });
                if (cid !== 0) {
                    req.body.teams[i].forEach(function(nickname) {
                        clientlist.response.forEach(function(client) {
                            if (client.client_nickname === nickname) {
                                promises.push(cl.send("clientmove", {
                                    clid: client.clid,
                                    cid: cid,
                                    cpw: "5631"
                                }, []));
                            }
                        })
                    });
                }
            }
            return promises;
        })
        .spread(function() {
            res.send({success: 1});
        })
        .fail(function(err) {
            console.log(err);
            res.send({success: 0});
        });
};

module.exports.requiredParams = [
    'teams'
];

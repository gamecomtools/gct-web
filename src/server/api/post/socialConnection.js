var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var updateCommunity = require(global.utilsPath + '/updateCommunity');

module.exports = function(req, res, next) {
    global.parse.find('gctCommunities', req.session.community, function (err, response) {
        if (parseErrorHandler(err, response)) {
            if (response.socialNetworks.indexOf(req.body.network) === -1) {
                response.socialNetworks.push(req.body.network);
                if (!response.socialConnections) {
                    response.socialConnections = {};
                }
                response.socialConnections[req.body.network] = {};
                if (req.body.network === 'reddit') {
                    response.socialConnections[req.body.network] = {
                        subreddit: req.body.data.subreddit
                    };
                    // Accept the mod invite and mark the message as read
                    reddit('/r/' + req.body.data.subreddit + '/api/accept_moderator_invite').post().then(function() {
                        return reddit('/api/read_message').post({
                            id: req.body.data.name
                        })
                    })
                }

                updateCommunity(req.session.community, response).then(function() {
                    res.data = {
                        success: true
                    };
                    next();
                }, function(err) {
                    res.error(err);
                    next();
                })
            } else {
                res.data = {
                    success: true
                };
                next();
            }
        } else {
            res.error("Unable to contact Parse");
            return next();
        }
    });
};

module.exports.requiredParams = [
    'network'
];

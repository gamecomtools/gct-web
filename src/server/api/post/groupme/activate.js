var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var common = require(global.utilsPath + '/common');
var updateCommunity = require(global.utilsPath + '/updateCommunity');

module.exports = function(req, res, next) {
    global.parse.find('gctCommunities', req.session.community, function (err, response) {
        if (parseErrorHandler(err, response)) {
            var nickname = response.name + (response.name.indexOf('Community') === -1 || response.name.indexOf('community') === -1 ? ' Community' : '') + ' Bot';
            // Update the nickname
            global.groupme('groups/' + req.body.groupId + '/memberships/update', 'POST', {
                membership: {
                    nickname: nickname
                }
            }).then(function() {
                // Create the bot
                return global.groupme('bots', 'POST', {
                    bot: {
                        name: response.name + ' - ' + req.body.groupId,
                        group_id: req.body.groupId,
                        callback_url: 'http://gamecomtools.com/api/groupme/callback?communityId=' + req.session.community
                    }
                })
            }).then(function() {
                // Send activation message
                return global.groupme('groups/' + req.body.groupId + '/messages', 'POST', {
                    message: {
                        source_guid: 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                            var r = Math.random() * 16 | 0, v = c == 'x' ? r : r & 0x3 | 0x8;
                            return v.toString(16);
                        }),
                        text: 'Welcome to GameComTools, powered by The Reddit Rebels! I am now active and ready for commands! You can use !help to see all available commands.'
                    }
                })
            }).then(function() {
                var gData = {};
                gData[req.body.groupId] = req.body.groupFunction;
                return updateCommunity(req.session.community, {
                    socialConnections: {
                        groupme: {
                            groups: gData
                        }
                    }
                })
            }).then(function() {
                res.data = {
                    success: true
                };
                next();
            }, function(err) {
                res.error(err);
                next();
            });
        } else {
            res.error("Unable to contact Parse");
            return next();
        }
    });
};

module.exports.requiredParams = [
    'groupId', 'groupFunction'
];

module.exports.requireAuth = true;

var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var common = require(global.utilsPath + '/common');
var updateCommunity = require(global.utilsPath + '/updateCommunity');

module.exports = function(req, res, next) {
    global.parse.find('gctCommunities', req.session.community, function (err, response) {
        if (parseErrorHandler(err, response)) {
            var opts = {
                socialConnections: {
                    groupme: {
                        botOptions: {},
                        groups: {}
                    }
                }
            };

            var botOptions = ['lockTitle', 'lockTitleValue', 'eventNotifications', 'disabled', 'disabledCommands', 'enabledCustomCommands', 'customCooldowns', 'accessControl', 'excludeAutoInvite'];

            function updateBotOptions() {
                if (!opts.socialConnections.groupme.botOptions[req.body.groupId]) {
                    opts.socialConnections.groupme.botOptions[req.body.groupId] = {};
                }
                opts.socialConnections.groupme.botOptions[req.body.groupId][req.body.key] = req.body.value;
                updateCommunity(req.session.community, opts);
            }

            if (req.body.key === 'title') {
                global.groupme('groups/' + req.body.groupId + '/update', 'POST', {
                    name: req.body.value
                })
            } else if (req.body.key === 'directMessage') {
                global.groupme('groups/' + req.body.groupId + '/messages', 'POST', {
                    message: {
                        source_guid: global.guid(),
                        text: req.body.value
                    }
                })
            } else if (botOptions.indexOf(req.body.key) > -1) {
                updateBotOptions();
                if (req.body.key === 'disabled') {
                    if (req.body.value) {
                        global.groupme('groups/' + req.body.groupId + '/messages', 'POST', {
                            message: {
                                source_guid: global.guid(),
                                text: 'I have been disabled by this groups owner. I will no longer monitor this chat group.'
                            }
                        })
                    } else {
                        global.groupme('groups/' + req.body.groupId + '/messages', 'POST', {
                            message: {
                                source_guid: global.guid(),
                                text: 'I have been enabled by this groups owner. I will now monitor this caht group.'
                            }
                        })
                    }
                }
            } else if (req.body.key === 'nickname') {
                global.groupme('groups/' + req.body.groupId + '/memberships/update', 'POST', {
                    membership: {
                        nickname: req.body.value
                    }
                })
            } else if (req.body.key === 'function') {
                if (req.body.value !== 'non-selectable') {
                    opts.socialConnections.groupme.groups[req.body.groupId] = req.body.value;
                    updateCommunity(req.session.community, opts);
                }
            } else if (req.body.key === 'botOptions') {
                opts.socialConnections.groupme.botOptions = req.body.value;
                updateCommunity(req.session.community, opts);
            }
            setTimeout(function() {
                res.data = {
                    success: true
                };
                next();
            }, 500);
        } else {
            res.error("Unable to contact Parse");
            return next();
        }
    });
};

module.exports.requiredParams = [
    'key', 'value', 'groupId'
];

module.exports.requireAuth = true;

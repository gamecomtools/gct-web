var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var q = require('q');
var communityCache = {};
var cacheExpires = {};
var commandCooldowns = {};
var fs = require('fs');
var files = fs.readdirSync(global.utilsPath + '/groupmeCallback/commands/');
var builtInCommands = [];
files.forEach(function(file) {
    builtInCommands.push(file.replace('.js', ''));
});
var callbackStore = global.getPersistent('callbackStore');
if (!callbackStore.communityCache) callbackStore.communityCache = {};
if (!callbackStore.cacheExpires) callbackStore.cacheExpires = {};
cacheExpires = callbackStore.cacheExpires;
communityCache = callbackStore.communityCache;

module.exports = function(req, res, next) {
    res.data = {};
    next();
    req.body.text = req.body.text.trim();
    if (req.body.name.indexOf('Community') === -1 && req.body.name !== 'GameComTools' && req.body.name !== 'GroupMe') {
        if (req.query.type) {
            // This is a specific flow
            var typeStepData = global.persist.getItem('typeStepData') || {};
            var step = 0;
            if (typeStepData[req.body.group_id]) {
                step = typeStepData[req.body.group_id].step;
            } else {
                typeStepData[req.body.group_id] = {
                    step: step
                }
            }
            var runner = require(global.utilsPath + '/groupmeCallback/types/' + req.query.type + '/step' + step + '.js');
            setTimeout(function () {
                runner(global.guid, req.body, req.query.communityID, typeStepData);
            }, global.groupmeWaitTime);
        } else if(req.body.text.indexOf('!') === 0) {
            var promises = [];
            var date = new Date();

            function updateCommunityCache(communityId) {
                var deferred = q.defer();
                date.setMinutes(date.getMinutes() + 15);
                global.parse.find('gctCommunities', req.query.communityId, function (err, response) {
                    if (parseErrorHandler(err, response)) {
                        communityCache[req.query.communityId] = response;
                        cacheExpires[req.query.communityId] = date;
                        deferred.resolve();
                        global.savePersistent();
                    } else {
                        global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
                            "message": {
                                "source_guid": global.guid(),
                                "text": "Uh-oh! I was unable to fulfill your request, please try again in a few minutes."
                            }
                        });
                        deferred.reject();
                    }
                });
                return deferred.promise;
            }
            if (!communityCache[req.query.communityId] || new Date(cacheExpires[req.query.communityId]).getTime() < date.getTime()) {
                var updatePromise = updateCommunityCache(req.query.communityId);
                if (!communityCache[req.query.communityId]) {
                    promises.push(updatePromise);
                }
            }
            q.all(promises).delay(global.groupmeWaitTime).then(function() {
                try {
                    var command = req.body.text.split(' ')[0].replace('!', '');
                    var commandCooldown = global.getPersistent('commandCooldown-' + req.body.group_id);
                    var cooldown = undefined;
                    var unknown = false;
                    var unknownErr = '';
                    var disabled = false;
                    if (builtInCommands.indexOf(command) > -1) {
                        if (communityCache[req.query.communityId].socialConnections.groupme.botOptions &&
                            communityCache[req.query.communityId].socialConnections.groupme.botOptions[req.body.group_id]) {
                            if (communityCache[req.query.communityId].socialConnections.groupme.botOptions[req.body.group_id].disabled) {
                                disabled = true;
                            }
                            if (communityCache[req.query.communityId].socialConnections.groupme.botOptions[req.body.group_id].disabledCommands &&
                                communityCache[req.query.communityId].socialConnections.groupme.botOptions[req.body.group_id].disabledCommands[command]) {
                                unknown = true;
                            }
                            if (communityCache[req.query.communityId].socialConnections.groupme.botOptions[req.body.group_id].customCooldowns &&
                                communityCache[req.query.communityId].socialConnections.groupme.botOptions[req.body.group_id].customCooldowns[command]) {
                                cooldown = communityCache[req.query.communityId].socialConnections.groupme.botOptions[req.body.group_id].customCooldowns[command];
                                if (cooldown === '') {
                                    cooldown = undefined;
                                } else {
                                    cooldown = parseInt(cooldown);
                                }
                            }
                        }

                        if (communityCache[req.query.communityId].socialConnections.groupme.groups[req.body.group_id]) {
                            req.body.groupFunction = communityCache[req.query.communityId].socialConnections.groupme.groups[req.body.group_id];
                        }
                        if (!unknown && !disabled) {
                            var now = new Date();
                            if (commandCooldown[command] === undefined || commandCooldown[command] <= now.getTime()) {
                                var cmd = require(global.utilsPath + '/groupmeCallback/commands/' + command + '.js');
                                var found = false;
                                module.parent.children.forEach(function (mod) {
                                    var cmdName = mod.id.split('/');
                                    cmdName = cmdName[cmdName.length - 1].replace('.js', '');
                                    if (cmdName.toLowerCase() === command.toLowerCase()) {
                                        found = true;
                                        if (!mod.limitCommand) {
                                            var meetsRequriement = false;
                                            if (mod.gameRequirements && req.body.groupFunction !== 'main' && req.body.groupFunction !== 'leadership') {
                                                mod.gameRequirements.forEach(function(game) {
                                                    if (game === req.body.groupFunction) {
                                                        meetsRequriement = true;
                                                    }
                                                });
                                                if (!meetsRequriement) {
                                                    unknown = true;
                                                    unknownErr = 'NO_GAME_REQ';
                                                }
                                            }
                                            if (mod.communityRequriements) {
                                                function getProperty(obj, prop) {
                                                    var parts = prop.split('.'),
                                                        last = parts.pop(),
                                                        l = parts.length,
                                                        i = 1,
                                                        current = parts[0];

                                                    if (l === 0) {
                                                        return obj[prop];
                                                    }

                                                    while((obj = obj[current]) && i < l) {
                                                        current = parts[i];
                                                        i++;
                                                    }

                                                    if(obj) {
                                                        return obj[last];
                                                    }
                                                }
                                                meetsRequriement = true;
                                                mod.communityRequriements.forEach(function(requirement) {
                                                    var value = getProperty(communityCache[req.query.communityId], requirement);
                                                    if (value === undefined || (typeof value === 'string' && value === '')) {
                                                        meetsRequriement = false;
                                                    }
                                                });
                                                if (!meetsRequriement) {
                                                    unknown = true;
                                                    unknownErr = 'NO_COM_REQ';
                                                }
                                            }
                                            var accessControl = global.getPersistent('accessControl-' + req.query.communityId);
                                            var allowedAccess = true;
                                            if (accessControl[req.body.group_id] && accessControl[req.body.group_id][command]) {
                                                allowedAccess = false;
                                                if (accessControl[req.body.group_id][command] === 'none') {
                                                    allowedAccess = true;
                                                } else if(accessControl[req.body.group_id][command].indexOf(req.body.user_id) > -1) {
                                                    allowedAccess = true;
                                                }
                                            }
                                            if (!unknown) {
                                                if (allowedAccess) {
                                                    cmd(req.body, req.query, communityCache[req.query.communityId]);

                                                    var cooldownTime = cooldown;
                                                    if (cooldownTime === undefined) {
                                                        cooldownTime = mod.cooldownTime;
                                                    }
                                                    if (cooldownTime === undefined) {
                                                        cooldownTime = 10;
                                                    }
                                                    now.setMinutes(now.getMinutes() + cooldownTime);
                                                    commandCooldown[command] = now.getTime();
                                                    global.savePersistent();
                                                } else {
                                                    global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
                                                        "message": {
                                                            "source_guid": global.guid(),
                                                            "text": "You are not allowed to use that command."
                                                        }
                                                    });
                                                }
                                            }
                                        } else {
                                            unknownErr = 'LIMIT';
                                            unknown = true;
                                        }
                                    }
                                });
                                if (!found) {
                                    unknownErr = 'NO_FOUND';
                                    unknown = true;
                                }
                            } else {
                                var diffMs = (commandCooldown[command] - now.getTime()); // milliseconds between now & Christmas
                                var diffMins = Math.round(diffMs / 1000 / 60); // minutes
                                var diffSeconds = Math.round(diffMs / 1000); // seconds

                                var message = 'That command was just used! Try again in ';
                                if (diffSeconds < 60) {
                                    message += diffSeconds + ' seconds!';
                                } else {
                                    message += diffMins + ' minutes!';
                                }
                                global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
                                    message: {
                                        source_guid: guid(),
                                        text: message
                                    }
                                });
                            }
                        }
                    } else {
                        unknown = true;
                        unknownErr = 'NO_FOUND';
                    }

                    if (unknown && !disabled) {
                        global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
                            "message": {
                                "source_guid": global.guid(),
                                "text": "Unknown Command (" + unknownErr + ")"
                            }
                        });
                    }
                } catch(err) {
                    console.log(err);
                    console.log(err.stack);
                    var text = "";
                    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                    for ( var i=0; i < 5; i++ ) {
                        text += possible.charAt(Math.floor(Math.random() * possible.length));
                    }
                    var errors = global.getPersistent('errors-' + req.query.communityId);
                    errors.errs = errors.errs || [];
                    var now = new Date();
                    errors.errs.push({
                        type: 'groupMeCallbackCommand',
                        date: now.toJSON(),
                        id: text,
                        data: {
                            groupID: req.body.group_id,
                            text: req.body.text,
                            stack: err.stack
                        }
                    });
                    global.savePersistent();
                    global.groupme('groups/' + req.body.group_id + '/messages', 'POST', {
                        "message": {
                            "source_guid": global.guid(),
                            "text": "Well this is embarrassing but I ran into an error. Please try again in a few moments. If this continues, please contact support. (ERR-ID: " + text + ")"
                        }
                    });
                }
            });
        }
    } else if(req.body.name === 'GroupMe') {
        if (req.body.text.indexOf('changed the group\'s name to')) {
            global.parse.find('gctCommunities', req.query.communityId, function (err, response) {
                if (parseErrorHandler(err, response)) {
                    if (
                        response.socialConnections &&
                        response.socialConnections.groupme &&
                        response.socialConnections.groupme.botOptions &&
                        response.socialConnections.groupme.botOptions[req.body.group_id] &&
                        response.socialConnections.groupme.botOptions[req.body.group_id].lockTitle) {
                        global.groupme('groups/' + req.body.group_id + '/update', 'POST', {
                            name: response.socialConnections.groupme.botOptions[req.body.group_id].lockTitleValue
                        });
                    }
                }
            });
        }
    }
};

var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var common = require(global.utilsPath + '/common');
var updateCommunity = require(global.utilsPath + '/updateCommunity');

module.exports = function(req, res, next) {
    updateCommunity(req.session.community, {
        socialConnections: {
            groupme: {
                autoInvites: req.body.data
            }
        }
    }).then(function() {
        res.data = {
            success: true
        };
        next();
    })
};

module.exports.requiredParams = [
    'data'
];

module.exports.requireAuth = true;

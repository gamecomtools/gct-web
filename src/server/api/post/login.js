var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var passwordUtil = require(global.utilsPath + '/password');

module.exports = function(req, res, next) {
    var bypassAuth = false;
    if (req.session.user) {
        bypassAuth = true;
        req.body.username = req.session.user;
        req.body.password = 'invalid';
    }

    global.parse.find('gctUsers', {username: req.body.username}, function (err, response) {
        if (parseErrorHandler(err, response)) {
            if (response.results.length > 0) {
                var user = response.results[0];
                return passwordUtil.comparePassword(req.body.password, user.password, function (err, isMatch) {
                    if (err) {
                        res.error("Unknown Error Occurred (passComp)");
                        return next();
                    }
                    if (isMatch || bypassAuth) {
                        req.session.user = req.body.username;
                        req.session.community = user.community;
                        delete user.password;
                        user.id = user.objectId;
                        delete user.objectId;
                        res.data = user;
                        return next();
                    }
                    res.error("Invalid Username/Password");
                    return next();
                });
            } else {
                res.error("Invalid Username/Password");
                return next();
            }
        } else {
            res.error("Unable to contact Parse");
            return next();
        }
    });
};

module.exports.requiredParams = [
    'username', 'password'
];

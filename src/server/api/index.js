var fs = require('fs');
var files = fs.readdirSync(global.utilsPath + '/groupmeCallback/commands/');
files.forEach(function(file) {
    require(global.utilsPath + '/groupmeCallback/commands/' + file);
});

module.exports = function(app) {
    var Q = require('q');
    var deferred = Q.defer();
    var fs = require('fs');

    var recursive = require('recursive-readdir');

    app.use('/api', function(req, res, next) {
        res.data = {};

        res.error = function(message, statusCode) {
            if (!res.wasError) {
                res.wasError = true;
                message = message || 'An Unknown Error Has Occurred';
                statusCode = statusCode || 200;
                res.send(statusCode, {
                    error: message
                });
            }
        };

        if (app._data[req.method.toLowerCase()][req.originalUrl.split('?')[0].replace(/\/$/, '')]) {
            var module = require('./' + app._data[req.method.toLowerCase()][req.originalUrl.split('?')[0].replace(/\/$/, '')]);
            if (module.requiredParams) {
                module.requiredParams.forEach(function (param) {
                    if (req.body[param] === undefined && req.query[param] === undefined) {
                        res.error('Invalid Input');
                    }
                });
            }
            if (module.requireAuth) {
                if (!req.session.user) {
                    res.error('Authorization Required!', 401);
                }
            }
        } else {
            res.error('Invalid Endpoint');
        }
        next();
    });

    recursive(__dirname + '/', function (err, files) {
        app._data = {};
        files.forEach(function(file) {
            file = file.replace(__dirname + '/', '');
            if (file !== 'index.js' && file.indexOf('.js') > -1) {
                var parts = file.split('/');
                var method = parts[0];
                parts.splice(0, 1);
                var path = '/api/' + parts.join('/').replace('.js', '');
                if (!app._data[method]) {
                    app._data[method] = {};
                }
                app._data[method][path] = file;
                app[method](path, require('./' + file));
            }
        });

        app.use('/api', function(req, res, next) {
            if (!res.headersSent || !res.wasError) {
                res.send(res.data);
            }
            next();
        });
        deferred.resolve();
    });

    return deferred.promise;
};

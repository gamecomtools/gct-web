var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var passwordUtil = require(global.utilsPath + '/password');
var common = require(global.utilsPath + '/common');
var fs = require('fs');
var files = fs.readdirSync(global.utilsPath + '/groupmeCallback/commands/');
var commands = [];
files.forEach(function(file) {
    commands.push(file.replace('.js', ''));
});

module.exports = function(req, res, next) {
    global.parse.find('gctCommunities', req.session.community, function (err, community) {
        global.groupme('users/me', 'GET').then(function (me) {
            global.groupme('groups?per_page=100', 'GET', {}, true).then(function (response) {
                var groups = response.response;
                var resp = {
                    me: me.response,
                    groups: [],
                    groupMeGameMap: common.groupMeGameMap,
                    groupMeGameTypes: common.groupMeGameTypes,
                    builtInCommands: []
                };

                commands.forEach(function(command) {
                    module.parent.children.forEach(function (mod) {
                        var cmd = mod.id.split('/');
                        if (cmd[cmd.length - 1] === command + '.js') {
                            resp.builtInCommands.push({
                                name: command,
                                description: mod.helpText,
                                coolDownTime: (mod.cooldownTime === undefined ? 10 : mod.cooldownTime),
                                gameRequirements: mod.gameRequirements || []
                            });
                        }
                    });
                });

                var incomingGroups = req.query.groups.split(',');

                groups.forEach(function (group, index) {
                    if (incomingGroups.indexOf(group.group_id) > -1) {
                        group.botNickname = '';
                        group.members.forEach(function (member, i) {
                            if (member.user_id === resp.me.user_id) {
                                group.botNickname = member.nickname;
                            }
                        });
                        group.botOptions = {};
                        if (community.socialConnections.groupme.botOptions) {
                            group.botOptions = community.socialConnections.groupme.botOptions[group.id] || {};
                        }
                        resp.groups.push(group);
                    }
                });
                res.data = resp;
                next();
            });
        });
    });
};

module.exports.requiredParams = [
    'groups'
];

module.exports.requireAuth = true;

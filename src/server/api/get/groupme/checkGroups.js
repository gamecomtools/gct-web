var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var common = require(global.utilsPath + '/common');

module.exports = function(req, res, next) {
    global.groupme('users/me', 'GET').then(function (me) {
        global.groupme('groups?per_page=100', 'GET', {}, true).then(function (response) {
            var groups = response.response;
            var resp = {
                groups: [],
                groupMeGameMap: common.groupMeGameMap,
                groupMeGameTypes: common.groupMeGameTypes
            };

            groups.forEach(function (group, index) {
                group.botNickname = '';
                group.members.forEach(function (member, i) {
                    if (member.user_id === me.response.user_id) {
                        group.botNickname = member.nickname;
                    }
                });
                if (group.botNickname === 'GameComTools' && group.creator_user_id !== me.response.user_id) {
                    resp.groups.push(group);
                }
            });
            res.data = resp;
            next();
        });
    });
};

module.exports.requireAuth = true;

var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var common = require(global.utilsPath + '/common');
var updateCommunity = require(global.utilsPath + '/updateCommunity');

module.exports = function(req, res, next) {
    global.groupme('groups', 'GET', {}, true).then(function(resp) {
        var data = [];
        resp.response.forEach(function(group) {
            if (group.name.indexOf('The Reddit Rebels Bot - ') === 0 && group.description === "Welcome to the The Reddit Rebels") {
                data.push(group.name.split(' - ')[1]);

            }
        });
        res.data = data;
        next();
    });
};

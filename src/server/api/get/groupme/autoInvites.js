module.exports = function(req, res, next) {
    res.data = global.getPersistent(req.session.community + '-groupmeInvites') || {};
    next();
};

module.exports.requireAuth = true;

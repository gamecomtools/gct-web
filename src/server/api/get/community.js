var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var passwordUtil = require(global.utilsPath + '/password');

module.exports = function(req, res, next) {
    global.parse.find('gctCommunities', req.query.communityId, function (err, response) {
        if (parseErrorHandler(err, response)) {
            res.data = response;
            next();
        } else {
            res.error("Unable to contact Parse");
            return next();
        }
    });
};

module.exports.requiredParams = [
    'communityId'
];

module.exports.requireAuth = true;

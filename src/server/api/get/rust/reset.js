var ts3 = require("node-ts");

module.exports = function(req, res, next) {
    var cl = new ts3.TeamSpeakClient('ts85.gameservers.com', 9100);
    cl.send("login", {
            client_login_name: "automation",
            client_login_password: "ywkqv2Ix"
        })
        .then(function(_) {
            return cl.send("use", {port: 9146})
        })
        .then(function(_) {
            return [
                cl.send("clientlist", {}, []),
                cl.send("channellist", {}, [])
            ];
        })
        .spread(function(clientlist, channellist) {
            var lobbyCid = 0;
            channellist.response.forEach(function(channel) {
                if (channel.channel_name.indexOf('Game Lobby') === 0) {
                    lobbyCid = channel.cid;
                }
            });
            var promises = [];
            channellist.response.forEach(function(channel) {
                clientlist.response.forEach(function(client) {
                    if (client.cid === channel.cid) {
                        promises.push(cl.send("clientmove", {
                            clid: client.clid,
                            cid: lobbyCid,
                            cpw: "5631"
                        }, []));
                    }
                });
                if (channel.pid !== 0) {
                    promises.push(cl.send('channeldelete', {
                        cid: channel.cid,
                        force: 1
                    }));
                }
            });
            return promises;
        })
        .spread(function() {
            res.send({success: 1});
        })
        .fail(function(err) {
            console.log(err);
            res.send({success: 0});
        });
};

var ts3 = require("node-ts");

module.exports = function(req, res, next) {
    var cl = new ts3.TeamSpeakClient('ts85.gameservers.com', 9100);
    cl.send("login", {
        client_login_name: "automation",
        client_login_password: "ywkqv2Ix"
    })
        .then(function(_) {
            return cl.send("use", {port: 9146})
        })
        .then(function(_) {
            return [
                cl.send("clientlist", {}, []),
                cl.send("channellist", {}, [])
            ]
        })
        .spread(function(clientlist, channellist) {
            var welcomeCid = 0;
            var clients = [];
            channellist.response.forEach(function(channel) {
                if (channel.channel_name === 'Welcome') {
                    welcomeCid = channel.cid;
                }
            });
            clientlist.response.forEach(function(client) {
                if (client.cid === welcomeCid && client.client_type === 0) {
                    clients.push(client.client_nickname);
                }
            });
            res.send(clients);
        })
        .fail(function(err) {
            res.send({success: 0});
        });
};

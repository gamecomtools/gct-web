var ts3 = require("node-ts");

module.exports = function(req, res, next) {
    var cl = new ts3.TeamSpeakClient('ts85.gameservers.com', 9100);
    cl.send("login", {
        client_login_name: "automation",
        client_login_password: "ywkqv2Ix"
    })
        .then(function(_) {
            return cl.send("use", {port: 9146})
        })
        .then(function(_) {
            return [
                cl.send("clientlist", {}, []),
                cl.send("channellist", {}, [])
            ]
        })
        .spread(function(clientlist, channellist) {
            var lobbyCid = 0;
            var c = null;
            channellist.response.forEach(function(channel) {
                if (channel.channel_name.indexOf('Game Lobby') === 0) {
                    lobbyCid = channel.cid;
                }
            });
            clientlist.response.forEach(function(client) {
                if (client.client_nickname === req.query.nickname) {
                    c = client;
                }
            });
            if (c !== null) {
                res.send(c);
                return cl.send("clientmove", {
                    clid: c.clid,
                    cid: lobbyCid,
                    cpw: "5631"
                }, [])
            } else {
                res.send({success: 0});
            }
        })
        .then(function() {
            res.send({success: 1});
        })
        .fail(function(err) {
            res.send({success: 0});
        });
};

module.exports.requiredParams = [
    'nickname'
];

module.exports = function(req, res, next) {
    var webshot = require('webshot');
    var fs      = require('fs');

    // TODO: Make the widget width dynamic based on stylesheet
    var options = {
        windowSize: {
            width: 268,
            height: 1
        },
        shotSize: {
            width: 'window',
            height: 'all'
        }
    };
    if (req.query.widget === 'gctTeamspeakSessionsWidget') {
        options.shotSize.width = 'all';
        options.windowSize.width = 400;
    }

    var query = "";
    for(var i in req.query){
        query += i + '=' + req.query[i] + '&';
    }

    console.log('[WEBSHOT] Getting: http://' + global.webHost + '/api/widget/source/' + req.query.widget + '?' + query);

    var tmpFileName = '/tmp/' + req.query.widget + '-' + req.query.communityID + '.png';
    webshot('http://' + global.webHost + '/api/widget/source/' + req.query.widget + '?' + query, tmpFileName, options, function(err) {
        if (err !== null) {
            console.log('[WEBSHOT] ERR: ' + err);
        } else {
            res.sendFile(tmpFileName);
        }
    });
};

module.exports = function(req, res, next) {
    if (req.session.user) {
        res.data = {
            valid: true
        };
        return next();
    }
    res.data = {
        valid: false
    };
    next();
};

var request = require('request');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var googleEventHelper = require(global.utilsPath + '/googleEventHelper');

module.exports = function(req, res, next) {
    if (req.query.communityID === undefined) {
        return res.send('Invalid Community ID');
    }

    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    global.parse.find('gctCommunities', req.query.communityID, function (err, community) {
        if (parseErrorHandler(err, community)) {
            var min = new Date().toJSON();
            if (community.googleCalendarEmail) {
                var url = 'https://www.googleapis.com/calendar/v3/calendars/' + community.googleCalendarEmail + '/events?maxResults=' + 10 + '&key=' + global.googleKey + '&orderBy=startTime&singleEvents=true&timeMin=' + min;
                request.get(url, function (err, response, body) {
                    var cal = JSON.parse(body);
                    body = googleEventHelper(cal);

                    var sessions = [];
                    var now = new Date();
                    body.forEach(function (event) {
                        if (event.data.platform === 'PC') {
                            var start = new Date(event.originalStartTime.dateTime);
                            if (start <= now) {
                                //event is occurring now
                                event.data.occurringNow = true;
                            } else {
                                // event is in the future
                                event.data.occurringNow = false;
                            }
                            event.data.start = getTimeString(start);
                            if (start.getDate() !== now.getDate()) {
                                event.data.start = days[start.getDay()] + ' @ ' + event.data.start;
                            }
                            if (sessions.length < 2) {
                                sessions.push(event);
                            }
                        }
                    });

                    res.render(__dirname + '/gctTeamspeakSessionsWidget', {
                        sessions: sessions,
                        stylesheet: "https://www.reddit.com/r/" + community.socialConnections.reddit.subreddit + "/stylesheet/"
                    });
                });
            } else {
                res.setStatus(500);
            }
        } else {
            res.setStatus(500);
        }
    });

    function getTimeString(d) {
        var h = d.getHours();
        var m = d.getMinutes();
        var a = 'AM';
        if (h >= 12) {
            a = 'PM';
            if (h !== 12) {
                h = h - 12;
            }
        }
        if (h === 0) {
            h = 12;
        }
        if (m < 10) {
            m = "0" + m;
        }

        return h + ':' + m + ' ' + a;
    }
};

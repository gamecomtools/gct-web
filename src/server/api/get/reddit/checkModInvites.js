module.exports = function(req, res, next) {
    reddit('/message/unread').get().then(function (data) {
        if (data.data.children.length === 0) {
            return res.send({status: 'noInvites'});
        }
        if (data.data.children[0].data.distinguished !== 'moderator') {
            return res.send({status: 'noInvites'});
        }
        res.send({
            status: 'invite',
            subreddit: data.data.children[0].data.subreddit,
            name: data.data.children[0].data.name
        });
    }, function () {
        return res.send({status: 'noInvites'});
    });
};

module.exports.requireAuth = true;

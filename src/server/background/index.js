module.exports = function(app){
    var devTasks = [
        //'accessControlUpdate'
    ];
    var Q = require('q');
    var deferred = Q.defer();
    var path = require('path');
    global.helpersPath = path.resolve(__dirname + '/helpers');
    global.queueMod = {};

    // Setup kue
    var kue = require('kue');
    var queue = kue.createQueue();
    app.use('/kue', kue.app);

    // Error handler for jobs
    queue.on( 'error', function( err ) {
        console.log( 'Oops... ', err );
    });

    // Register the job handlers
    registerJobHandlers();

    var jobClearAfter = 3600000;
    if (process.env.env !== 'PROD') {
        jobClearAfter = 0;
    }

    var defer = q.defer();
    // Restart active jobs
    queue.active(function( err, ids ) {
        ids.forEach( function( id ) {
            kue.Job.get( id, function( err, job ) {
                if (process.env.env === 'PROD') {
                    job.progress(0, 0, 'Restarting active job due to server restart');
                    job.inactive();
                } else {
                    job.complete();
                }
            });
        });
        defer.resolve();
    });
    defer.promise.then(function() {
        defer = q.defer();
        if (process.env.env !== 'PROD') {
            queue.delayed(function (err, ids) {
                ids.forEach(function (id) {
                    kue.Job.get(id, function (err, job) {
                        job.complete();
                    });
                });
                defer.resolve();
            });
        } else {
            queue.delayed(function (err, ids) {
                ids.forEach(function (id) {
                    kue.Job.get(id, function (err, job) {
                        if (job.type === 'groupMeApprovedUsersHandler' || job.type === 'rebelsConnorRandomizer') {
                            job.complete();
                        }
                    });
                });
                defer.resolve();
            });
            defer.resolve();
        }
        return defer.promise;
    }).then(function() {
        defer = q.defer();
        // Clean up completed
        kue.Job.rangeByState('complete', 0, 9999999, 'asc', function (err, jobs) {
            var d = new Date().getTime();
            jobs.forEach(function (job) {
                if (d - job.updated_at > jobClearAfter) {
                    // 2 completed over 1 hours ago
                    job.remove(function () {
                    });
                }
            });
            defer.resolve();
        });
        return defer.promise;
    }).then(function() {
        defer = q.defer();
        // Clean up inactive
        kue.Job.rangeByState('inactive', 0, 9999999, 'asc', function (err, jobs) {
            var d = new Date().getTime();
            jobs.forEach(function (job) {
                if (d - job.updated_at > jobClearAfter) {
                    // 2 started over 1 hour ago
                    job.remove(function () {
                    });
                }
            });
            defer.resolve();
        });
        return defer.promise;
    }).then(function() {
        // Ensure the generic jobs have been started
        validateGenericJobs();
    });


    // Outside available methods
    global.queueMod.saveHandler = function(err) {
        if (err) {
            console.log(err);
        }
    };
    global.queueMod.createJob = function(jobType, data) {
        var job = queue.create(jobType, data);
        job.done = function(){
            this.save(global.queueMod.saveHandler);
        };
        return job;
    };
    global.queueMod.queue = queue;
    global.queueMod.kue = kue;

    function genericProcessor(job, done) {
        // require the handler
        var handler = require('./handlers/' + job.type);
        var data = job.data;
        var defer = q.defer();
        handler(data, defer);
        defer.promise.then(function(result) {
            // The job was a success
            if (result && result.delay) {
                // This job should run again
                global.queueMod.createJob(job.type, job.data)
                    .delay(result.delay)
                    .done();
            }
            done();
        }, function() {
            // The job failed, recreate the job with a 5 second delay
            global.queueMod.createJob(job.type, job.data)
                .delay(5000)
                .done();
        }, function(data) {
            // We are getting a progress notification
            if (handler.steps) {
                job.progress(data.steps, handler.steps+1, data.data);
            }
        });
    }

    function registerJobHandlers(){
        var fs = require('fs');
        fs.readdir(__dirname + '/handlers', function(err, files){
            files.forEach(function(file){
                // register the generic processor
                queue.process(file.split('.js')[0], genericProcessor);
            });
        });
    }

    function validateGenericJobs(){
        var requiredJobs = [
            'groupmeAutoInvites',
            'parseHealthStatus',
            'redditWidgetUpdates',
            'accessControlUpdate'
            //'giveawayWatcher'
        ];
        if (process.env.env !== 'PROD') {
            requiredJobs = devTasks;
        }
        var foundJobs = [];
        var complete = 0;
        queue.active(handler);
        queue.inactive(handler);
        queue.delayed(handler);

        function handler(err, ids) {
            var completedCheck = 0;
            ids.forEach(function(id) {
                kue.Job.get(id, function(err, job) {
                    if (job && job.type) {
                        if (requiredJobs.indexOf(job.type) > -1) {
                            foundJobs.push(job.type);
                        }
                        completedCheck++;
                        if (completedCheck === ids.length) {
                            complete++;
                            if (complete === 3) {
                                completed();
                            }
                        }
                    }
                });
            });
            if (ids.length === 0) {
                complete++;
                if (complete === 3) {
                    completed();
                }
            }
        }

        function completed(){
            if (foundJobs.length !== requiredJobs) {
                // Some jobs are missing. Start them
                requiredJobs.forEach(function(job){
                    if (foundJobs.indexOf(job) === -1) {
                        global.queueMod.createJob(job, {}).done();
                    }
                });
            }
            deferred.resolve();
            // Prevent stuck jobs
            setInterval(function() {
                var date = new Date();
                queue.active( function( err, ids ) {
                    ids.forEach( function( id ) {
                        kue.Job.get( id, function( err, job ) {
                            var diff = date.getTime() - parseInt(job.started_at);
                            if (diff >= 600000) {
                                job.progress(0, 0, 'Restarting active job due to job stuck');
                                job.complete();
                                global.queueMod.createJob(job.type, {}).done();
                            }
                        });
                    });
                });
            }, 900000);
        }
    }

    return deferred.promise;
};

var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(communityId, trigger, socialConnections) {
    var count = 0;
    var maxPages = 4;
    var after = null;
    var def = q.defer();

    var promiseCheck = function() {
        count++;
        if (count < maxPages) {
            getMessages().then(promiseCheck).catch(function(error) {
                def.resolve();
            });
        } else {
            def.resolve();
        }
    };

    promiseCheck();
    return def.promise;

    function getMessages() {
        var options = {
            limit: 100
        };
        if (after !== null) {
            options.after = after;
        }
        return global.reddit('/message/moderator').get(options).then(function(messages) {
            return messagesHandler(messages);
        });
    }

    function messagesHandler(messages) {
        var doneDefer = q.defer();
        var emailRegex = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;

        messages.data.children.reduce(function (previous, message, index) {
            return previous.then(function (previousValue) {
                var defer = q.defer();

                if (message.data.subreddit == socialConnections.reddit.subreddit) {
                    var checkMemberDB = false;
                    if (message.data.replies) {
                        var foundResponse = false;
                        var foundNoticeOfApproval = false;
                        var foundApproval = false;
                        message.data.replies.data.children.forEach(function (reply) {
                            if (
                                reply.data.author === 'GameComTools' &&
                                reply.data.body.indexOf('Thanks for your email address! We are still processing your application.') === 0
                            ) {
                                checkMemberDB = true;
                            }
                            if (
                                reply.data.author === 'GameComTools' &&
                                reply.data.body.indexOf('Hey There! I have messaged you on GroupMe to get everything started') === 0
                            ) {
                                foundResponse = true;
                            }
                            if (
                                reply.data.author === 'GameComTools' &&
                                reply.data.body.indexOf('Hey Mods! I need a little help here! I was not able to take care of this one, can a human please take care of it? Thanks!') === 0
                            ) {
                                foundResponse = true;
                            }
                            if (
                                reply.data.author === 'GameComTools' &&
                                reply.data.body.indexOf('Thanks for your email address!  We will get you started once a moderator approves.\n\nMODERATORS: Please reply with \'Approved\' if approved.') === 0
                            ) {
                                foundNoticeOfApproval = true;
                            }
                            if (
                                reply.data.author !== message.data.author &&
                                reply.data.body === 'Approved'
                            ) {
                                foundApproval = true;
                            }
                        });
                        if (foundResponse) {
                            checkMemberDB = false;
                        }
                    }
                    after = message.data.name;
                    if (!foundResponse) {
                        if (message.data.subject.trim() === trigger.redditMessageSubject.trim()) {
                            var email = message.data.body.match(emailRegex);
                            var logs = global.getPersistent(communityId + '-groupmeInvites') || {};
                            if (email !== null) {
                                logs[email[0]] = {
                                    received: new Date().toJSON(),
                                    email: email[0],
                                    username: message.data.author,
                                    status: 'Message Received',
                                    subStatus: 'Processing...'
                                };
                                global.savePersistent();
                                var startConvo = false;
                                if (trigger.approval) {
                                    if (foundApproval) {
                                        startConvo = true;
                                    } else if (!foundNoticeOfApproval) {
                                        // Send notice of approval
                                        logs[email[0]] = {
                                            received: new Date().toJSON(),
                                            email: email[0],
                                            username: message.data.author,
                                            status: 'Waiting For Moderator Approval',
                                            subStatus: ''
                                        };
                                        return reddit('/api/comment').post({
                                            text: 'Thanks for your email address!  We will get you started once a moderator approves.\n\nMODERATORS: Please reply with \'Approved\' if approved.',
                                            thing_id: message.data.name
                                        }).then(function () {
                                            if (index === messages.data.children.length - 1) {
                                                return doneDefer.resolve();
                                            }
                                            defer.resolve();
                                        });
                                    }
                                } else {
                                    startConvo = true;
                                }
                                if (startConvo) {
                                    return startConversation(email[0], message.data.author, message.data.name, checkMemberDB).then(function () {
                                        if (index === messages.data.children.length - 1) {
                                            return doneDefer.resolve();
                                        }
                                        defer.resolve();
                                    });
                                }
                            } else {
                                logs[message.data.author] = {
                                    received: new Date().toJSON(),
                                    username: message.data.author,
                                    status: 'Unknown Email Address',
                                    subStatus: 'Email address was unable to be parsed from message'
                                };
                                global.savePersistent();
                                //Send a something seems wrong, so mod mail can manually take care of it
                                return reddit('/api/comment').post({
                                    text: 'Hey Mods! I need a little help here! I was not able to take care of this one, can a human please take care of it? Thanks!',
                                    thing_id: message.data.name
                                }).then(function () {
                                    if (index === messages.data.children.length - 1) {
                                        return doneDefer.resolve();
                                    }
                                    defer.resolve();
                                });
                            }
                        }
                    }
                }
                if (index === messages.data.children.length-1) {
                    return doneDefer.resolve();
                }
                defer.resolve();

                return defer.promise;
            })
        }, q.resolve('start'));

        return doneDefer.promise;
    }

    function startConversation(email, author, thingID, disableResponse) {
        var logs = global.getPersistent(communityId + '-groupmeInvites');
        var defer = q.defer();
        var query = {
            redditUsername: author
        };

        global.parse.find('gctCommunities', communityId, function (err, response) {
            if (parseErrorHandler(err, response)) {
                global.parse.find('gctMembers', query, function (err, member) {
                    if (parseErrorHandler(err, member)) {
                        if (member.results[0]) {
                            global.parse.update('gctMembers', member.results[0].objectId, {groupMeEmail: email}, function () {
                            });
                        }
                        var group = {};
                        logs[email].status = 'Starting Group';
                        logs[email].subStatus = '';
                        global.savePersistent();
                        global.groupme('groups', 'POST', {
                            name: response.name + ' Bot - ' + author,
                            share: false,
                            description: 'Welcome to the ' + response.name
                        }).then(function (grp) {
                            logs[email].status = 'Adding Member To Group';
                            global.savePersistent();
                            // Group has been created
                            group = grp;
                            return global.groupme('groups/' + group.response.id + '/members/add', 'POST', {
                                members: [
                                    {
                                        nickname: author,
                                        email: email,
                                        guid: guid()
                                    }
                                ]
                            })
                        }).then(function (resp) {
                            logs[email].status = 'Creating Callback Bot & Sending Reddit Message';
                            global.savePersistent();
                            global.groupme('bots', 'POST', {
                                bot: {
                                    name: 'gmApplication - ' + author,
                                    group_id: group.response.id,
                                    callback_url: 'http://gamecomtools.com/api/groupme/callback?type=gmApplication&communityID=' + communityId
                                }
                            });
                            global.groupme('groups/' + group.response.id + '/messages', 'POST', {
                                "message": {
                                    "source_guid": guid(),
                                    "text": "Hey there! I am a bot for " + response.name + '. I am going to help you get into all the correct groups. To begin, just reply with \'hello\' and we will get started!'
                                }
                            });
                            reddit('/api/comment').post({
                                text: 'Hey There! I have messaged you on GroupMe to get everything started, please see the group ' + response.name + ' Bot - ' + author + '!',
                                thing_id: thingID
                            }).then(function () {
                                logs[email].status = 'Waiting On Member';
                                logs[email].subStatus = 'Step 1';
                                global.savePersistent();
                                defer.resolve();
                            });
                        });
                        //} else {
                        //    if (!disableResponse) {
                        //        logs[email].status = 'Waiting On Community Approval';
                        //        logs[email].subStatus = '';
                        //        global.savePersistent();
                        //        return reddit('/api/comment').post({
                        //            text: 'Thanks for your email address! We are still processing your application. Once approved, I will begin your GroupMe onboarding!',
                        //            thing_id: thingID
                        //        }).then(function () {
                        //            defer.resolve();
                        //        });
                        //    }
                        //    defer.resolve();
                        //}
                    } else {
                        defer.resolve();
                    }
                });
            } else {
                defer.resolve();
            }
        });

        return defer.promise;
    }

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
}

var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var requestErrorHandler = require(global.utilsPath + '/requestErrorHandler');

module.exports = function(data){
    var defer = q.defer();

    request('https://www.reddit.com/r/' + data.subreddit + '/about/stylesheet', function (err, response, body) {
        if (requestErrorHandler(err, response, body)) {
            var tmpFileName = '/tmp/' + data.widget + '-' + data.communityId + '.png';
            var query = '';
            Object.keys(data.options).forEach(function(key) {
                query += key + '=' + data.options[key] + '&';
            });
            request('http://' + global.webHost + '/api/widget/?communityID=' + data.communityId + '&widget=' + data.widget + '&' + query)
                .pipe(fs.createWriteStream(tmpFileName))
                .on('finish', function(){
                    var imageFile = fs.readFileSync(tmpFileName);
                    reddit.auth().then(function () {
                        return reddit('/r/' + data.subreddit + '/api/upload_sr_img').post({
                            file: Snoocore.file(tmpFileName, 'image/png', imageFile),
                            header: 0,
                            name: data.widget
                        });
                    }, function(){defer.resolve();}).then(function () {
                        var css = cheerio.load(body);
                        css = css(".language-css").text();
                        if (css.indexOf("a[href='#" + data.widgetCSS + "']") > -1) {
                            // Exists. Get rid of it for an update
                            var regex = new RegExp("a\\[href='#" + data.widgetCSS + "'](.*\\n*)+}", "m");
                            css = css.replace(regex, '');
                        }
                        var sizeOf = require('image-size');
                        var dimensions = sizeOf(tmpFileName);

                        var widgetCSS = 'a[href=\'#' + data.widgetCSS + '\']{\n';
                        widgetCSS += 'background-image: url(\%\%' + data.widget + '\%\%);\n';
                        widgetCSS += 'width: 100%;\n';
                        widgetCSS += 'height: ' + dimensions.height + 'px;\n';
                        widgetCSS += 'display: block;\n';
                        widgetCSS += 'cursor: default;\n';
                        widgetCSS += 'position: inherit;\n';
                        widgetCSS += 'border: none;\n';
                        widgetCSS += 'box-shadow: none;\n';
                        widgetCSS += 'border-radius: 0;\n';
                        widgetCSS += 'padding: 0;\n';
                        widgetCSS += '}';

                        css += widgetCSS.replace(/\n/g, '');

                        reddit('/r/' + data.subreddit + '/api/subreddit_stylesheet').post({
                            stylesheet_contents: encodeURIComponent(css),
                            op: 'save',
                            reason: 'Updating ' + data.widget
                        }).then(function () {
                            defer.resolve();
                        }, function(){defer.resolve();});
                    }, function(){defer.resolve();});
                });
        } else {
            defer.resolve();
        }
    });

    return defer.promise;
};

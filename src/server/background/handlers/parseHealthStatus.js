var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var outageNotification = require(global.utilsPath + '/outageNotification');
var lastStatus = 'online';

module.exports = function(data, defer) {
    global.parse.findMany('gctCommunities', {}, function (err, communities) {
        if (parseErrorHandler(err, communities)) {
            if (lastStatus === 'offline') {
                outageNotification('online', 'parse');
            }
            lastStatus = 'online';
        } else {
            if (lastStatus === 'online') {
                lastStatus = 'possiblyOffline';
            } else if (lastStatus === 'possiblyOffline') {
                lastStatus = 'offline';
                outageNotification('offline', 'parse');
            }
        }
        defer.resolve({
            delay: 30000
        });
    });
};

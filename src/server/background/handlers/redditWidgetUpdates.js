var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var requestErrorHandler = require(global.utilsPath + '/requestErrorHandler');

module.exports = function(data, logger) {
    global.parse.findMany('gctCommunities', {}, function (err, communities) {
        if (parseErrorHandler(err, communities)) {
            var promises = [];

            communities.results.forEach(function(community) {
                var deferred = q.defer();

                promises.push(deferred.promise);

                if (community.socialConnections && community.socialConnections.reddit && community.socialConnections.reddit.subreddit) {
                    var subreddit = community.socialConnections.reddit.subreddit;
                    var promise = reddit('/r/' + subreddit + '/about').get().then(function(aboutData) {
                        var widgets = ['gctEventsWidget'];
                        var widgetPromises = [];
                        widgets.forEach(function(widget) {
                            if (aboutData.data.description_html.indexOf('#' + widget) > -1) {
                                // Community is using this widget
                                // Get meta from stylesheet
                                var widgetCSS = widget;
                                var parts = aboutData.data.description_html.split('#' + widget + ';');
                                parts = parts[1].split('"&gt;');
                                widgetCSS += ';' + parts[0];
                                parts = parts[0].split(';');
                                var options = {};
                                parts.forEach(function(part) {
                                    part = part.split('=');
                                    if (part[1] === 'true') {
                                        part[1] = true;
                                    } else if (part[1] === 'false') {
                                        part[1] = false;
                                    }
                                    options[part[0]] = part[1];
                                });
                                var data = {
                                    widget: widget,
                                    communityId: community.objectId,
                                    subreddit: subreddit,
                                    options: options,
                                    widgetCSS: widgetCSS
                                };
                                widgetPromises.push(require('../helpers/widgetUpdater')(data));
                            }
                        });
                        q.all(widgetPromises).then(function() {
                            deferred.resolve();
                        });
                    }, function() {
                        deferred.resolve();
                    });
                    promises.push(promise);
                } else {
                    deferred.resolve();
                }
            });

            q.all(promises).then(function() {
                defer.resolve({
                    delay: 900000
                });
            });
        } else {
            defer.reject();
        }
    });
};

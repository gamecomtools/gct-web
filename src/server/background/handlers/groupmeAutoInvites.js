var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var q = require('q');

module.exports = function(data, defer) {
    global.parse.findMany('gctCommunities', {}, function (err, communities) {
        if (parseErrorHandler(err, communities)) {
            var promises = [];

            communities.results.forEach(function(community) {
                var deferred = q.defer();

                if (community.socialConnections && community.socialConnections.groupme && community.socialConnections.groupme.autoInvites && community.socialConnections.groupme.autoInvites.type !== 'disabled' && community.socialConnections.groupme.autoInvites.type !== undefined) {
                    require('../helpers/autoInviteTypes/' + community.socialConnections.groupme.autoInvites.type)(community.id, community.socialConnections.groupme.autoInvites, community.socialConnections).then(function() {
                        deferred.resolve();
                    });
                } else {
                    deferred.resolve();
                }

                promises.push(deferred.promise);
            });

            q.all(promises).then(function() {
                defer.resolve({
                    delay: 60000
                });
            });
        } else {
            defer.reject();
        }
    });
};

var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var request = require('request');
var requestErrorHandler = require(global.utilsPath + '/requestErrorHandler');
var q = require('q');

module.exports = function(data, defer) {
    global.parse.findMany('gctCommunities', {}, function (err, communities) {
        if (parseErrorHandler(err, communities)) {
            var promises = [];

            communities.results.forEach(function(community) {
                var deferred = q.defer();

                if (community.socialConnections && community.socialConnections.reddit && community.socialConnections.reddit.subreddit) {
                    var subreddit = community.socialConnections.reddit.subreddit;
                    var query = 'flair%3Agiveaway';
                    request('https://www.reddit.com/r/' + subreddit + '/search.json?q=' + query + '&restrict_sr=on&sort=new', function (err, response, body) {
                        if (requestErrorHandler(err, response, body)) {
                            body = JSON.parse(body);
                            body.data.children.forEach(function(post) {
                                post = post.data;
                                var answer = global.getPersistent('giveawayAnswer-' + post.id);
                                if (answer.number === undefined) {
                                    answer.number = Math.floor(Math.random() * 999) + 1;
                                    answer.members = [];
                                    answer.repliedTo = [];
                                    answer.winners = [];
                                    answer.open = true;
                                    global.savePersistent();
                                }
                                reddit('/r/' + subreddit + '/comments/' + post.id + '?sort=new').get().then(function(comments) {
                                    var answerData = global.getPersistent('giveawayAnswer-' + post.id);
                                    comments = comments[1].data.children;
                                    comments.forEach(function(comment) {
                                        if (answerData.members.indexOf(comment.data.author) === -1 && answer.repliedTo.indexOf(comment.data.id) === -1) {
                                            if (answer.open) {
                                                if (!isNaN(comment.data.body)) {
                                                    if (parseInt(comment.data.body) === answerData.number) {
                                                        answer.winners.push(comment.data.author);
                                                        if (answer.winners.length === 1) {
                                                            answer.number = Math.floor(Math.random() * 999) + 1;
                                                        } else {
                                                            answer.open = false;
                                                        }
                                                        sendReply(comment.data.name, 'WINNER - WINNER - WINNER ~ You got it! We will contact you shortly! ~ WINNER - WINNER - WINNER');
                                                    } else {
                                                        sendReply(comment.data.name, 'Sorry, this is not the correct answer.');
                                                    }
                                                } else {
                                                    if (comment.data.body !== '[deleted]') {
                                                        sendReply(comment.data.name, 'You have comment with more than just a number. You have now lost your chance.');
                                                    }
                                                }
                                                if (comment.data.author !== '[deleted]') {
                                                    answer.members.push(comment.data.author);
                                                }
                                                answer.repliedTo.push(comment.data.id);
                                            } else {
                                                sendReply(comment.data.name, 'Sorry, the maximum people have already won!');
                                            }
                                        } else {
                                            if (answer.repliedTo.indexOf(comment.data.id) === -1) {
                                                sendReply(comment.data.name, 'You only get one entry.');
                                                answer.repliedTo.push(comment.data.id);
                                            }
                                        }
                                    });
                                    global.savePersistent();
                                });
                            });
                            deferred.resolve();
                        } else {
                            deferred.resolve();
                        }
                    });
                } else {
                    deferred.resolve();
                }
                promises.push(deferred.promise);
            });

            function sendReply(id, text) {
                //reddit.auth().then(function () {
                //    return reddit('/api/comment').post({
                //        text: text,
                //        thing_id: id
                //    });
                //});
            }

            q.all(promises).then(function() {
                defer.resolve({
                    delay: 60000
                });
            });
        } else {
            defer.reject();
        }
    });
};

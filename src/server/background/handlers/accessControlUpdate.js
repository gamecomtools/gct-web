var Promise = require('bluebird');
var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');

module.exports = function(data, defer) {
    global.parse.findMany('gctCommunities', {}, function (err, communities) {
        if (parseErrorHandler(err, communities)) {
            var count = 0;
            communities.results.forEach(function(community) {
                var accessControl = global.getPersistent('accessControl-' + community.objectId);
                var getMembersOf = [];
                if (community.socialNetworks.indexOf('groupme') > -1) {
                    if (community.socialConnections.groupme && community.socialConnections.groupme.botOptions) {
                        for (var i in community.socialConnections.groupme.botOptions) {
                            if (community.socialConnections.groupme.botOptions[i].accessControl) {
                                accessControl[i] = community.socialConnections.groupme.botOptions[i].accessControl;
                                for (var j in accessControl[i]) {
                                    if (accessControl[i][j].indexOf('groupme:') > -1) {
                                        var id = accessControl[i][j].replace('groupme:', '');
                                        if (getMembersOf.indexOf(id) === -1) {
                                            getMembersOf.push(id);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                Promise.each(getMembersOf, function(groupId){
                    return global.groupme('groups/' + groupId, 'GET', {}).then(function (response) {
                        for (var i in accessControl) {
                            for (var j in accessControl[i]) {
                                if (accessControl[i][j] === 'groupme:' + groupId) {
                                    accessControl[i][j] = [];
                                    if (response.response) {
                                        response.response.members.forEach(function (member) {
                                            accessControl[i][j].push(member.user_id);
                                        });
                                    }
                                }
                            }
                        }
                    });
                }).then(function() {
                    count++;
                    if (count === communities.results.length) {
                        global.savePersistent();
                        defer.resolve({
                            delay: 900000
                        });
                    }
                });
            });
        } else {
            defer.reject();
        }
    });
};

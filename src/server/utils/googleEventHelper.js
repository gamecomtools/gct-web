module.exports = function(cal) {
    cal.items.forEach(function(event){
        event.timeZone = cal.timeZone;
        delete event.kind;
        delete event.etag;
        delete event.creator;
        delete event.organizer;
        event.start = event.start.dateTime;
        event.end = event.end.dateTime;
        delete event.iCalUID;
        delete event.sequence;

        var titleParts = event.summary.match(/\[(.*)\] (.*) - (.*)/);
        if (titleParts !== null) {
            var game = titleParts[2].split(' (');
            event.data = {
                platform: titleParts[1],
                game: game[0],
                info: (game[1] || '').replace(')', ''),
                host: titleParts[3]
            };
        }
    });
    return cal.items;
};

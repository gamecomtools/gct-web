module.exports = {};

module.exports.nonGames = ['Home', 'Hulu Plus', 'Media Player', 'My games & apps', 'Friends', 'Netflix', 'TV', 'Twitch', 'Xbox Video', 'YouTube', 'Store', 'HBO GO',
    'Network', 'Pandora', 'Upload', 'Achievements', 'Blu-ray Player', 'Comedy Central', 'Sling Television', 'Amazon Instant Video', 'Skype',
    'Internet Explorer', 'Vine', 'Media Player Preview', 'BBC iPlayer', 'iHeartRadio', 'VUDU Movies & TV', 'WWE Network', 'Showtime Anytime',
    'Marketplace', 'Upload Studio', 'OneDrive', 'MLBTV', 'MTV', 'Plex', 'Uplay', 'TrueAchievements', '???', 'At&T U-verse', 'AT&T U-verse', 'Help', 'Xbox Fitness',
    'Xbox Help', 'Xbox Music', 'Crackle', 'ESPN', 'Encore Play', 'FXNOW', 'Halo Channel', 'STARZ Play', 'XLEi', 'Network Troubleshooter', 'Avatar Editor', 'EA Access Hub',
    'IGN for Xbox', 'Syfy Now', 'Xbox Live Events', 'ABC News'];

module.exports.sameNames = {
    'Plants vs Zombies: Garden Warfare': 'Plants vs Zombies Garden Warfare',
    'Middle-earth: Shadow of Mordor™ - Game of the Year Edition': 'Middle-earth: Shadow of Mordor™',
    'RESIDENT EVIL REVELATIONS 2 (Episode One)': 'Resident Evil Revelations 2 (Episode One)',
    'FIFA 15 Downloadable Demo': 'FIFA 15',
    'Xbox Dash PB': 'Gears of War: Ultimate Edition Beta',
    'NHL 15 Full Game': 'NHL 15'
};

module.exports.gameAcronyms = {
    'Grand Theft Auto V': [
        'gta',
        'gta v',
        'gtav'
    ],
    'The Elder Scrolls Online: Tamriel Unlimited': [
        'eso'
    ],
    'Call of Duty: Advanced Warfare': [
        'cod aw',
        'cod'
    ],
    'Battlefield 4': [
        'bf4'
    ],
    'Battlefield Hardline': [
        'bfh'
    ],
    'Elite: Dangerous Game Preview': [
        'e:d'
    ]
};

module.exports.groupMeGameMap = {
    minecraft: 'Minecraft on XB1',
    minecraft_pc: 'Minecraft on PC',
    pc: 'PC Games (Steam)',
    grandTheftAuto: 'GTA V on XB1',
    borderlands: 'Borderlands on XB1',
    battlefield: 'the Battlefield Franchise on XB1',
    madden: 'the Madden Franchise on XB1',
    fifa: 'the FIFA Franchise on XB1',
    halo: 'the Halo Franchise on XB1',
    destiny: 'Destiny on XB1',
    mortalKombad: 'Mortal Kombat X on XB1',
    eso: 'Elder Scrolls Online on XB1',
    gow: 'Gears of War on XB1',
    clashOfClans: 'Clash of Clans',
    rust: 'Rust'
};

module.exports.groupMeGameTypes = {
    fps: 'First Person Shooters (fps)',
    sports: 'Sports'
};

module.exports.gameTypeMaps = {
    fps: [
        'battlefield', 'halo', 'borderlands', 'destiny'
    ],
    sports: [
        'madden', 'fifa'
    ]
};

module.exports.groupMeEventMap = {
    '[XB1] GTA': 'game_grandTheftAuto',
    '[XB1] Minecraft': 'game_minecraft',
    '[XB1] Halo: MCC': 'game_halo',
    '[XB1] Battlefield 4': 'game_battlefield',
    '[PC] GTA': 'game_pc',
    '[PC] Euro Truck 2': 'game_pc'
};

module.exports.bannedGroupMeKeywords = ['boobs', 'tits', 'boob', 'tit', 'boobies', 'porn'];

module.exports.streamers = {
    // nickname: groupme member ID
    'kpkody': 24997941
};

module.exports.streamCount = 0;

module.exports.currentlyStreaming = {};

module.exports.streamStartPromises = {};

module.exports.approvedGroupMeUsers = {};

module.exports.connorisms = ['bitch', 'heifer', 'Nah, bud', 'Your mother!', 'BK Randy', 'Ya, I\'ll bet you do!'];

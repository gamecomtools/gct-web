module.exports = function(err, response) {
    if (err !== null) {
        console.log('PARSE ERR: ' + err);
        console.log('PARSE RESPONSE: ' + response);
        return false;
    }
    return true;
};

var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var Q = require('q');
var merge = require('deepmerge');

module.exports = function(communityID, updatedData, fullUpdate) {
    var deferred = Q.defer();

    global.parse.find('gctCommunities', communityID, function (err, response) {
        if (parseErrorHandler(err, response)) {
            if (response === undefined) {
                return deferred.reject('Community Not Found');
            }
            var community = response;
            delete community.objectId;
            delete community.createdAt;
            delete community.updatedAt;

            delete updatedData.objectId;
            delete updatedData.createdAt;
            delete updatedData.updatedAt;

            if (!fullUpdate) {
                community = merge(community, updatedData);
            } else {
                community = updatedData;
            }
            global.parse.update('gctCommunities', communityID, community, function (err, response) {
                if (parseErrorHandler(err, response)) {
                    deferred.resolve(community);
                } else {
                    deferred.reject('Parse.com Error');
                }
            });
        }else {
            deferred.reject('Parse.com Error');
        }
    });

    return deferred.promise;
};

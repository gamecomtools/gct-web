var common = require(global.utilsPath + '/common');

module.helpText = "Used for clash of clans community members to call enemies when war has begun.";

module.cooldownTime = 0;

module.gameRequirements = ['clashOfClans'];

module.exports = function(data, query) {
    var callout = data.text.replace('!callout', '').trim();
    var now = new Date();
    var currentWar = global.getPersistent('war-' + data.group_id);

    if (currentWar.warData !== undefined && currentWar.warData.expires <= now.getTime()) {
        currentWar.warData = undefined;
        global.savePersistent();
    }

    if (currentWar.warData === undefined) {
        global.groupme('groups/' + data.group_id + '/messages', 'POST', {
            message: {
                source_guid: guid(),
                text: 'A war has not yet begun!'
            }
        });
    } else {
        if (currentWar.warData.enemyCount === 0) {
            global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: 'A war is still being configured!'
                }
            });
        } else {
            if (currentWar.warData.calloutExpires <= now.getTime()) {
                return global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: 'Callouts are closed! Use \'!war\' to see the list of calls'
                    }
                });
            }

            if (callout === '') {
                return global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: 'An enemy number is required. ex \'!callout 3\''
                    }
                });
            }
            callout = parseInt(callout);

            if (currentWar.warData.callouts.indexOf(data.name) === -1) {
                if (callout > currentWar.warData.enemyCount) {
                    global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                        message: {
                            source_guid: guid(),
                            text: data.name + ', that enemy number is too high. Only ' + currentWar.warData.enemyCount + ' enemies in this war.'
                        }
                    });
                } else if(currentWar.warData.callouts[callout - 1] !== undefined && currentWar.warData.callouts[callout - 1] !== null) {
                    global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                        message: {
                            source_guid: guid(),
                            text: data.name + ', that enemy has already been called.'
                        }
                    });
                } else {
                    currentWar.warData.callouts[callout - 1] = data.name;
                    global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                        message: {
                            source_guid: guid(),
                            text: data.name + ' has called enemy ' + callout + '!'
                        }
                    });
                    global.savePersistent();
                }
            } else {
                global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: data.name + ', you have already called enemy ' + (parseInt(currentWar.warData.callouts.indexOf(data.name))+1) + '.'
                    }
                });
            }
        }
    }

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
};

var common = require(global.utilsPath + '/common');

module.helpText = "Used for clash of clans communities to list the current war callouts.";

module.cooldownTime = 1;

module.gameRequirements = ['clashOfClans'];

module.exports = function(data, query) {
    var now = new Date();
    var currentWar = global.getPersistent('war-' + data.group_id);

    if (currentWar.warData !== undefined && currentWar.warData.expires <= now.getTime()) {
        currentWar.warData = undefined;
        global.savePersistent();
    }

    if (currentWar.warData === undefined) {
        global.groupme('groups/' + data.group_id + '/messages', 'POST', {
            message: {
                source_guid: guid(),
                text: 'A war is not currently in progress.'
            }
        });
        return global.clearCommandCooldown('warList', data.group_id);
    } else {
        if (currentWar.warData.enemyCount === 0 || currentWar.warData.enemyCount === null) {
            global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: 'A war is currenly being configured.'
                }
            });
            return global.clearCommandCooldown('war', data.group_id);
        } else {
            var message = 'A war is open against ' + currentWar.warData.opponent + '!\n\nThere are ' + currentWar.warData.enemyCount + ' enemies, use the command \'!callout ENEMY_NUMBER\' to call an enemy, use \'!war\' to see current calls!\n\n';
            var i = 0;
            while(i !== currentWar.warData.enemyCount) {
                message += 'Enemy ' + (i+1) + ': ';
                if (currentWar.warData.callouts[i] === undefined || currentWar.warData.callouts[i] === null) {
                    message += '';
                } else {
                    message += currentWar.warData.callouts[i];
                }
                message += '\n';
                i++;
            }
            message += '\n';
            now = new Date();
            if (currentWar.warData.calloutExpires <= now.getTime()) {
                message += 'You can no longer call an enemy. '
            } else {
                var msec = (currentWar.warData.calloutExpires - now.getTime());

                var hours = Math.floor(msec / 1000 / 60 / 60);
                msec -= hours * 1000 * 60 * 60;
                var minutes = Math.floor(msec / 1000 / 60);
                msec -= minutes * 1000 * 60;
                var seconds = Math.floor(msec / 1000);
                msec -= seconds * 1000;

                if (hours > 0) {
                    message += hours + ' hour' + (hours > 1 ? 's' : '') + ' left to call an enemy. ';
                } else if (minutes > 0) {
                    message += minutes + ' minute' + (minutes > 1 ? 's' : '') + ' left to call an enemy. ';
                } else if (seconds > 0) {
                    message += seconds + ' second' + (seconds > 1 ? 's' : '') + ' left to call an enemy. ';
                } else {
                    message += 'You can no longer call an enemy. '
                }
            }

            now = new Date();
            msec = (currentWar.warData.expires - now.getTime());

            hours = Math.floor(msec / 1000 / 60 / 60);
            msec -= hours * 1000 * 60 * 60;
            minutes = Math.floor(msec / 1000 / 60);
            msec -= minutes * 1000 * 60;
            seconds = Math.floor(msec / 1000);
            msec -= seconds * 1000;

            if (hours > 0) {
                message += hours + ' hour' + (hours > 1 ? 's' : '') + ' left in war.';
            } else if (minutes > 0) {
                message += minutes + ' minute' + (minutes > 1 ? 's' : '') + ' left in war.';
            } else {
                message += seconds + ' second' + (seconds > 1 ? 's' : '') + ' left in war.';
            }

            global.groupme('groups/' + data.group_id + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: message
                }
            });
        }
    }

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
};

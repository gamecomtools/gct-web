var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var common = require(global.utilsPath + '/common');

module.exports = function(guid, body, communityID, typeStepData) {
    var logs = global.persist.getItem(communityID + '-groupmeInvites') || {};

    if (!typeStepData[body.group_id].sentIntro) {
        global.groupme('groups/' + body.group_id + '/messages', 'POST', {
            message: {
                source_guid: guid(),
                text: "Alright, let's get everything in order! I am just going to ask a series of yes/no questions to get you into the correct groups."
            }
        });
    }

    if (!typeStepData[body.group_id].groups) {
        typeStepData[body.group_id].groups = {
            main: null,
            games: [],
            gameIds: [],
            addToGroup: []
        };
        typeStepData[body.group_id].currentGroup = -1;
        global.parse.find('gctCommunities', communityID, function (err, response) {
            if (parseErrorHandler(null, err, response)) {
                for (var i in response.socialConnections.groupme.groups) {
                    if (!response.socialConnections.groupme.botOptions[i] || !response.socialConnections.groupme.botOptions[i].excludeAutoInvite) {
                        if (response.socialConnections.groupme.groups[i] === 'main') {
                            typeStepData[body.group_id].groups.main = i;
                        } else if (response.socialConnections.groupme.groups[i].indexOf('game_') === 0) {
                            var game = response.socialConnections.groupme.groups[i].replace('game_', '');
                            typeStepData[body.group_id].groups.games.push(common.groupMeGameMap[game] || game);
                            typeStepData[body.group_id].groups.gameIds.push(i);
                        } else if (response.socialConnections.groupme.groups[i] !== 'leadership' && response.socialConnections.groupme.groups[i] !== 'random') {
                            var game = response.socialConnections.groupme.groups[i];
                            if (common.gameTypeMaps[game]) {
                                var games = [];
                                common.gameTypeMaps[game].forEach(function (gm) {
                                    games.push(common.groupMeGameMap[gm] || gm);
                                });
                                typeStepData[body.group_id].groups.games.push(games.join(' or '));
                                typeStepData[body.group_id].groups.gameIds.push(i);
                            } else {
                                typeStepData[body.group_id].groups.games.push(common.groupMeGameMap[game] || game);
                                typeStepData[body.group_id].groups.gameIds.push(i);
                            }
                        }
                    }
                }
                askGame();
            }
        });
    } else {
        askGame();
    }

    function askGame() {
        if (typeStepData[body.group_id].groups.games.length !== 0) {
            var waitTime = 0;
            if (!typeStepData[body.group_id].sentIntro) {
                waitTime = global.groupmeWaitTime;
            }
            typeStepData[body.group_id].sentIntro = true;
            setTimeout(function () {
                var logs = global.getPersistent(communityID + '-groupmeInvites') || {};
                var logsEmails = Object.keys(logs);
                var email = '';
                logsEmails.forEach(function (em) {
                    if (logs[em].username === body.name) {
                        email = em;
                    }
                });


                if (typeStepData[body.group_id].currentGroup !== undefined) {
                    if (typeStepData[body.group_id].currentGroup !== -1) {
                        var response = body.text.toLowerCase().replace(/ /, '');
                        if (response === 'yes') {
                            typeStepData[body.group_id].groups.addToGroup[typeStepData[body.group_id].currentGroup] = true;
                        } else if (response === 'no') {
                            typeStepData[body.group_id].groups.addToGroup[typeStepData[body.group_id].currentGroup] = false;
                        } else {
                            return global.groupme('groups/' + body.group_id + '/messages', 'POST', {
                                message: {
                                    source_guid: guid(),
                                    text: "I didn't understand that. Please use just 'yes' or 'no'"
                                }
                            });
                        }
                    }
                    typeStepData[body.group_id].currentGroup++;
                }
                if (typeStepData[body.group_id].currentGroup === typeStepData[body.group_id].groups.games.length) {
                    if (email !== '') {
                        logs[email].status = 'Adding Member To Groups';
                        logs[email].subStatus = '';
                        global.savePersistent();
                    }

                    global.groupme('groups/' + body.group_id + '/messages', 'POST', {
                        message: {
                            source_guid: guid(),
                            text: "Awesome! Over the next few minutes I will get you added to all of the groups based on your responses. If at anytime you would like to be added to any other groups feel free to contact 'himynameisneck' in the main chat! Welcome to the community and have fun! You can now leave this chat group!"
                        }
                    });

                    global.groupme('bots', 'GET').then(function (bots) {
                        var botName = '';
                        bots.response.forEach(function (bot) {
                            if (bot.group_id == body.group_id) {
                                botName = bot.name;
                                global.groupme('bots/destroy', 'POST', {
                                    bot_id: bot.bot_id
                                }).then(function (bots) {
                                    global.groupme('groups/' + body.group_id + '/members/26724659/remove', 'POST', {
                                        membership_id: '26724659'
                                    });
                                });
                            }
                        })
                    });

                    if (typeStepData[body.group_id].groups.main !== null) {
                        addToGroup(typeStepData[body.group_id].groups.main, body.sender_id, body.name);
                    }
                    typeStepData[body.group_id].groups.addToGroup.forEach(function (value, index) {
                        if (value) {
                            addToGroup(typeStepData[body.group_id].groups.gameIds[index], body.sender_id, body.name);
                        }
                    });
                    delete typeStepData[body.group_id];

                    if (email !== '') {
                        logs[email].status = 'Complete';
                        logs[email].subStatus = '';
                        global.savePersistent();
                    }
                } else {
                    if (email !== '') {
                        logs[email].status = 'Waiting On Member';
                        logs[email].subStatus = 'Step ' + typeStepData[body.group_id].currentGroup;
                        global.savePersistent();
                    }

                    var game = typeStepData[body.group_id].groups.games[typeStepData[body.group_id].currentGroup];
                    global.groupme('groups/' + body.group_id + '/messages', 'POST', {
                        message: {
                            source_guid: guid(),
                            text: 'Do you play ' + game + '?'
                        }
                    });
                }
                global.persist.setItemSync('typeStepData', typeStepData);
            }, waitTime);
        } else {
            if (email !== '') {
                logs[email].status = 'Adding Member To Groups';
                logs[email].subStatus = '';
                global.savePersistent();
            }

            global.groupme('groups/' + body.group_id + '/messages', 'POST', {
                message: {
                    source_guid: guid(),
                    text: "Awesome! Over the next few minutes I will get you added to all of the groups based on your responses. If at anytime you would like to be added to any other groups feel free to contact 'himynameisneck' in the main chat! Welcome to the community and have fun! You can now leave this chat group!"
                }
            });

            global.groupme('bots', 'GET').then(function (bots) {
                var botName = '';
                bots.response.forEach(function (bot) {
                    if (bot.group_id == body.group_id) {
                        botName = bot.name;
                        global.groupme('bots/destroy', 'POST', {
                            bot_id: bot.bot_id
                        }).then(function (bots) {
                            global.groupme('groups/' + body.group_id + '/members/26724659/remove', 'POST', {
                                membership_id: '26724659'
                            });
                        });
                    }
                })
            });

            if (typeStepData[body.group_id].groups.main !== null) {
                addToGroup(typeStepData[body.group_id].groups.main, body.sender_id, body.name);
            }
            delete typeStepData[body.group_id];

            if (email !== '') {
                logs[email].status = 'Complete';
                logs[email].subStatus = '';
                global.savePersistent();
            }
        }
    }

    function addToGroup(groupID, userID, nickname) {
        global.groupme('groups/' + groupID + '/members/add', 'POST', {
            members: [
                {
                    nickname: nickname,
                    user_id: userID,
                    guid: guid()
                }
            ]
        }).then(function(resp) {})
    }
};

var parseErrorHandler = require(global.utilsPath + '/parseErrorHandler');
var common = require(global.utilsPath + '/common');

module.exports = function(guid, body, communityID, typeStepData) {
    if (!typeStepData[body.group_id].sentIntro) {
        global.groupme('groups/' + body.group_id + '/messages', 'POST', {
            message: {
                source_guid: guid(),
                text: "Alright, let's get everything in order! I am just going to ask a series of yes/no questions to get you into the correct groups."
            }
        });
    }

    if (!typeStepData[body.group_id].groups) {
        typeStepData[body.group_id].groups = {
            main: null,
            games: [],
            gameIds: [],
            addToGroup: []
        };
        typeStepData[body.group_id].currentGroup = -1;
        global.parse.find('gctCommunities', communityID, function (err, response) {
            if (parseErrorHandler(null, err, response)) {
                for (var i in response.socialConnections.groupme.groups) {
                    if (response.socialConnections.groupme.groups[i] === 'main') {
                        typeStepData[body.group_id].groups.main = i;
                    } else if (response.socialConnections.groupme.groups[i].indexOf('game_') === 0) {
                        var game = response.socialConnections.groupme.groups[i].replace('game_', '');
                        typeStepData[body.group_id].groups.games.push(common.groupMeGameMap[game] || game);
                        typeStepData[body.group_id].groups.gameIds.push(i);
                    }
                }
                askGame();
            }
        });
    } else {
        askGame();
    }

    function askGame() {
        var waitTime = 0;
        if (!typeStepData[body.group_id].sentIntro) {
            waitTime = global.groupmeWaitTime;
        }
        typeStepData[body.group_id].sentIntro = true;
        setTimeout(function () {
            if (typeStepData[body.group_id].currentGroup !== undefined) {
                if (typeStepData[body.group_id].currentGroup !== -1) {
                    var response = body.text.toLowerCase().replace(/ /, '');
                    if (response === 'yes') {
                        typeStepData[body.group_id].groups.addToGroup[typeStepData[body.group_id].currentGroup] = true;
                    } else if(response === 'no') {
                        typeStepData[body.group_id].groups.addToGroup[typeStepData[body.group_id].currentGroup] = false;
                    } else {
                        return global.groupme('groups/' + body.group_id + '/messages', 'POST', {
                            message: {
                                source_guid: guid(),
                                text: "I didn't understand that. Please use just 'yes' or 'no'"
                            }
                        });
                    }
                }
                typeStepData[body.group_id].currentGroup++;
            }
            if (typeStepData[body.group_id].currentGroup === typeStepData[body.group_id].groups.games.length) {
                var message = "As a reminder, we ask that you refresh yourself with our GroupMe rules:\n\n";
                message += '* Keep conversation relevant in each game specific chat. For anything non-session related please use “REBL: General Chat”\n';
                message += '* Absolutely no nudity/NSFL content should be posted in any of the chats.\n';
                message += '* Do not treat each chat as an in game chat/mic replacement.\n';
                message += '* If you do not have access to a mic and need to get in contact with someone, feel free to use “REBL: General Chat” or send the user a private message.\n';
                message += '* DO NOT change the name of any group that you are in.\n';
                message += '* DO NOT add or remove anyone from any group. Crew leaders are the only people authorized to add or remove anyone from a group.\n';
                message += '* Please set your GroupMe display name to be the same as your Gamertag and/or Steam ID\n';
                message += '* Treat each other with respect. We don’t want this to be an avenue for someone to deliver personal attacks.\n';
                message += '* Have a thick skin, we’re here to play games. People can be trolls, but our reaction is what fuels their fire!\n\n';

                global.groupme('groups/' + body.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: message
                    }
                });
                setTimeout(function() {
                    global.groupme('groups/' + body.group_id + '/messages', 'POST', {
                        message: {
                            source_guid: guid(),
                            text: "Over the next few minutes I will get you added to all of the groups based on your responses. If at anytime you would like to be added to any other groups feel free to contact 'himynameisneck' in the main chat! You can also always ping me here if you need anything. You can see my commands via !help"
                        }
                    });
                }, global.groupmeWaitTime);

                global.groupme('bots', 'GET').then(function (bots) {
                    var botName = '';
                    bots.response.forEach(function (bot) {
                        if (bot.group_id == body.group_id) {
                            botName = bot.name;
                            global.groupme('bots/destroy', 'POST', {
                                bot_id: bot.bot_id
                            }).then(function(bots) {
                                global.groupme('bots', 'POST', {
                                    bot: {
                                        name: 'directConnect - ' + body.name,
                                        group_id: body.group_id,
                                        callback_url: 'http://gamecomtools.com/api/groupme/callback?type=directConnect&communityID=' + communityID
                                    }
                                });
                            });
                        }
                    })
                });

                addToGroup(typeStepData[body.group_id].groups.main, body.sender_id, body.name);
                typeStepData[body.group_id].groups.addToGroup.forEach(function(value, index) {
                    if (value) {
                        addToGroup(typeStepData[body.group_id].groups.gameIds[index], body.sender_id, body.name);
                    }
                });
                delete typeStepData[body.group_id];
            } else {
                var game = typeStepData[body.group_id].groups.games[typeStepData[body.group_id].currentGroup];
                global.groupme('groups/' + body.group_id + '/messages', 'POST', {
                    message: {
                        source_guid: guid(),
                        text: 'Do you play ' + game + '?'
                    }
                });
            }
            global.persist.setItemSync('typeStepData', typeStepData);
        }, waitTime);
    }

    function addToGroup(groupID, userID, nickname) {
        global.groupme('groups/' + groupID, 'GET').then(function (response) {
            var found = false;
            response.response.members.forEach(function(member) {
                if (member.user_id === userID) {
                    found = true;
                }
            });
            if (!found) {
                global.groupme('groups/' + groupID + '/members/add', 'POST', {
                    members: [
                        {
                            nickname: nickname,
                            user_id: userID,
                            guid: guid()
                        }
                    ]
                });
            }
        });
    }
};

module.exports = function(status, service) {
    var outageSubscriptions = [
        '13489736', // The Reddit Rebels
        '8933432' //The Reddit Bandits
    ];

    var offlineMessage = "ATTENTION: This is an automated early alert notification that one of my backed services is down. At this time the following services are affected:\n\n";
    if (service === 'parse') {
        offlineMessage += '    - Automatic GroupMe Invites (delayed)\n';
        offlineMessage += '    - GroupMe bot commands (fail)\n';
    }
    if (service === 'reddit') {
        offlineMessage += '    - Automatic GroupMe Invites (delayed)\n';
    }
    offlineMessage += "\n\nI will notify once the service has been restored by our external partners.";

    var message = "ATTENTION: This is an automated early response notification that our backend service has been restored by our partner. All delayed services will now catch up.";

    if (status === 'offline') {
        message = offlineMessage;
    }

    outageSubscriptions.forEach(function(groupId) {
        global.groupme('groups/' + groupId+ '/messages', 'POST', {
            "message": {
                "source_guid": global.guid(),
                "text": message
            }
        });
    });
};

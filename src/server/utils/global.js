var request = require('request');
var q = require('q');
var path = require('path');

global.utilsPath = path.resolve(__dirname + '/');

global.webHost = 'gamecomtools.com';
if (process.env.env !== 'PROD') {
    global.webHost = 'localhost:3000';
}

global.q = q;

var ParseLib = require('node-parse-api').Parse;
global.parse = new ParseLib(global.secureData.parseOptions);

global.Snoocore = require('snoocore');
global.reddit = new Snoocore({
    userAgent: 'GameComTools@1.0.0 by kpkody',
    oauth: {
        type: 'script',
        key: global.secureData.reddit.key,
        secret: global.secureData.reddit.secret,
        scope: [ 'modconfig', 'modflair', 'privatemessages', 'modself', 'read', 'submit' ],
        username: global.secureData.reddit.username,
        password: global.secureData.reddit.password
    }
});

// Google Setup
global.google = global.secureData.google;

// GroupMe Setup
global.groupme = function(endpoint, method, data, all) {
    var deferred = q.defer();
    var options = {
        uri: 'https://api.groupme.com/v3/' + endpoint,
        method: method,
        qs: {
            token: global.secureData.groupMe.token
        },
        headers : {
            'X-Access-Token': global.secureData.groupMe.token
        },
        json: data || {}
    };
    var query = ['token=' + global.secureData.groupMe.token];
    if (method === 'GET') {
        for (var i in options.json) {
            query.push(i+'='+options.json[i]);
        }
        options.json = true;
    }
    if (options.uri.indexOf('?') === -1) {
        options.uri += '?';
    } else {
        options.uri += '&';
    }
    options.uri += query.join('&');
    var getNextPage = function(resp, def, response) {
        if (resp === undefined) {
            return def.resolve({
                response: response
            });
        }
        data.page ++;
        if (resp.response !== null && resp.response !== undefined && resp.response.length > 0) {
            response = response.concat(resp.response);
            global.groupme(endpoint, method, data).then(function(resp) {
                getNextPage(resp, def, response);
            });
        } else {
            def.resolve({
                response: response
            });
        }
    };
    request(options, function(error, response, body) {
        if (all) {
            data.page = 1;
            getNextPage(body, deferred, []);
        } else {
            deferred.resolve(body);
        }
    });

    return deferred.promise;
};
global.groupmeWaitTime = 700;

// GoogleAPI Setup
var google = require('googleapis');
global.googleCalendar = google.calendar('v3');
global.googleKey = global.secureData.googleCalendar.key;

// Global Q Error Handler
//global.QErrorHandler = require('./QErrorHandler');

//xbox
//global.xbox = require('./xboxLive');
//global.xbox.username = global.secureData.xbox.username;
//global.xbox.password = global.secureData.xbox.password;

//Storage
global.persist = require('node-persist');
global.persist.initSync();

//Steam
var Steam = require('steam-webapi');
Steam.key = global.secureData.steam.apiKey;
Steam.ready(function(err) {
    if (err) return console.log(err);
    global.steam = new Steam();
});

global.guid = function() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
};

global.persistentChanged = false;
global.persistent = {};
global.getPersistent = function(key) {
    if (!global.persistent[key]) {
        global.persistent[key] = global.persist.getItem(key) || {};
    }
    return global.persistent[key];
};
global.savePersistent = function() {
    global.persistentChanged = true;
};

global.clearCommandCooldown = function(command, groupId) {
    setTimeout(function() {
        var time = global.getPersistent('commandCooldown-' + groupId);
        time[command] = 0;
        global.savePersistent();
    }, 1000);
};

setInterval(function() {
    if (global.persistentChanged) {
        Object.keys(global.persistent).forEach(function(key) {
            global.persist.setItem(key, global.persistent[key]);
        });
        global.persistentChanged = false;
    }
}, 5000);

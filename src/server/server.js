var site = require('./site');
require('./api')(site).then(function() {
    require('./background')(site).then(function() {
        site.listen(3000, function () {
            console.log('Server Started!');
        });
    });
});

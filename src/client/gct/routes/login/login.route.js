'use strict';

angular
    .module('gct.login')
    .config(GctLoginConfig);

function GctLoginConfig($stateProvider){
    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: '/gct/routes/login/login.htm',
            controller: 'GctLogin',
            controllerAs: 'GctLoginVM',
            data: {
                title: 'Login',
                fullpage: true,
                class: 'login-page'
            },
            resolve: {
                authCheck: function(gUser) {
                    return gUser.authCheck();
                }
            },
            params: {
                returnState: 'dashboard'
            }
        })
}

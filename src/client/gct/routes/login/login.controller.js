'use strict';

angular
    .module('gct.login')
    .controller('GctLogin', GctLoginController);

function GctLoginController(gUser, $state, authCheck, gCommunity, $rootScope, $stateParams) {
    var vm = this;
    vm.loggingIn = false;
    vm.validating = true;

    vm.login = function() {
        if (!vm.loggingIn) {
            vm.loggingIn = true;
            gUser.login(vm.username, vm.password).then(function(resp) {
                vm.loggingIn = false;
                if (resp.error) {
                    vm.error = resp.error;
                } else {
                    // valid login
                    gUser.data = resp;
                    gCommunity.community(resp.community).then(function(resp) {
                        gCommunity.data = resp;
                        $state.go($rootScope.previousState);
                    });
                }
            });
        }
    };

    if (authCheck.valid === true) {
        vm.username = 'session';
        vm.password = 'session';
        vm.login();
    } else {
        vm.validating = false;
    }
}

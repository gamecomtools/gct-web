'use strict';

angular
    .module('gct.groupme')
    .controller('GctGroupMeChats', GctGroupMeChatsController);

function GctGroupMeChatsController(gGroupMe, gCommunity, $state, $timeout, $interval, GctWidgetsToColumns) {
    var vm = this;
    vm.loading = true;
    vm.addingGroups = false;
    vm.checkForGroups = false;

    vm.groups = gCommunity.data.socialConnections.groupme.groups || {};
    vm.groupIds = Object.keys(vm.groups);

    vm.foundGroups = [];
    vm.notGroups = [];
    vm.preNotGroups = {

    };
    vm.notGroupsChecks = {};
    vm.groupMeGameMap = {};
    vm.groupMeGameTypes = {};
    vm.groupFunctions = {};
    vm.directMessages = {};

    vm.currentGroup = 0;
    vm.groupFunctionsKeys = null;

    if (vm.groupIds.length == 0) {
        vm.loading = false;
    } else {
        loadGroups();
    }

    vm.doneAdding = function() {
        vm.checkForGroups = false;
        vm.addingGroupsStatus = 'Getting Ready...';
        $state.current.data.step = 2;
        vm.groupFunctionsKeys = Object.keys(vm.groupFunctions);
        activateGroup();
    };

    vm.addGroups = function() {
        vm.addingGroups = true;
        vm.checkForGroups = true;
        $state.current.data.step = 1;
        $state.current.data.stepTotal = 2;
        checkGroups();
    };

    vm.notMyGroup = function(group) {
        if (vm.preNotGroups[group.id]) {
            vm.preNotGroups[group.id] = -1;
        } else {
            vm.preNotGroups[group.id] = 3;
            var int = $interval(function () {
                vm.preNotGroups[group.id]--;
                if (vm.preNotGroups[group.id] === 0) {
                    $interval.cancel(int);
                    delete vm.preNotGroups[group.id];
                    vm.notGroups.push(group.id);
                }else if (vm.preNotGroups[group.id] < 0) {
                    $interval.cancel(int);
                    delete vm.preNotGroups[group.id];
                }
            }, 1000);
        }
    };

    vm.updateGroup = function(groupId, key, value) {
        return gGroupMe.update(groupId, key, value).then(function() {
            if (key === 'lockTitle' || key === 'title') {
                vm.groups.forEach(function(row) {
                    row.forEach(function(col) {
                        col.forEach(function(group) {
                            if (group.id === groupId) {
                                vm.updateGroup(groupId, 'lockTitleValue', group.name);
                            }
                        });
                    });
                });
            }
        });
    };

    function loadGroups() {
        gGroupMe.groupme(vm.groupIds).then(function(resp) {
            vm.groupMeGameMap = resp.groupMeGameMap;
            vm.groupMeGameTypes = resp.groupMeGameTypes;
            resp.groups.forEach(function(group) {
                group.function = vm.groups[group.id];
                if (group.function === 'leadership' || group.function === 'random') {
                    group.botOptions.excludeAutoInvite = true;
                }
            });
            vm.groups = GctWidgetsToColumns(resp.groups, 3);
            vm.loading = false;
        });
    }

    function activateGroup() {
        var groupId = vm.groupFunctionsKeys[vm.currentGroup];
        vm.foundGroups.forEach(function(group) {
            if (group.id == groupId) {
                vm.addingGroupsStatus = 'Activating ' + group.name + '...';
            }
        });
        gGroupMe.activate(groupId, vm.groupFunctions[groupId]).then(function() {
            vm.currentGroup++;
            if (vm.currentGroup > vm.groupFunctionsKeys.length-1) {
                vm.addingGroupsStatus = 'Wrapping Up...';
                vm.addingGroups = false;
                $state.current.data.step = undefined;
                $state.current.data.stepTotal = undefined;
                vm.loading = true;
                gCommunity.community(gCommunity.data.objectId).then(function(resp) {
                    gCommunity.data = resp;
                    loadGroups();
                });
            } else {
                activateGroup();
            }
        });
    }

    function checkGroups() {
        if (vm.checkForGroups) {
            if (vm.foundGroups.length === 0) {
                vm.loading = true;
            }
            gGroupMe.checkGroups().then(function(resp) {
                if (vm.checkForGroups) {
                    vm.loading = false;
                    if (vm.foundGroups.length === 0) {
                        vm.groupMeGameMap = resp.groupMeGameMap;
                        vm.groupMeGameTypes = resp.groupMeGameTypes;
                    }
                    vm.foundGroups = resp.groups;
                    $timeout(checkGroups, 5000);
                }
            });
        }
    }
}

'use strict';

angular
    .module('gct.groupme')
    .controller('GctGroupMeAutoInvites', GctGroupMeAutoInvitesController);

function GctGroupMeAutoInvitesController(gGroupMe, gCommunity, $filter) {
    var vm = this;
    vm.loading = false;
    vm.showForm = false;

    vm.community = gCommunity;
    vm.inviteFormData = {
        type: 'disabled'
    };
    vm.invites = {};

    vm.enableAutoUpdates = function() {
        vm.loading = true;
        vm.showForm = false;
        gGroupMe.enableAutoInvites(vm.inviteFormData).then(function() {
            vm.loadLogs();
        });
    };

    vm.updateGroup = function(key, value) {
        var data = {};
        data[key] = value;
        gGroupMe.updateAutoInvite(data);
    };

    vm.orderByDate = function(item) {
        console.log(item);
        return 1;
    };

    vm.loadLogs = function() {
        vm.loading = true;
        gGroupMe.autoInvites().then(function(resp) {
            vm.invites = resp.plain();
            var keys = Object.keys(vm.invites);
            var invites = [];
            keys.forEach(function (key) {
                invites.push({
                    key: key,
                    received: vm.invites[key].received
                });
            });
            invites = $filter('orderBy')(invites, 'received', true);
            var i = [];
            invites.forEach(function(invite) {
                i.push(vm.invites[invite.key]);
            });
            vm.invites = i;
            vm.loading = false;
        });
    }

    vm.loadLogs();
}

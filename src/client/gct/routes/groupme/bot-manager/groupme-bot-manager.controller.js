'use strict';

angular
    .module('gct.groupme')
    .controller('GctGroupMeBotManager', GctGroupMeBotManagerController);

function GctGroupMeBotManagerController(gCommunity, gGroupMe, $state, GctWidgetsToColumns, $timeout) {
    var vm = this;
    vm.loading = true;
    vm.showAccessControl = false;
    vm.showAreYouSure = false;

    vm.groups = gCommunity.data.socialConnections.groupme.groups || {};
    vm.groupIds = Object.keys(vm.groups);

    vm.customCommands = gCommunity.data.socialConnections.groupme.customCommands || {};
    vm.builtInCommands = [];
    vm.selectedGroup = {};
    vm.hasCustomCommands = Object.keys(vm.customCommands).length !== 0;

    if (vm.groupIds.length === 0) {
        $state.go('groupme-add');
    } else {
        gGroupMe.groupme(vm.groupIds).then(function (resp) {
            vm.groupMeGameMap = resp.groupMeGameMap;
            vm.groupMeGameTypes = resp.groupMeGameTypes;
            resp.groups.forEach(function (group) {
                if (!vm.selectedGroup.name) {
                    vm.selectedGroup = group;
                }
                group.function = vm.groups[group.id];
                group.botOptions.disabled = group.botOptions.disabled || false;
                group.botOptions.disabledCommands = group.botOptions.disabledCommands || {};
                group.botOptions.enabledCustomCommands = group.botOptions.enabledCustomCommands || {};
                group.botOptions.customCooldowns = group.botOptions.customCooldowns || {};
                group.botOptions.accessControl = group.botOptions.accessControl || {};
                group.disabledCommands = {};
                for (var key in resp.builtInCommands) {
                    if (!group.botOptions.disabledCommands[resp.builtInCommands[key].name]) {
                        group.botOptions.disabledCommands[resp.builtInCommands[key].name] = false;
                    }
                    if (!group.botOptions.accessControl[resp.builtInCommands[key].name]) {
                        group.botOptions.accessControl[resp.builtInCommands[key].name] = 'none';
                    }
                    if (resp.builtInCommands[key].gameRequirements.length > 0) {
                        var meetsRequirement = false;
                        resp.builtInCommands[key].gameRequirements.forEach(function(game) {
                            if (game === group.function) {
                                meetsRequirement = true;
                            }
                        });
                        if (!meetsRequirement && group.function !== 'main' && group.function !== 'leadership') {
                            group.botOptions.disabledCommands[resp.builtInCommands[key].name] = true;
                            group.disabledCommands[resp.builtInCommands[key].name] = 'Group is not ' + resp.builtInCommands[key].gameRequirements.join(' or ');
                        }
                    }
                }
                for (var key in resp.customCommands) {
                    if (!group.botOptions.enabledCustomCommands[resp.customCommands[key].name]) {
                        group.botOptions.enabledCustomCommands[resp.customCommands[key].name] = false;
                    }
                    if (!group.botOptions.accessControl[resp.customCommands[key].name]) {
                        group.botOptions.accessControl[resp.customCommands[key].name] = 'none';
                    }
                }
            });
            vm.builtInCommands = resp.builtInCommands;
            vm.groups = resp.groups;
            vm.loading = false;
        });
    }

    vm.updateGroup = function(groupId, key, value) {
        vm.selectedGroup.botOptions[key] = value;
        return gGroupMe.update(groupId, key, value).then(function() {

        });
    };

    var timeout = null;
    vm.updateCommandCooldown = function(groupId) {
        if (timeout !== null) {
            $timeout.cancel(timeout);
        }
        timeout = $timeout(function(groupId) {
            vm.updateGroup(groupId, 'customCooldowns', vm.selectedGroup.botOptions.customCooldowns);
        }.bind(null, groupId), 1500, true);
    };

    vm.accessControlChanged = function(command) {
        if (vm.selectedGroup.botOptions.accessControl[command] === 'new') {
            vm.selectedGroup.botOptions.accessControl[command] = 'none';
            vm.showAccessControl = true;
        } else if (vm.selectedGroup.botOptions.accessControl[command] === 'spacer') {
            vm.selectedGroup.botOptions.accessControl[command] = 'none';
        } else {
            vm.updateGroup(vm.selectedGroup.id, 'accessControl', vm.selectedGroup.botOptions.accessControl);
        }
    };

    var command = null;
    vm.applyToAll = function(cmd) {
        if (!vm.showAreYouSure) {
            command = cmd;
            vm.showAreYouSure = true;
        } else {
            var botOptions = {};
            vm.groups.forEach(function (group) {
                for (var i in group.botOptions.accessControl) {
                    if (i === command) {
                        group.botOptions.accessControl[i] = angular.copy(vm.selectedGroup.botOptions.accessControl[i]);
                    }
                }
                for (var i in group.botOptions.customCooldowns) {
                    if (i === command) {
                        group.botOptions.customCooldowns[i] = angular.copy(vm.selectedGroup.botOptions.customCooldowns[i]);
                    }
                }
                botOptions[group.id] = angular.copy(group.botOptions);
            });
            vm.showAreYouSure = false;
            console.log(botOptions);
            vm.updateGroup(undefined, 'botOptions', botOptions);
        }
    }
}

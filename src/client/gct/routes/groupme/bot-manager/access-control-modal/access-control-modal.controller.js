'use strict';

angular
    .module('gct.groupme')
    .controller('GctAccessControlModal', GctAccessControlModalController);

function GctAccessControlModalController(gCommunity, gGroupMe, $scope) {
    var vm = this;

    vm.test = true;

    vm.closeModal = function() {
        $scope.parent.showAccessControl = false;
    }
}

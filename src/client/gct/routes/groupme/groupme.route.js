'use strict';

angular
    .module('gct.groupme')
    .config(GctGroupMeConfig);

function GctGroupMeConfig($stateProvider){
    $stateProvider
        .state('groupme-add', {
            url: '/groupme/add',
            templateUrl: '/gct/routes/groupme/add/groupme-add.htm',
            controller: 'GctGroupMeAdd',
            controllerAs: 'GctGroupMeAddVM',
            data: {
                title: 'GroupMe',
                securePage: true
            }
        })
        .state('groupme-chats', {
            url: '/groupme/chats',
            templateUrl: '/gct/routes/groupme/chats/groupme-chats.htm',
            controller: 'GctGroupMeChats',
            controllerAs: 'GctGroupMeChatsVM',
            data: {
                title: 'GroupMe',
                description: 'Chat Groups',
                securePage: true
            }
        })
        .state('groupme-automatic-invites', {
            url: '/groupme/automatic-invites',
            templateUrl: '/gct/routes/groupme/automatic-invites/groupme-automatic-invites.htm',
            controller: 'GctGroupMeAutoInvites',
            controllerAs: 'GctGroupMeAutoInvitesVM',
            data: {
                title: 'GroupMe',
                description: 'Automatic Invites',
                securePage: true
            }
        })
        .state('groupme-bot-manager', {
            url: '/groupme/bot-manager',
            templateUrl: '/gct/routes/groupme/bot-manager/groupme-bot-manager.htm',
            controller: 'GctGroupMeBotManager',
            controllerAs: 'GctGroupMeBotManagerVM',
            data: {
                title: 'GroupMe',
                description: 'Bot Manager',
                securePage: true
            }
        })
}

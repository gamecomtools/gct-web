'use strict';

angular
    .module('gct.groupme')
    .controller('GctGroupMeAdd', GctGroupMeAddController);

function GctGroupMeAddController(gSocialConnection, gCommunity, $state) {
    var vm = this;
    vm.activating = false;

    vm.activate = function(){
        vm.activating = true;

        gSocialConnection.activate('groupme').then(function(resp) {
            if (resp.success) {
                return gCommunity.community(gCommunity.data.objectId);
            } else {
                $state.go('500', {
                    error: resp.error
                });
            }
        }).then(function(resp) {
            gCommunity.data = resp;
            $state.go('groupme-chats');
        });
    }
}

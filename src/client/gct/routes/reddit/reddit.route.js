'use strict';

angular
    .module('gct.reddit')
    .config(GctRedditConfig);

function GctRedditConfig($stateProvider){
    $stateProvider
        .state('reddit-add', {
            url: '/reddit/add',
            templateUrl: '/gct/routes/reddit/add/reddit-add.htm',
            controller: 'GctRedditAdd',
            controllerAs: 'GctRedditAddVM',
            data: {
                title: 'Reddit',
                securePage: true
            }
        })
        .state('reddit-overview', {
            url: '/reddit/',
            templateUrl: '/gct/routes/reddit/overview/reddit-overview.htm',
            controller: 'GctRedditOverview',
            controllerAs: 'GctRedditOverviewVM',
            data: {
                title: 'Reddit',
                securePage: true
            }
        })
}

'use strict';

angular
    .module('gct.reddit')
    .controller('GctRedditAdd', GctRedditAddController);

function GctRedditAddController(gReddit, gSocialConnection, gCommunity) {
    var vm = this;
    vm.step = 1;
    vm.foundData = {};
    vm.notSubreddits = [];

    checkModInvite();

    vm.activate = function() {
        vm.step = 3;
        gSocialConnection.activate('reddit', vm.foundData).then(function() {
            if (resp.success) {
                return gCommunity.community(gCommunity.data.objectId);
            } else {
                $state.go('500', {
                    error: resp.error
                });
            }
        }).then(function(resp) {
            gCommunity.data = resp;
            $state.go('reddit-overview');
        });
    };

    vm.wrongSub = function() {
        vm.notSubreddits.push(vm.foundData.subreddit);
        vm.step = 1;
        checkModInvite()
    };

    function checkModInvite(){
        gReddit.checkModInvites().then(function(result) {
            if (result.status === 'invite' && vm.notSubreddits.indexOf(result.subreddit) === -1) {
                vm.foundData = result.plain();
                vm.step = 2;
            } else {
                setTimeout(checkModInvite, 10000);
            }
        });
    }
}

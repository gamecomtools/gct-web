'use strict';

angular
    .module('gct.components')
    .directive('gctNavbar', GctNavbarDirective);

function GctNavbarDirective() {
    return {
        templateUrl: '/gct/components/navbar/navbar.directive.htm',
        controller: 'GctNavbarController',
        controllerAs: 'GctNavbarVM',
        replace: true
    }
}

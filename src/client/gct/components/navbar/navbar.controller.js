'use strict';

angular
    .module('gct.components')
    .controller('GctNavbarController', GctNavbarController);

function GctNavbarController(gUser){
    var vm = this;

    vm.user = gUser;
}

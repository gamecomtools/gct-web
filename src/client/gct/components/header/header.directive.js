'use strict';

angular
    .module('gct.components')
    .directive('gctHeader', GctHeaderDirective);

function GctHeaderDirective() {
    return {
        templateUrl: '/gct/components/header/header.directive.htm',
        controller: 'GctHeaderController',
        controllerAs: 'GctHeaderVM',
        replace: true
    }
}

'use strict';

angular
    .module('gct.components')
    .controller('GctSidebarController', GctSidebarController);

function GctSidebarController(gCommunity, $timeout){
    var vm = this;

    vm.community = gCommunity;
    vm.menu = [
        {
            type: 'header',
            text: 'Social Connections'
        },
        {
            type: 'link',
            text: 'GroupMe',
            state: 'groupme-add',
            icon: 'users',
            rightIcon: 'plus'
        },
        {
            type: 'link',
            text: 'Reddit',
            state: 'reddit-add',
            icon: 'reddit',
            rightIcon: 'plus'
        },
        {
            type: 'header',
            text: 'Member Management'
        },
        {
            type: 'header',
            text: 'Member Services'
        },
        {
            type: 'header',
            text: 'Task Management'
        }
    ];
    var mapping = {
        groupme: 1,
        reddit: 2
    };

    var socialMenus = {
        groupme: [
            {
                text: 'Chat Groups',
                state: 'groupme-chats'
            },
            {
                text: 'Automatic Invites',
                state: 'groupme-automatic-invites'
            },
            {
                text: 'Trigger Actions',
                state: 'groupme-triggers'
            },
            {
                text: 'Bot Manager',
                state: 'groupme-bot-manager'
            }
        ],
        reddit: [
            {
                text: 'Overview',
                state: 'reddit-overview'
            }
        ]
    };

    if (gCommunity.data !== null) {
        gCommunity.data.socialNetworks.forEach(function (connection) {
            if (mapping[connection]) {
                delete vm.menu[mapping[connection]].rightIcon;
                vm.menu[mapping[connection]].type = 'sub';
                if (socialMenus[connection]) {
                    vm.menu[mapping[connection]].submenu = socialMenus[connection];
                    vm.menu[mapping[connection]].subStates = [];
                    socialMenus[connection].forEach(function (sub) {
                        vm.menu[mapping[connection]].subStates.push(sub.state);
                    });
                }
            }
        });
        $timeout(function() {
            $.AdminLTE.tree('.sidebar');
        });
    }
}

'use strict';

angular
    .module('gct.components')
    .directive('gctSidebar', GctSidebarDirective);

function GctSidebarDirective() {
    return {
        templateUrl: '/gct/components/sidebar/sidebar.directive.htm',
        controller: 'GctSidebarController',
        controllerAs: 'GctSidebarVM',
        replace: true
    }
}

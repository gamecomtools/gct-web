'use strict';

angular
    .module('gct', [
        'ui.router',
        //'ui.bootstrap',
        'angular-loading-bar',
        'restangular',
        'gct.templates',
        'gct.components',
        'gct.dashboard',
        'gct.login',
        'gct.groupme',
        'gct.reddit'
    ])
    .config(GctConfig)
    .run(gctRun);

var moving = false;

function GctConfig($urlRouterProvider, $locationProvider, cfpLoadingBarProvider, RestangularProvider){
    RestangularProvider.setBaseUrl('/api');

    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise(function($injector, $location) {
        if (!moving) {
            moving = true;
            $location.url('/dashboard');
        }
    });
    //GctThemeProvider.$get().setTheme('blue-grey');
    cfpLoadingBarProvider.includeSpinner = false;
}

function gctRun($state, $rootScope, gUser, $timeout) {
    $rootScope.state = $state;
    $rootScope.previousState = null;

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if (toState.name !== 'login') {
            $rootScope.previousState = toState.name;
        }
        if (toState.data.securePage && gUser.data === null) {
            event.preventDefault();
            $state.go('login', {
                returnState: $rootScope.previousState
            });
            $timeout(function() {
                moving = false;
            })
        } else if(fromState.name === 'login') {
            $timeout(function() {
                $.AdminLTE.layout.activate();
                var o = $.AdminLTE.options;
                $.AdminLTE.pushMenu.activate(o.sidebarToggleSelector);
            });
        }
    });
}

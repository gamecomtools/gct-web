'use strict';

angular
    .module('gct')
    .factory('gUser', gUser);

function gUser($q, Restangular){
    var login = Restangular.all('login');
    var authCheck = Restangular.all('authCheck');
    return {
        data: null,
        login: function(username, password) {
            return login.post({
                username: username,
                password: password
            });
        },
        authCheck: function() {
            return authCheck.customGET('');
        }
    }
}

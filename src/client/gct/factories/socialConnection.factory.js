'use strict';

angular
    .module('gct')
    .factory('gSocialConnection', gSocialConnection);

function gSocialConnection($q, Restangular){
    var socialConnection = Restangular.all('socialConnection');
    return {
        activate: function(network, data) {
            return socialConnection.post({
                network: network,
                data: data
            });
        }
    }
}

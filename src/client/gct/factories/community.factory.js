'use strict';

angular
    .module('gct')
    .factory('gCommunity', gCommunity);

function gCommunity($q, Restangular){
    var community = Restangular.all('community');
    return {
        data: null,
        community: function(communityId) {
            return community.customGET('', {
                communityId: communityId
            });
        }
    }
}

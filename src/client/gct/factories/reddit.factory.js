'use strict';

angular
    .module('gct')
    .factory('gReddit', gReddit);

function gReddit($q, Restangular){
    var reddit = Restangular.all('reddit');
    return {
        checkModInvites: function() {
            return reddit.customGET('checkModInvites');
        }
    }
}

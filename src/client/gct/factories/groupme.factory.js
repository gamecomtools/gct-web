'use strict';

angular
    .module('gct')
    .factory('gGroupMe', gGroupMe);

function gGroupMe($q, Restangular){
    var groupme = Restangular.all('groupme');
    return {
        data: null,
        groupme: function(groups) {
            return groupme.customGET('', {
                groups: groups.join(',')
            });
        },
        checkGroups: function(groups) {
            return groupme.customGET('checkGroups');
        },
        activate: function(groupId, groupFunction) {
            return groupme.customPOST({
                groupId: groupId,
                groupFunction: groupFunction
            }, 'activate');
        },
        update: function(groupId, key, value) {
            return groupme.customPOST({
                groupId: groupId,
                key: key,
                value: value
            }, 'update');
        },
        enableAutoInvites: function(data) {
            return groupme.customPOST({
                data: data
            }, 'autoInvites');
        },
        autoInvites: function() {
            return groupme.customGET('autoInvites');
        },
        updateAutoInvite: function(data) {
            return groupme.customPOST({
                data: data
            }, 'autoInvites');
        }
    }
}
